import os, logging, fileinput, stat, shutil, sys, itertools, yaml, random


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

TEMPLATES_FOLDER = "./templates"
TEMPLATE_FIRST_PART = TEMPLATES_FOLDER + "/first_part"
TEMPLATE_FINAL_PART = TEMPLATES_FOLDER + "/final_part"
TEMPLATE_LOGIC = TEMPLATES_FOLDER+"/multi_domain_logic"
TMP_FOLDER = "./tmp"
EXECUTABLE_FILE = TMP_FOLDER+"/test.py"

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

config = dict()

SOCKET_COUNT = 1

dbs = ["cassandra","mysql"]
ios = ["ffmpeg"]
cpus = ["svm","page_rank","redis"]

groups = [dbs, ios, cpus]

#
# every benchmark on the same socket belogns to the same group
# puts exactly "bench_per_socket" benchmark on each socket (repetition is allowed)
#
def affine_batches(bench_per_socket):
    batches = []

    combinations = list(itertools.combinations(groups, SOCKET_COUNT))

    for combination in combinations:
        current_batch = []
        for i in range(0, SOCKET_COUNT):
            current_group = combination[i]
            socket_batch = []
            for j in range(0, bench_per_socket):
                socket_batch.append(current_group[j % len(current_group)])
            current_batch.append(socket_batch)
        batches.append(current_batch)

    return batches

#
# every benchmak on the same socket belongs to a different group
# puts at most "max_bench_per_socket" benchmarks on each socket (repetition not allowed)
#
def mixed_batches(max_bench_per_socket):
    batches = []
    
    combinations = list(itertools.combinations(groups, max_bench_per_socket))
    
    for combination in combinations:
        current_batch = []
        next_available = []
        for group in groups:
            next_available.append(0)

        for i in range(0, SOCKET_COUNT):
            socket_batch = []
            for i in range(0,len(combination)):
                if next_available[i] < len(combination[i]):
                    socket_batch.append(combination[i][next_available[i]])
                    next_available[i] = next_available[i] + 1
            current_batch.append(socket_batch)
        batches.append(current_batch)

    return batches


def standard_batches():

	batches = []

	for db in dbs:
		batches.append([ios[0],db],[])

	for cpu in cpus:
		batches.append([ios[0],cpu],[])

	for cpu in cpus:
		for db in dbs:
			batches.append([cpu,db],[])

	return batches


def create_combinations():
	
	batches = standard_batches()

	all_tests = [] 
	for batch in batches:
	
		test_name = ""
		name_test_folder = ""

		socket_1_data = batch[0]
		socket_2_data = batch[1]

		for test in socket_1_data:
			name_test_folder = name_test_folder + test + "+"
		name_test_folder = name_test_folder[:-1]
		name_test_folder = name_test_folder + "-"	

		for test in socket_2_data:
			name_test_folder = name_test_folder + test + "+"
		name_test_folder = name_test_folder[:-1]
		print "Batch generated {}".format(batch)
		print "Test folder name: {}".format(name_test_folder)
		print "Test on socket 1: {}".format(str(socket_1_data))
		print "Test on socket 2: {}".format(str(socket_2_data))
		print "::::::::::::::::::::::::::::::::::::::::::::::::"
		all_tests.append((socket_1_data,socket_2_data,name_test_folder,test_name))

	return all_tests



def write_executable_file():
	logger.info("Creating test file executable...")
	with open(EXECUTABLE_FILE,'a+') as output:
		for line in open(TEMPLATE_FIRST_PART,'r'):
			output.write(line)

		for line in open(TEMPLATE_LOGIC,'r'):
			output.write(line)

		for line in open(TEMPLATE_FINAL_PART,'r'):
			output.write(line)

	st = os.stat(EXECUTABLE_FILE)
	os.chmod(EXECUTABLE_FILE, st.st_mode | stat.S_IEXEC)

def main():
	logger.info("Creating tmp working directory...")
	
	if not os.path.exists(TMP_FOLDER):
		os.makedirs(TMP_FOLDER)

	logger.info("Generating standard executable...")
	write_executable_file()

	logger.info("Starting creating scripts test file...")

	combinations = create_combinations()

	#create all combinations of tests
	'''
	creo un oggetto che ha:
		per ogni socket:
			* numero di domini sul socket (s1,s2)
			* numero di core per domain
			* ip vari domain
			* lista dei test socket 1
			* lista dei test socket 2

	'''
	test_paths = []

	for combination in combinations:
		name_test_folder = combination[2]
		full_path = os.environ['TESTS_LAUNCHERS_FOLDER']+"/"+name_test_folder
		logger.info("Creating test folder..."+full_path)
		if	not	os.path.exists(full_path):
			os.makedirs(full_path)
		test_paths.append(full_path)
		test = combination[3]
		socket1_tests = combination[0]
		socket2_tests = combination[1]
		core_per_domain = 2
		config["current_test"] = test
		config["test_id"] = name_test_folder
		config["socket1.domains"] = len(socket1_tests)
		config["socket2.domains"] = len(socket2_tests)
		config["core_per_socket"] = int(os.environ['CORE_PER_SOCKET'])
		config["core_per_domain"] = int(core_per_domain)
		config["socket1.tests"] = socket1_tests
		config["socket2.tests"] = socket2_tests

		logger.info("Copying executable	test file...")
		shutil.copy2(EXECUTABLE_FILE,full_path)

		logger.info("Creating config yaml file...")

		with open(full_path+'/config.yml', 'w') as outfile:
			outfile.write(yaml.dump(config, default_flow_style=True))

	logger.info("Removing tmp folder and files...")
	shutil.rmtree(TMP_FOLDER) 

	with open(os.environ['LIST_LAUNCHER_FILES'],"w+") as output:
		for line in test_paths:
			output.write(line+"\n")

main()