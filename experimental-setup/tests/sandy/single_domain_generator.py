import	os, logging, fileinput, stat, shutil, yaml


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


TEMPLATES_FOLDER = "./templates"
TEMPLATE_FIRST_PART = TEMPLATES_FOLDER + "/first_part"
TEMPLATE_FINAL_PART = TEMPLATES_FOLDER + "/final_part"
TEMPLATE_LOGIC = TEMPLATES_FOLDER+"/single_domain_logic"
TEMPLATE_SINGLE_INSTANCE = TEMPLATES_FOLDER + "/single_instance_socket"
TMP_FOLDER = "./tmp"
EXECUTABLE_FILE = TMP_FOLDER+"/test.py"

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

config = dict()

def	write_executable_file():
	logger.info("Creating test file	executable...")
	with	open(EXECUTABLE_FILE,'a+') as output:
		for	line in	open(TEMPLATE_FIRST_PART,'r'):
			output.write(line)

		for	line in	open(TEMPLATE_LOGIC,'r'):
			output.write(line)

		for	line in	open(TEMPLATE_FINAL_PART,'r'):
			output.write(line)

	st = os.stat(EXECUTABLE_FILE)
	os.chmod(EXECUTABLE_FILE, st.st_mode | stat.S_IEXEC)


def	create_test_environment(test,domains_socket_1,domains_socket_2,vCPU_per_domain):
	name_test_folder = test + "_" + str(domains_socket_1) + "_" + str(domains_socket_2) + "_" + str(vCPU_per_domain)
	full_path = os.environ['TESTS_LAUNCHERS_FOLDER']+"/"+name_test_folder
	
	logger.info("Creating test folder..."+full_path)
	if	not	os.path.exists(full_path):
		os.makedirs(full_path)

	logger.info("Copying executable	test file...")
	shutil.copy2(EXECUTABLE_FILE,full_path)

	instances = int(domains_socket_1) + int(domains_socket_2)
	config["current_test"] = test
	config["test_id"] = name_test_folder
	config["number_instances"] = int(instances)
	config["core_per_socket"] = int(os.environ['CORE_PER_SOCKET'])
	config["instances_socket_1"] = int(domains_socket_1)
	config["instances_socket_2"] = int(domains_socket_2)
	config["vcpu_per_domain"] = int(2)
	
	logger.info("Creating config yaml file...")
	with open(full_path+'/config.yml', 'w') as	outfile:
		outfile.write(yaml.dump(config, default_flow_style=True))
	return full_path


def	main():
	logger.info("Creating tmp working directory...")
	if not	os.path.exists(TMP_FOLDER):
		os.makedirs(TMP_FOLDER)

	logger.info("Generating	standard executable...")
	write_executable_file()


	logger.info("Creating tmp working directory...")

	test_paths = []

	with open(os.environ['LIST_TESTS_FILE'],"a+") as input:
		tests = input.readlines()

		logger.info("Starting creating scripts	test file...")

		for	item in	tests:
			test = item.replace("\n","").replace(" ","-")
			print test
			logger.info("Writing single	domain	config...")
			test_paths.append(create_test_environment(test,1,0,2))
			test_paths.append(create_test_environment(test,2,0,2))
	
	logger.info("Removing tmp folder and files...")
	shutil.rmtree(TMP_FOLDER)

	with open(os.environ['LIST_LAUNCHER_FILES'],"w+") as output:
		for line in test_paths:
			output.write(line+"\n")


	logger.info("All done here:)")

main()



	

