#!/bin/bash

sudo service cassandra start
sleep 10s
cassandra-stress mixed ratio\(write=1,read=3\) n=1000000 cl=ONE -pop dist=UNIFORM\(1..1000000\) -schema keyspace="keyspace1" -mode native cql3 -rate threads\>=16 threads\<=32 -log file=~/mixed_autorate_50r50w_1M.log
sudo service cassandra stop