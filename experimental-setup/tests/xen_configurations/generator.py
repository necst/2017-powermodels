import os

MAX_CFG_NUMBER 		= 1
MAX_CORE_NUMBER 	= 8
MAX_DOMAIN_NUMBER 	= 4

CONFIGURATION_DIRECTORY = "./configurations"

BASE_NAME 	= "marc_{}"
BASE_IP 	= "10.0.0.{}"

if not os.path.exists(CONFIGURATION_DIRECTORY):
    os.makedirs(CONFIGURATION_DIRECTORY)

with open('./templates/domain.cfg', 'r') as myfile:
    data = str(myfile.read())

for domain_id in range(0,MAX_CFG_NUMBER):
	domain_name = BASE_NAME.format(str(domain_id))
	domain_ip = BASE_IP.format(str(domain_id+10))

	with open('./templates/domain.cfg', 'r') as myfile:
	
		filedata = myfile.read()
		newdata = filedata.replace("$MARC_NAME",domain_name)
		
		finaldata = newdata.replace("$MARC_IP",domain_ip)

		with open(CONFIGURATION_DIRECTORY+"/"+domain_name+".cfg",'a+') as output:
			output.write(finaldata)



