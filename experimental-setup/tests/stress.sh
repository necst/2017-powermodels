#!/bin/bash

NOW=`/bin/date +"%Y-%m-%d-%H.%M"`
CURRENT_FOLDER=$DATA_TESTS_FOLDER/bench-NBP-$NOW
MAPPING_FILE=$CURRENT_FOLDER"/domain_mapping.csv"
WATTSUP_OUTPUT_TMP=$CURRENT_FOLDER"/watts-up-tmp"
WATTSUP_OUTPUT=$CURRENT_FOLDER"/wattsup-watts"


if [ ! -d $CURRENT_FOLDER ]; then
	mkdir -p $CURRENT_FOLDER
fi

echo $(date +%s.%N) " - Starting xen..."
sudo service xencommons start
echo $(date +%s.%N) " - Xen started"

sleep 20s

echo $(date +%s.%N) " - Starting Watt's up? Power Meter..."
sudo $WATTSUP -c 5 ttyUSB0 watts
sleep 5
eval ${WATTSUP_CLEAR}
sleep 1
eval ${WATTSUP_START_LOG}
echo $(date +%s.%N) " - Watt's up? Power Meter started."

sleep 5

echo $(date +%s.%N) " - Starting benchmarks Marc..."

echo $(date +%s.%N) " - Launching Marc domain..."
sudo xl create /home/marc/asplos/workspace/vms/marc_training_tmp/conf.cfg
sudo xl vcpu-pin $MARC_TRAIN_NAME 0 1
sudo xl vcpu-pin $MARC_TRAIN_NAME 1 2
sudo xl vcpu-pin $MARC_TRAIN_NAME 2 3
sudo xl vcpu-pin $MARC_TRAIN_NAME 3 4
sudo xl vcpu-pin $MARC_TRAIN_NAME 4 5
sudo xl vcpu-pin $MARC_TRAIN_NAME 5 6
sudo xl vcpu-pin $MARC_TRAIN_NAME 6 7
sudo xl vcpu-pin $MARC_TRAIN_NAME 7 8
sudo xl vcpu-pin $MARC_TRAIN_NAME 8 9

MAPPING_MARC=$(sudo xl list $MARC_TRAIN_NAME |awk '{print $2}' | sed -n 2p)
echo $MARC_TRAIN_NAME","$MAPPING_MARC >> $MAPPING_FILE
echo $(date +%s.%N) " - Marc domain started"

echo $(date +%s.%N) " - Starting Xentrace..."
$START_XENTRACE_4_PM_SCRIPT $CURRENT_FOLDER
START=$(date +%s.%N)
echo $(date +%s.%N) " - Xentrace started."

sleep 60s

echo $(date +%s.%N) " - Sending commands to Marc domain..."
ssh root@10.0.0.10 'screen -d -m /home/marc/stress.sh'
echo $(date +%s.%N) " - Command sent"

sleep 300s

echo $(date +%s.%N) " - Stopping domain marc..."
ssh root@10.0.0.10 'screen -d -m sudo halt'
echo $(date +%s.%N) " - Domain marc stopped."

sleep 5s

echo $(date +%s.%N) " - Stopping Xentrace..."
$STOP_XENTRACE_SCRIPT
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo $(date +%s.%N) " - Xentrace stopped. Duration: "$DIFF"s."


echo $(date +%s.%N) " - Stopping wattsup log..."
$WATTSUP_READER $WATTSUP_USB $WATTSUP_OUTPUT_TMP & 
sleep 1
eval ${WATTSUP_GET_DATA}
eval ${WATTSUP_LOW_LOAD}

echo $(date +%s.%N) " - Parsing trace data..."
$PARSE_DATA_4_PMC_SCRIPT $CURRENT_FOLDER
echo $(date +%s.%N) " - CSV file produced."

python $WATTUP_DATA_PARSER $WATTSUP_OUTPUT_TMP $WATTSUP_OUTPUT
rm $WATTSUP_OUTPUT_TMP
sudo chown -R $ASPLOS_USER:$ASPLOS_USER $CURRENT_FOLDER

echo $(date +%s.%N) " - Compressing results..."
tar -zcvf $CURRENT_FOLDER.tar.gz $CURRENT_FOLDER
rm -r $CURRENT_FOLDER
sudo chown -R $ASPLOS_USER:$ASPLOS_USER $CURRENT_FOLDER.tar.gz
echo $(date +%s.%N) " - Results compressed"

python $EMAIL_SCRIPT "Train test completed"

sudo reboot



