#!/bin/bash
#usage: ./parse_data.sh TEST_FOLDER
#echo every command

CURRENT_FOLDER=$1

echo "Parsing trace data..."
cat $CURRENT_FOLDER/trace.data | xentrace_format $CURRENT_FOLDER/rapl_trace_matlab.format >> $CURRENT_FOLDER/rapl.csv
cat $CURRENT_FOLDER/trace.data | xentrace_format $CURRENT_FOLDER/pmc1_4_trace_matlab.format >> $CURRENT_FOLDER/pmc1_4.csv
cat $CURRENT_FOLDER/trace.data | xentrace_format $CURRENT_FOLDER/pmc5_8_trace_matlab.format >> $CURRENT_FOLDER/pmc5_8.csv
cat $CURRENT_FOLDER/trace.data | xentrace_format $CURRENT_FOLDER/ctr_trace_matlab.format >> $CURRENT_FOLDER/ctr.csv
cat $CURRENT_FOLDER/trace.data | xentrace_format $CURRENT_FOLDER/freq_trace_matlab.format >> $CURRENT_FOLDER/freq.csv
echo "CSV file produced."