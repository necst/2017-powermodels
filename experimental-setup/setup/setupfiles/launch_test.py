import os,pickle,logging,sys,subprocess

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(os.environ['LOG_TESTS_FILE'])
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.DEBUG)
sh.setFormatter(formatter)
logger.addHandler(sh)

CMD_EXEC = ". /home/marc/.bashrc && sudo -E python {}/test.py &"

def parse_shell_result(cmd,out,err):
	return "\nCOMMAND => {}\nOUTUP => {}\nERROR => {}".format(cmd,out,err)


class TestState:

	def __init__(self,tests):
		self.tests = sorted(tests)
		self.current_test = 0
		self.state_directory = os.environ['STATE_FOLDER']
		self.state_file_name = os.environ['STATE_FILE_NAME']


	def launch_test(self):
		test = self.__test_to_execute()
		if test is None:
			cmd="python $EMAIL_SCRIPT \"{}\" \"{}\" ".format("Suite completed","All tests completed")
			proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		else:
			self.__increase_counter()
			self.__save_state()
			self.__launch_test(test)

	
	def __test_to_execute(self):
		if self.current_test < len(self.tests):
			return self.tests[self.current_test]
		else:
			None
	
	def __increase_counter(self):
		self.current_test = self.current_test + 1

	
	def __save_state(self):
		path_file = self.state_file_name
		filehandler = open(path_file,"wb")
		pickle.dump(self,filehandler)
		filehandler.close()

	
	def __launch_test(self,test_path):
		cmd = CMD_EXEC.format(test_path)
		self.__executor_background(cmd)

	def __executor_background(self,cmd):
		logger.info("Trying executing command:\n{}".format(cmd))
		print cmd
		proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		logger.debug(parse_shell_result(cmd,"",""))
		
		

state_file_path = os.environ['STATE_FILE_NAME']

print state_file_path

if os.path.isfile(state_file_path) == True:
	file = open(state_file_path,'rb')
	test_state = pickle.load(file)
	file.close()
else:
	with open(os.environ['LIST_LAUNCHER_FILES']) as f:
		content = []
		for line in f.readlines():
			content.append(line.replace("\n",""))

		test_state = TestState(content)

test_state.launch_test()
