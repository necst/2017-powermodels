package it.necst.marc.asplos

import it.necst.marc.asplos.helpers.RedisCSVWriter
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.infrastructure.support.EmailSender

/**
  * Created by andreacorna on 04/08/16.
  */
object CsvLoader {

  private val logger = MarcLogger(getClass.getName)

  def main(args : Array[String]): Unit = {

    val baseDirectory = "/home/morphone/asplos_csv_to_load"

    val listKeys: List[String] = (for(line <- scala.io.Source.fromFile("/home/morphone/marc_keys.txt").getLines())
      yield line.toString).toList

    def writeCsv(toBeProcessed: List[String]): Unit = {
      if(toBeProcessed.nonEmpty){
        RedisCSVWriter(baseDirectory+"/"+toBeProcessed.head,",",0,"marc","asplos")
        writeCsv(toBeProcessed.tail)
      }
    }

    writeCsv(listKeys)

    EmailSender.apply("andrea.corna.ac.91@gmail.com","ASPLOS DATA INSERT COMPLETED","Completed")

    System.exit(0)

  }


}
