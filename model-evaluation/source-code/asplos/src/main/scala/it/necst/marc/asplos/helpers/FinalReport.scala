package it.necst.marc.asplos.helpers

/**
  * Created by andreacorna on 07/08/16.
  */

case class FinalReport(testReports: List[TestReport])

case class TestInfo(machine: String, testName: String, HTEnabled: Boolean)

case class TestReport(testInfo: TestInfo, MSE: ModelMSE, CfgMSEs: List[ConfigurationMSE], testCoverage:Double)

case class ModelMSE(mse: Option[Double],rows: Int)

case class ConfigurationMSE(trainCode:String, mse: Option[Double], count: Int)
