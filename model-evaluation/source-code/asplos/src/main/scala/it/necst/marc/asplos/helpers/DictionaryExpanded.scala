package it.necst.marc.asplos.helpers

import it.necst.marc.data.Binding
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.support.Phase2Subphase

/**
  * Created by andreacorna on 06/08/16.
  */

case class DictionaryExpanded(byPhase:List[Dictionary])

case class Dictionary(phase:Phase2Subphase.Value, entries:List[Entry])

case class Entry(code:Code, bindings:List[Binding], codeExpanded: Code, bindingsExpanded: List[Binding])