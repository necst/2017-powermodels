package it.necst.marc.asplos.helpers

/**
  * Created by andreacorna on 10/08/16.
  *
  *
  *
  */

object MultiDomainResult {

  def apply(time: Int, realPower: Double, powerEstimated: Double, powerDomains: Map[String,Double], domains: List[String]): MultiDomainResult = {

    val distance = (realPower - powerEstimated) / realPower

    MultiDomainResult(time,realPower,powerEstimated,powerDomains,distance,domains)

  }
}
case class MultiDomainResult(time: Int, realPower: Double, powerEstimated: Double, powerDomains: Map[String,Double], distance: Double,domains: List[String]){}
