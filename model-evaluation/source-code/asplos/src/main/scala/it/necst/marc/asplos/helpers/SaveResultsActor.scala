package it.necst.marc.asplos.helpers

import java.io.File
import java.nio.charset.Charset
import java.nio.file.{Files, Path, StandardOpenOption}

import akka.actor.{Actor, ActorRef, ActorSelection, Props}
import it.necst.marc.infrastructure.actors.messages.{GiveMeAllResults, TakeAllResults}
import it.necst.marc.infrastructure.support.DataCompressor

/**
  * Created by andreacorna on 04/08/16.
  */
object SaveResultsActor {

  def props(loadBalancerRedisPath:String): Props = Props(new SaveResultsActor(loadBalancerRedisPath))

}


class SaveResultsActor(loadBalancerRedisPath:String) extends Actor{

  private var loadBalancerExternalRedis: ActorSelection = _
  private var caller: ActorRef = _

  private var currentCollection = ""

  override def preStart()  = {
    loadBalancerExternalRedis = context.system.actorSelection(loadBalancerRedisPath)
  }

  def receive: Receive = {
    case DownloadFiles(uri,collection) =>
      caller = sender()
      currentCollection = collection
      loadBalancerExternalRedis ! GiveMeAllResults(uri,self)

    case TakeAllResults(results) =>

      val outputDir = new File("/home/output/"+currentCollection)
      if(!outputDir.exists()) outputDir.mkdir()
      val files = createFile(results,outputDir.toPath,Nil)
      caller ! (files,outputDir)


  }

  private def createFile(toBeProcessed: List[(String,(Array[Byte],Int))],outputFolder: Path, files: List[File]): List[File] = {
    if(toBeProcessed.isEmpty) files
    else{

      val outputFile = outputFolder.resolve(toBeProcessed.head._1.replace("/","-")+".json")
      if(outputFile.toAbsolutePath.toFile.exists()) outputFile.toFile.delete()
      val file = Files.createFile(outputFile)

      val data = new String(DataCompressor.decompress(toBeProcessed.head._2._1,toBeProcessed.head._2._2))
      val bufferWriter = Files.newBufferedWriter(outputFile,Charset.forName("UTF-8"),StandardOpenOption.WRITE,StandardOpenOption.CREATE,StandardOpenOption.TRUNCATE_EXISTING)
      bufferWriter.write(data)
      bufferWriter.close()

      createFile(toBeProcessed.tail,outputFolder,file.toFile :: files)
    }
  }

}