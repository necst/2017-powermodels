package it.necst.marc.entrypoint.actors

import java.util.concurrent.atomic.AtomicLong

import akka.actor.{ActorRef, Actor, Props}
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.entrypoint.data.internal.NoFreeActorsException

object LoadBalancerEntryPoint{

  def props(actorsList: List[ActorRef]):Props = Props(new LoadBalancerEntryPoint(actorsList))

}

/**
 * Created by andrea on 19/08/15.
 */
class LoadBalancerEntryPoint(val actorsList: List[ActorRef]) extends Actor{

  private val logger = MarcLogger(getClass.getName)
  private val next = new AtomicLong(0)

  private var actorsFree: List[ActorRef] = actorsList
  private var workingActors: List[ActorRef] = Nil


  def receive: Receive = {
    case UserRequest(xmlString,phaseRequested,email,actorRef) =>
      logger.info("Received UserRequest message")
      try{
        val actor = chooseActor()
        actor ! UserRequest(xmlString,phaseRequested,email,actorRef)
      } catch {
        case e: Throwable =>
          //TODO notify no actors free
      }

    case FreeActor(ref) =>
      actorsFree = ref :: actorsFree
      workingActors = workingActors.filter(_!=ref)
  }


  private def chooseActor(): ActorRef = {
    if(actorsFree.nonEmpty){
      val actor = actorsFree((next.getAndIncrement % actorsFree.size).asInstanceOf[Int])
      actorsFree = actorsFree.filter(_!=actor)
      workingActors = actor :: workingActors
      actor
    }else{
      throw NoFreeActorsException
    }
  }

}
