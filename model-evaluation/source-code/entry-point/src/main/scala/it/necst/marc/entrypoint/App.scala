package it.necst.marc.entrypoint

import akka.actor.{ActorRef, ActorSelection, ActorSystem}
import com.typesafe.config.{Config, ConfigFactory, ConfigList, ConfigValue}
import it.necst.marc.data.support.{MarcLogger, NamesPhases}
import it.necst.marc.entrypoint.actors.{InternalWorker, LoadBalancerEntryPoint}
import it.necst.marc.entrypoint.data.internal.ConfigurationsMapException

import scala.collection.JavaConverters._
import scala.collection.JavaConversions._


/**
 * Created by Andrea Corna on 19/08/15.
 */
object App {


  private val logger = MarcLogger(getClass.getName)

  def main(args : Array[String]): Unit = {
    logger.info("Set up entry point")

    val entryPointConfiguration = ConfigFactory.load("entry-point_configuration")
    val phaseName = entryPointConfiguration.getString("name")

    val actorsNumber = entryPointConfiguration.getInt(phaseName+".number_internal_actors")

    val phasesLoadBalancers = loadMapRemoteAddresses(entryPointConfiguration.getConfig(phaseName+".remote_addresses"))
    val timeoutJobCFGReceiveState = entryPointConfiguration.getInt(phaseName+".timeout_Job_CFG_receive_state")
    val timeoutWaitResultState = entryPointConfiguration.getInt(phaseName+".timeout_result_receive_state")
    val beforePhasesList = for(phase <- entryPointConfiguration.getStringList(phaseName+".previous_phases").asScala.toList) yield {
      NamesPhases.withName(phase)
    }

    val configurationsMap = loadConfigurationsMap(entryPointConfiguration.getConfig(phaseName+".configurations_map"))

    val _systemInternalActors = ActorSystem("CommunicationActorSystem",ConfigFactory.load("internalWorker"))

    val actorsData = initializeInternalActors(actorsNumber,
                                              phasesLoadBalancers,timeoutJobCFGReceiveState,
                                              timeoutWaitResultState,beforePhasesList,
                                              _systemInternalActors,configurationsMap)

    val _systemLoadBalancer = ActorSystem("LoadBalancerSystem",ConfigFactory.load("loadBalancer"))
    val loadBalancer = _systemLoadBalancer.actorOf(LoadBalancerEntryPoint.props(actorsData._2),"loadBalancerEntryPoint")
    System.gc()

  }

  private def initializeInternalActors(actorsNumber: Int,
                                       phasesLoadBalancers: Map[NamesPhases.Value,String],timeoutJobCFGReceiveState: Int,
                                       timeoutWaitResultState: Int, beforePhasesList: List[NamesPhases.Value],
                                       systemInternalActors: ActorSystem,configurationsMap: Map[NamesPhases.Value,List[NamesPhases.Value]]): (List[String],List[ActorRef]) = {

    val mapLoadBalancers = createActorRef(phasesLoadBalancers,Map(),systemInternalActors)

    def createLists(currentIteration: Int, numberOfActors: Int,paths: List[String], actorsRef: List[ActorRef]):(List[String],List[ActorRef]) = {
      if(currentIteration >= numberOfActors) (paths,actorsRef)

      else{
        val newActorRef =
          systemInternalActors.actorOf(InternalWorker.props(phasesLoadBalancers,timeoutJobCFGReceiveState,
                                    timeoutWaitResultState,beforePhasesList,configurationsMap,mapLoadBalancers),"communicationActor"+currentIteration)
        val actorName = "/user/communicationActor"+currentIteration
        val newPaths = paths :+ actorName
        val newRefs = actorsRef :+ newActorRef
        createLists(currentIteration+1,numberOfActors,newPaths,newRefs)
      }
    }

    createLists(0,actorsNumber,Nil,Nil)
  }

  private def loadMapRemoteAddresses(configuration: Config): Map[NamesPhases.Value,String] = {

    def scanConfiguration(toBeProcessed: java.util.Set[java.util.Map.Entry[String, ConfigValue]], currentMap: Map[NamesPhases.Value,String]): Map[NamesPhases.Value,String]= {
      if (toBeProcessed.isEmpty) currentMap

      else {
        val newMap = currentMap + (NamesPhases.withName(toBeProcessed.head.getKey) -> toBeProcessed.head.getValue.render().replace("\"",""))
        scanConfiguration(toBeProcessed.tail, newMap)
      }
    }
    scanConfiguration(configuration.entrySet(), Map[NamesPhases.Value,String]())
  }

  private def loadConfigurationsMap(configuration: Config): Map[NamesPhases.Value,List[NamesPhases.Value]] = {
    def scanConfiguration(toBeProcessed: java.util.Set[java.util.Map.Entry[String, ConfigValue]],
                          map: Map[NamesPhases.Value,List[NamesPhases.Value]]):Map[NamesPhases.Value,List[NamesPhases.Value]] = {
      if(toBeProcessed.isEmpty) map

      else{
        val list = toBeProcessed.head.getValue match {
          case x: ConfigList =>
            (for(item <- x) yield NamesPhases.withName(new String(item.unwrapped().toString.replace("\"","")))).toList
          case _ =>
            throw ConfigurationsMapException
        }
        val newMap = map + (NamesPhases.withName(toBeProcessed.head.getKey) -> list)
        scanConfiguration(toBeProcessed.tail,newMap)
      }
    }
    scanConfiguration(configuration.entrySet(),Map())
  }


  private def createActorRef(toBeProcessed: Map[NamesPhases.Value,String],
                             temporaryMap: Map[NamesPhases.Value,ActorSelection],
                            actorSystem: ActorSystem):Map[NamesPhases.Value,ActorSelection] = {
    if(toBeProcessed.isEmpty) temporaryMap
    else{
      val actorRefLoadBalancer = actorSystem.actorSelection(toBeProcessed.head._2)
      val newMap = temporaryMap + (toBeProcessed.head._1 -> actorRefLoadBalancer)
      createActorRef(toBeProcessed.tail,newMap,actorSystem)
    }

  }

}
