package it.necst.marc.entrypoint

import it.necst.marc.data.support.MarcLogger

/**
 * Created by andrea on 23/08/15.
 */
object ParserTest {

    private val logger = MarcLogger(getClass.getName)
    def main(args : Array[String]): Unit = {
      logger.info("Set up entry point")

      //val configurations = ConfigurationParser("/home/andrea/Documents/Projects/maas/entry-point/src/main/scala/mpower_v2.xml") //TODO INCORRECT - ONLY FOR COMPILATION

      println("Parser terminated")
    }
}
