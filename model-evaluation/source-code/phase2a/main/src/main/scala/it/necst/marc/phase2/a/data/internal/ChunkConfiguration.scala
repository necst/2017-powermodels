package it.necst.marc.phase2.a.data.internal

import it.necst.marc.data.{FeatureMonotony, PartialStat}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase2.a.data.ConfigurationModel

import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
 * Created by andrea on 21/09/15.
 */


case class ChunkComputationResult(configurations: List[ChunkConfiguration],
                                  reportConfigurations:List[ConfigurationModel],
                                  reportFeatures:List[it.necst.marc.data.Feature],
                                  reportChunkIdentificationStats: List[PartialStat]){
  require(configurations != null, "configurations must be defined")
}

case class ChunkConfiguration(chunks: List[Chunk],code: Code) {
  require(chunks != null, "chunks must be defined")
  require(!code.isEmpty,"Code must be specified")
}

case class Chunk(output: RichDouble1DArray, phi: RichDouble2DArray, outputMonotony:FeatureMonotony.Value){
  require(output != null && output.size()>0, "Output vector must be defined.")
  require(phi != null && phi.size()._1>0 && phi.size()._2>0, "Phi matrix must be defined.")
  require(outputMonotony != null && outputMonotony != FeatureMonotony.NONE, "Output monotony must be defined")
}

