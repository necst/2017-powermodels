package it.necst.marc.phase2.a.computation

import it.necst.marc.data.FeatureMonotony
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase2.a.data.{Alpha, Model}
import it.necst.marc.phase2.a.data.internal.{Chunk, Configuration}

import scalaSci.RichDouble2DArray

/**
 * Created by andrea on 21/09/15.
 */
object ModelComputation {
  def apply(chunk: Chunk, configuration: Configuration): Option[Array[Double]] = {
    try {
      val output = chunk.output.toDoubleArray
      val phi = chunk.phi

      val qrMatrix = phi.pinv()
      val alphas = (qrMatrix *# new RichDouble2DArray(output.dropRight(output.length - qrMatrix.numColumns())).~).getCol(0)

      if(alphas.isEmpty){
        None
      } else {
        Some(alphas)
      }
    } catch {
      case e: Throwable =>
        None
    }
  }


  def apply(alphas: Array[Double],
            outputFeatureMonotony:FeatureMonotony.Value,
            configuration: Configuration,
             code: Code): Model = {

    def generateFeatureMatches() = {
      val output = for(i <- 1 to configuration.lags.autoregressive) yield {
        (configuration.outputFeature, i)
      }

      val exoFeatures = configuration.features.filterNot(f => configuration.ToBeRemovedForModelComputation.contains(f.column_index)).sortBy(_.column_index)
      val exogenous = for{
        f <- exoFeatures
        i <- 0 to configuration.lags.exogenous
      } yield {
        (f.name, i)
      }

      output ++ exogenous
    }

    val labels = generateFeatureMatches().toArray
    require(labels.length == alphas.length, "Missing alphas")

    val labeledAlphas = for(i<-alphas.indices) yield {
      val currentLabel = labels(i)
      Alpha(currentLabel._1, currentLabel._2, alphas(i))
    }

    Model(code, labeledAlphas.toList, outputFeatureMonotony)

    //TODO Replaced
    /*def createAlphasOutput(currentLagOutput: Int, currentIndex: Int,tmpAlphas: List[Alpha]): List[Alpha] ={
      if(currentLagOutput > configuration.lags.autoregressive) tmpAlphas
      else{
        val newList = tmpAlphas :+ Alpha(configuration.outputFeature,currentLagOutput,alphas(currentIndex))
        createAlphasOutput(currentLagOutput+1,currentIndex+1,newList)
      }
    }

    def createAlphasExogenous(toBeProcessed: List[it.necst.marc.data.Feature.ID],startIndex: Int,tmpAlphas: List[Alpha]): List[Alpha] = {
      if(toBeProcessed.isEmpty) tmpAlphas
      else{
        val newList = tmpAlphas ++ createAlphasFeature(startIndex,0,Nil,toBeProcessed.head)
        createAlphasExogenous(toBeProcessed.tail,startIndex+configuration.lags.exogenous,newList)
      }
    }

    def createAlphasFeature(currentIndex: Int,currentLag: Int, listAlphas: List[Alpha],currentFeature: String): List[Alpha] = {
      if(currentLag > configuration.lags.exogenous) listAlphas
      else{
        val newList = listAlphas :+ Alpha(currentFeature,currentLag,alphas(currentIndex))
        createAlphasFeature(currentIndex+1,currentLag+1,newList,currentFeature)
      }
    }

    val alphasOutput = createAlphasOutput(1,0,Nil)
    def isToKeep(x: it.necst.marc.data.Feature) =
        if(configuration.ToBeRemovedForModelComputation.contains(x.column_index)) false else true

    def sort = (x: it.necst.marc.data.Feature, y: it.necst.marc.data.Feature) => {x.column_index < y.column_index}

    val sortedFeatures = configuration.features.filter(isToKeep(_)).sortWith(sort)

    val featuresExogenous = for(feature <- sortedFeatures) yield feature.name
    val alphasExogenous = createAlphasExogenous(featuresExogenous,alphasOutput.size,Nil)

    Model(code,alphasOutput ++ alphasExogenous, outputFeatureMonotony)*/
  }



  def computeWeightedAlphas(listAlphas: List[Array[Double]]): Array[Double] = {
    if(listAlphas.isEmpty){
      return Array[Double]()
    }
    val results = sumAlphas(listAlphas,new Array[Double](listAlphas.head.length))

    def divide(toBeProcessed: Array[Double],currentIndex: Int): Array[Double] = {
      if(toBeProcessed.length == currentIndex) toBeProcessed
      else{
        toBeProcessed(currentIndex) = toBeProcessed(currentIndex) / listAlphas.length
        divide(toBeProcessed,currentIndex + 1)
      }
    }
    divide(results,0)

  }

  def parallelSquareError(dataset:Array[Array[Double]], alphas:List[Alpha], configuration: Configuration):(Double, Int) = {
    val featureMap = configuration.features.map{f => f.name -> f.column_index}.toMap

    val maxLag = Math.max(configuration.lags.autoregressive, configuration.lags.exogenous)

    val squareErrors = (for {
      i <- maxLag until dataset.length
    } yield {
      if (dataset(i)(configuration.timeFeatureIndex)-dataset(i-maxLag)(configuration.timeFeatureIndex) <= configuration.timeSplittingDiff) {
        val estimated = (for {
          alpha <- alphas
          row = dataset(i - alpha.lag)
          j = featureMap.get(alpha.feature) match {
            case Some(j) => j
            case None => throw new IllegalArgumentException("Feature not found.")
          }
        } yield {
          row(j) * alpha.alpha
        }).sum
        val real = dataset(i)(configuration.outputColumnIndex)

        Some(Math.pow(real - estimated, 2))
      } else {
        None
      }
    }).filter(_.isDefined).map(_.get)

    (squareErrors.sum, squareErrors.length)
  }

  def computeMSE(parallelResults:Seq[(Double, Int)]):Option[Double] = {
    val num = (for(r <- parallelResults) yield r._1).sum
    val den = (for(r <- parallelResults) yield r._2).sum

    if(den!=0){
      Some(num/den)
    } else {
      None
    }
  }

  private def invertArray(arrayToInvert: Array[Double]): Array[Double] = {

    def invert(toBeProcessed: Array[Double], list: List[Double]): Array[Double] = {
      if(toBeProcessed.isEmpty) list.toArray[Double]
      else{
        val newList = toBeProcessed.head :: list
        invert(toBeProcessed.tail,newList)
      }
    }
    invert(arrayToInvert,Nil)
  }

  private def deleteFeaturesFromChunk(toBeProcessed: List[Int],chunk: RichDouble2DArray): RichDouble2DArray = {

    if(toBeProcessed.isEmpty) chunk

    else{
      val index = toBeProcessed.head
      //filter function in order to delete feature from matrix
      def isIndexToKeep(n: Int) = if (n != index) true else false
      val newBatch = chunk.filterColumns(isIndexToKeep)
      deleteFeaturesFromChunk(toBeProcessed.tail,newBatch)
    }
  }

  private def sumAlphas(toBeProcessed: List[Array[Double]],sumArray: Array[Double]): Array[Double] = {
    if (toBeProcessed.isEmpty) sumArray
    else {
      val currentItem = toBeProcessed.head
      for (i <- 0 until currentItem.length) {
        sumArray(i) = sumArray(i) + currentItem(i)
      }
      sumAlphas(toBeProcessed.tail, sumArray)
    }

  }
}
