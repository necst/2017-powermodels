package it.necst.marc.phase2.a.actors

import it.necst.marc.data.{FeatureMonotony, PartialStat}
import it.necst.marc.phase1.post.data
import it.necst.marc.phase1.post.data.{ModelConfiguration}
import it.necst.marc.phase2.a.data.{PartialConfigurationModel, ConfigurationModel, Model}
import it.necst.marc.phase2.a.data.internal.{Configuration, Chunk, ChunkConfiguration, ChunkComputationResult}

import scalaSci.RichDouble2DArray

/**
 * Created by andrea on 21/09/15.
 */

sealed trait Message

//internal worker message
case class StartChunkIdentificationComputation() extends Message
case class ModelComputationComplete(models: List[Model],report: List[ConfigurationModel], modelComputationStats: List[PartialStat]) extends Message
case class ThrowsError(message: String) extends Message

//chunk identification collector
case class ComputeChunks(modelCfg: Configuration, modelInput: data.ModelConfiguration) extends Message
case class ComputationChunksComplete(reportData: PartialConfigurationModel) extends Message

//model computation collector
case class ModelComputed(model: Option[Model], mse: Option[Double]) extends Message

//ModelComputationActor messages
case class ComputeSingleModel(chunk: Chunk) extends Message
case class AlphasComputed(alphas: Option[Array[Double]]) extends Message


//TODO Bypassed code - review and delete
/*case class ComputeModels(dataToProcess: ChunkConfiguration) extends Message
case class ChunkIdentificationComplete(result: ChunkComputationResult) extends Message
case class StartModelComputation() extends Message
case class ManipulateBatch(configuration: ModelConfiguration) extends Message
case class ManipulationComplete(configuration: ModelConfiguration) extends Message
case class BatchManipulationComplete(configurations: List[ModelConfiguration],
                                     features: List[it.necst.marc.data.Feature]) extends Message*/