package it.necst.marc.phase2.a.actors.model_computation

import akka.actor.{Actor, Props}
import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.phase2.a.actors.{ThrowsError, AlphasComputed, ComputeSingleModel}
import it.necst.marc.phase2.a.computation.ModelComputation
import it.necst.marc.phase2.a.data.internal.Configuration
/**
 * Created by andrea on 22/09/15.
 */
object SingleModelActor{

  def props(configuration: Configuration): Props = Props(new SingleModelActor(configuration))

}

class SingleModelActor(val configuration: Configuration) extends Actor {

  private val logger = MarcLogger(getClass.getName)

  def receive: Receive = {
    case ComputeSingleModel(chunk) =>
      try{
        logger.info("Received ComputeSingleModel message for " + (for(i <- 0 until Math.min(3, chunk.output.size)) yield chunk.output(i)).mkString(";") + " " + chunk.outputMonotony)
        val alphas = ModelComputation(chunk,configuration)
        sender() ! AlphasComputed(alphas)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }


  }
}
