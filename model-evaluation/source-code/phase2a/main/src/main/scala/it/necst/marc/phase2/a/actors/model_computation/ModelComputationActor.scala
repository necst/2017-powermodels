//TODO Bypassed code - review and delete
/*package it.necst.marc.phase2.a.actors.model_computation

import akka.actor.{ActorRef, ActorSystem, Props, Actor}
import akka.routing.{BalancingPool}
import com.typesafe.config.ConfigFactory
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase2.a.actors._
import it.necst.marc.phase2.a.computation.ModelComputation
import it.necst.marc.phase2.a.data.internal.{Chunk, Configuration}

/**
 * Created by andrea on 22/09/15.
 */


object ModelComputationActor{

  def props(configuration: Configuration): Props = Props(new ModelComputationActor(configuration))
}

class ModelComputationActor(val configuration: Configuration) extends Actor{


  private val system = ActorSystem("ModelComputationActor",ConfigFactory.load("internal_actors"))

  private var waiting: Int = _

  private var routerActorRef: ActorRef = _

  private var replyTo: ActorRef = _

  private var listAlphas: List[Array[Double]] = Nil

  private var currentCode: Code = _


  def receive = initState

  def initState: Receive = {

    case ComputeModels(data) =>
      if(data.chunks.nonEmpty){
        replyTo = sender
        waiting = data.chunks.size
        currentCode = data.code
        routerActorRef = system.actorOf(BalancingPool(data.chunks.size).props(SingleModelActor.props(configuration)), name = "singleModelActor")
        sendComputationMessage(data.chunks)
        context.become(collectResultsState)
      }else{
        sender() ! ModelComputed(None)
        context.stop(self)
      }

  }

  def collectResultsState: Receive = {
    case AlphasComputed(alphas,outputFeatureMonotony) =>

      waiting = waiting - 1
      listAlphas = alphas :: listAlphas

      if(waiting == 0){
        val weightedAlphas = ModelComputation.computeWeightedAlphas(listAlphas)
        val model = ModelComputation(weightedAlphas,outputFeatureMonotony,configuration,currentCode)
        replyTo ! ModelComputed(Some(model))
        system.stop(routerActorRef)
        context.stop(self)
      }

    case ThrowsError(error) =>
      replyTo ! ThrowsError(error)
      system.stop(routerActorRef)
      context.stop(self)

  }
  
  private def sendComputationMessage(chunks: List[Chunk]) ={

    def sendMessage(toBeProcessed: List[Chunk]):Unit = {
      if(toBeProcessed.nonEmpty){
        routerActorRef ! ComputeSingleModel(toBeProcessed.head)
        sendMessage(toBeProcessed.tail)
      }
    }
    sendMessage(chunks)
  }

}*/

