package it.necst.marc.phase2.a.data

import it.necst.marc.data.{FeatureMonotony, Binding}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code

/**
 * Created by andreadamiani on 17/06/15.
 */
case class Result(models:List[Model], lags:LagsSpec, features:List[it.necst.marc.data.Feature])

case class Model(code:Code, alphas:List[Alpha], outputMonotony:FeatureMonotony.Value)

case class Alpha(feature:String, lag:Int, alpha:Double){
  require(!feature.isEmpty, "The feature must be identified")
  require(lag>=0, "The lag cannot be negative.")
}
