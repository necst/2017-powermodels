package it.necst.marc.phase2.a.actors

import akka.actor.{ActorSystem, Actor, Props, ActorRef}
import com.typesafe.config.Config
import it.necst.marc.data.{PerfStats, Performance, PartialStat, PartialStats}
import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.infrastructure.actors.messages.{ComputationError, InternalComputationCompleted, StartInternalComputation}
import java.net.URI
import it.necst.marc.infrastructure.support.{DataEncoder, DataCompressor, TraitInternalActor}
import it.necst.marc.phase2.a.actors.chunk_identification.ChunkIdentificationCollector
import it.necst.marc.phase2.a.computation.ModelComputation
import it.necst.marc.phase2.a.data._
import it.necst.marc.phase2.a.data.internal.{DataManagementStats, ChunkConfiguration, Configuration}
import rapture.json._
import rapture.json.jsonBackends.jawn._

/**
 * Created by Andrea Corna on 01/06/15.
 */

object InternalWorker extends TraitInternalActor{

  def  props(currentConfiguration:String,actorToReply: ActorRef,
             mapData: Map[URI,(Array[Byte],Int)], configFile: Config): Props = Props(new InternalWorker(currentConfiguration,actorToReply,mapData,configFile))

}


class InternalWorker(val currentConfiguration:String,
                     val actorToReply: ActorRef,
                     val mapData:Map[URI,(Array[Byte],Int)],
                     val configFile: Config) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private val system = ActorSystem("InternalWorker")

  private var internalConfiguration: Configuration = _

  private var currentConfig: it.necst.marc.phase2.a.data.Configuration = _
  private var performanceStats: List[PartialStat] = Nil
  private var initialTimestamp: Long = 0l
  def receive = initState


  def initState: Receive = {
    case StartInternalComputation() =>
      logger.info("Received StartInternalComputation message")
      try{
        initialTimestamp = System.currentTimeMillis()
        val dataString = new String(DataCompressor.decompress(mapData.head._2._1,mapData.head._2._2))
        val dataForPhase = Json.parse(dataString).as[it.necst.marc.phase1.post.data.Result]
        val finishDecompressionTimestamp = System.currentTimeMillis()

        currentConfig = Json.parse(currentConfiguration).as[it.necst.marc.phase2.a.data.Configuration]
        internalConfiguration = Configuration(dataForPhase.features,currentConfig)

        startChunkIdentification(dataForPhase)
        context.become(modelComputationCompleteState)
        performanceStats = PartialStat(DataManagementStats.DATA_DECOMPRESSION.toString,finishDecompressionTimestamp-initialTimestamp) :: performanceStats
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          val report = Json(Report(None,Some(error))).toString()
          actorToReply ! ComputationError(report,error)
          system.terminate()
          context.stop(self)
      }


    case ThrowsError(error) =>
      logger.info("Received ThrowsError message")
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      system.terminate()
      context.stop(self)

  }

  //TODO Bypassed code - review and delete
  /*def chunkIdentificationCompleteState: Receive = {
    case ChunkIdentificationComplete(result) =>
      logger.info("Receive ChunkIdentificationComplete message")
      startModelComputation(result.configurations,internalConfiguration)
      reportConfigurations = result.reportConfigurations
      reportFeatures = result.reportFeatures
      performanceStats = performanceStats ::: result.reportChunkIdentificationStats
      context.become(modelComputationCompleteState)

    case ThrowsError(error) =>
      logger.info("Received ThrowsError message")
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      system.terminate()
      context.stop(self)


  }*/

  def modelComputationCompleteState: Receive = {
    case ModelComputationComplete(models,reportConfigurations,modelStats) =>
      logger.info("Received ModelComputationComplete message")
      try{
        val timestamp = System.currentTimeMillis()
        val result = Result(models,internalConfiguration.lags,internalConfiguration.features)
        val resultString = Json(result).toString()
        val resultByte = DataCompressor.compress(resultString)
        val resultDim = DataEncoder.byteLength(resultString)
        val finishCompressionTimestamp = System.currentTimeMillis()


        val compressionStat = PartialStat(DataManagementStats.DATA_COMPRESSION.toString,timestamp-finishCompressionTimestamp)
        val performance = Performance(Some(PerfStats(timestamp-initialTimestamp,Some(PartialStats(compressionStat::modelStats++performanceStats)))))
        val reportString = Json(Report(Some(MathStats(reportConfigurations,performance)),None)).toString()

        val reportByte = DataCompressor.compress(reportString)
        val reportDim = DataEncoder.byteLength(reportString)


        actorToReply ! InternalComputationCompleted(reportByte,reportDim,resultByte,resultDim)
        system.terminate()
        context.stop(self)
      } catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          val report = Json(Report(None,Some(error))).toString()
          actorToReply ! ComputationError(report,error)
          system.terminate()
          context.stop(self)
      }



    case ThrowsError(error) =>
      logger.info("Received ThrowsError message")
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      system.terminate()
      context.stop(self)

  }

  private def startChunkIdentification(data: it.necst.marc.phase1.post.data.Result): Unit ={
    val numberOfActor = configFile.getInt(configFile.getString("name")+".number_chunk_identification_actors")
    val chunkCollector = system.actorOf(ChunkIdentificationCollector.props(data.batches, internalConfiguration, numberOfActor,configFile,data.features), "ChunkIdentificationCollector")
    chunkCollector ! StartChunkIdentificationComputation()
  }

  //TODO Bypassed code - review and delete
  /*private def startModelComputation(configurations: List[ChunkConfiguration],
                                    internalConfig: Configuration): Unit ={

    val numberOfActor = configFile.getInt(configFile.getString("name")+".number_model_computation_actors")
    val modelCollector = system.actorOf(ModelComputationCollector.props(configurations,internalConfig,numberOfActor),"ModelComputationCollector")
    modelCollector ! StartModelComputation()
  }*/


}
