package it.necst.marc.phase2.c.data

import it.necst.marc.data.Feature.ID
import it.necst.marc.data.{Performance, Binding, AbstractReport}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code

/**
 * Created by andreadamiani on 17/06/15.
 */

case class Report(content:Option[MathStats], error:Option[String]) extends AbstractReport(content, error)

case class MathStats(configurations:List[Entry],performance: Performance) extends it.necst.marc.data.MathStats{
  require(configurations.nonEmpty, "At least one configuration must be reported.")
  require(performance != null,"performance must be defined")
}

case class Entry(code:Code, rows:Int, distributions:List[Distribution]){
  //require(features.nonEmpty, "At least one feature must define the configuration.")
  require(rows>0, "Empty configurations must not be reported.")
  private val distFeatureInv = for(dist <- distributions) yield dist.feature
  require(distFeatureInv.size==distFeatureInv.toSet.size, "A feature can only have one distribution.")
}

case class Distribution(feature:ID, box:Box, std_dev:Double){
  require(feature!=null && feature.nonEmpty, "The feature ID must be defined")
  require(std_dev>=0, "A standard deviation must be non-negative.")
  require(box!=null, "Quartiles must be defined")
}

case class Box(min:Double, p25:Double, p50:Double, p75:Double, max:Double){
  require(min<=p25 && p25<=p50 && p50<=p75 && p75<=max, "The quartiles of the box plot must be consistently ordered.")
}
