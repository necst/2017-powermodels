package it.necst.marc.phase2.c.computation

import it.necst.marc.data.{FeatureMonotony, FeatureType, Binding, Feature}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase2.c.data.{Distribution, Box, Entry, Value}

import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
 * Created by andrea on 01/11/15.
 */
object MeanInput {


  def apply(features: List[Feature], dataset: RichDouble2DArray, indexTimeFeature: Int, indexOutputFeature: Int, clock: Double): List[Value] = {

    val timeColumn = dataset.getCol(indexTimeFeature)

    for(f <- features) yield {
      val currentCol = dataset.getCol(f.column_index)
      if(currentCol.length == 1){
        Value(f.name,currentCol(0))
      }else if(currentCol.length > 1){
        val zipped = timeColumn zip currentCol
        val sliding = zipped.sliding(2,1)
        val meanNumDen = f.specs.feature_type match {
          case FeatureType.INSTANTANEOUS =>
            sliding.foldLeft((0.0, 0.0)){
              (acc:(Double, Double), elem:Array[(Double,Double)]) =>
                val prev = elem(0)
                val curr = elem(1)
                val ticks = (curr._1 - prev._1)/clock
                (acc._1+curr._2*ticks, acc._2+ticks)
            }
          case FeatureType.CUMULATIVE =>
            sliding.foldLeft((0.0, 0.0)){
              (acc:(Double, Double), elem:Array[(Double,Double)]) =>
                val prev = elem(0)
                val curr = elem(1)
                val ticks = (curr._1 - prev._1)/clock
                (acc._1+(curr._2-prev._2)*ticks, acc._2+ticks)
            }
          case _ =>
            throw new IllegalArgumentException("Only INSTANTANEOUS and CUMULATIVE features are allowed in phase2.")
        }
        Value(f.name, meanNumDen._1/meanNumDen._2)
      }else{
        throw new IllegalArgumentException(s"feature ${f.name} is empty")
      }

    }


    //TODO Replaced
    /*def sort = (x: Feature, y: Feature) => {x.column_index < y.column_index}
    val orderedFeatures = features.sortWith(sort)

    val interestingCols = dataset.filterColumns(x => x != indexTimeFeature && x != indexOutputFeature)

    val timeColumn = dataset.filterColumns(_ == indexTimeFeature).getCol(0)

    val instantaneous = orderedFeatures.filter(_.specs.feature_type == FeatureType.INSTANTANEOUS)
    val cumulative = orderedFeatures.filter(_.specs.feature_type == FeatureType.CUMULATIVE)

    def createMeanInputsInstantaneous(toBeProcessed: List[Feature],meanInputs: Map[Int,Double]): Map[Int,Double] = {
      if(toBeProcessed.isEmpty) meanInputs
      else{
        val col = dataset.getCol(toBeProcessed.head.column_index)

        def computeMeanInputInstantaneous(tick: Double,currentRow: Int,sum: Double): Double = {
          if(currentRow == col.length) if(tick > 0 ) sum / tick else 0.0
          else{
            val diff = Math.abs(timeColumn(currentRow) - timeColumn(currentRow-1))
            val newTick = if(diff > 2*clock){
              tick
            }else{
              tick + diff/clock.toDouble
            }
            computeMeanInputInstantaneous(newTick,currentRow+1,sum+col(currentRow))
          }
        }
        val meanInput = computeMeanInputInstantaneous(0.0,1,col(0))
        createMeanInputsInstantaneous(toBeProcessed.tail,meanInputs+(toBeProcessed.head.column_index->meanInput))
      }
    }

    def createMeanInputsCumulative(toBeProcessed: List[Feature],meanInputs: Map[Int,Double]): Map[Int,Double] = {
      if(toBeProcessed.isEmpty) meanInputs
      else{
        val col = dataset.getCol(toBeProcessed.head.column_index)

        def computeMeanInputCumulative(tick: Double,currentRow: Int, first: Double,deltas: List[(Double,Double,Double)]): Double = {
          if(currentRow == col.length){
            val finalList = ((col(currentRow-1),first,tick) :: deltas).filter(_._3>0)
            if(finalList.nonEmpty){
              (for(item <- finalList) yield {
                (Math.abs(item._1-item._2))/item._3
              }).toArray.sum / finalList.length
            }
            else{
              0.0
            }
          }
          else{
            def check(x: Double,y: Double) =
              if((toBeProcessed.head.specs.monotony == FeatureMonotony.ASCENDANT && x-y >=0) ||

                (toBeProcessed.head.specs.monotony == FeatureMonotony.DESCENDANT && x-y <0)) true else false

            val data = if(check(col(currentRow),col(currentRow-1))){
              val newTick = tick + Math.abs(timeColumn(currentRow)-timeColumn(currentRow-1))/clock
              (newTick,deltas,first)
            }else{
              (0.0,(col(currentRow-1),first,tick) :: deltas ,col(currentRow))
            }
            computeMeanInputCumulative(data._1,currentRow+1,data._3,data._2)
          }
        }
        val meanInput = computeMeanInputCumulative(0.0,1,col(0),Nil)
        createMeanInputsCumulative(toBeProcessed.tail,meanInputs+(toBeProcessed.head.column_index->meanInput))
      }
    }

    val meanInputs =
      if(dataset.numRows() > 1) {
        val meanInputsMap = createMeanInputsCumulative(cumulative, Map[Int, Double]()) ++: createMeanInputsInstantaneous(instantaneous, Map[Int, Double]())
        new RichDouble1DArray((for (mean <- meanInputsMap.toSeq.sortWith(_._1 < _._1)) yield mean._2).toArray)
      }else new RichDouble1DArray(interestingCols.getRow(0))


    def createValues(toBeProcessed: List[Feature],currentIndex: Int,values: List[Value]): List[Value] = {
      if(toBeProcessed.isEmpty) values
      else{
        createValues(toBeProcessed.tail,currentIndex+1,Value(toBeProcessed.head.name,meanInputs(currentIndex)) :: values)
      }
    }

    createValues(orderedFeatures,0,Nil)*/
  }

}
