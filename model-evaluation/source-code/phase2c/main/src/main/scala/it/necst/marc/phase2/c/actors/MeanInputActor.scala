package it.necst.marc.phase2.c.actors

import akka.routing.BalancingPool
import com.typesafe.config.{ConfigValueFactory, ConfigFactory}
import it.necst.marc.data.Feature
import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.{Sample, ConfigurationCode}
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import akka.actor.{ActorSystem, ActorRef, Actor, Props}
import it.necst.marc.phase2.c.data._

import scala.annotation.tailrec
import scalaSci.RichDouble2DArray

/**
 * Created by andrea on 01/11/15.
 */

object MeanInputActor{

  def props(features: List[it.necst.marc.data.Feature],
            indexTimeFeature: Int, indexOutputFeature: Int,
            tick: Double,supercode:Code): Props = Props(new MeanInputActor(features,indexTimeFeature,indexOutputFeature,tick,supercode))
}

class MeanInputActor(features: List[it.necst.marc.data.Feature],
                     indexTimeFeature: Int, indexOutputFeature: Int,
                      tick: Double,
                     supercode:Code) extends Actor{

  private val logger = MarcLogger(getClass.getName)
  private val numberOfActors = 10 //TODO EXTERNALIZE

  private val waitingMessages:scala.collection.mutable.Map[Code, Int] = scala.collection.mutable.Map()
  private val waitingDistMessages:scala.collection.mutable.Map[Code, Int] = scala.collection.mutable.Map()

  private val ongoing:scala.collection.mutable.Map[Code,List[ConfigurationCode]] = scala.collection.mutable.Map()
  private val senders:scala.collection.mutable.Map[Code,ActorRef] = scala.collection.mutable.Map()

  private val computedMeans:scala.collection.mutable.Map[Code,List[Value]] = scala.collection.mutable.Map()
  private val computedDistributions:scala.collection.mutable.Map[Code, List[Distribution]] = scala.collection.mutable.Map()
  private val computedRows:scala.collection.mutable.Map[Code,Int] = scala.collection.mutable.Map()

  private var subAS:ActorSystem = _
  private var routerActorRef:ActorRef = null

  private var collector: ActorRef = null

  override def preStart() = {
    val config = ConfigFactory.load()
      .withValue("akka.loglevel", ConfigValueFactory.fromAnyRef("OFF"))
      .withValue("akka.stdout-loglevel", ConfigValueFactory.fromAnyRef("OFF"))
      .withValue("akka.logger-startup-timeout",ConfigValueFactory.fromAnyRef("30s"))
    subAS = ActorSystem(supercode,config)
    routerActorRef = subAS.actorOf(BalancingPool(numberOfActors).props(MeanInputBatchActor.props(features, indexTimeFeature, indexOutputFeature, tick)), s"meanInputBatchActor")
  }

  def receive: Receive = {
    case Compute(modelConfiguration,replyTo) =>
      try{
        collector = replyTo

        val code = modelConfiguration.codes.filter(_.subphase.equals(Phase2Subphase.C)).head.code
        //logger.info(s"Received Compute message for code $code")
        waitingMessages(code) = 0
        waitingDistMessages(code) = 0
        ongoing(code) = modelConfiguration.codes
        senders(code) = sender()
        computedMeans(code) = Nil
        computedDistributions(code) = Nil
        computedRows(code) = 0

        val subBatchSize = modelConfiguration.data.size/numberOfActors

        sendMessages(modelConfiguration.data.grouped(if(subBatchSize<=0) modelConfiguration.data.size else subBatchSize), code)
        sendDistMessages(modelConfiguration.dataForProcessing, features.filterNot(f => f.is_time.getOrElse(false) || f.column_index==indexOutputFeature), code)

      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          collector ! ThrowsError(error)
      }

    case BatchComputed(values, rows, code) if waitingMessages.contains(code) =>
      try {
        waitingMessages(code) = waitingMessages(code) - 1

        computedMeans(code) = computedMeans(code) match {
          case Nil => for(v <- values ) yield {
            Value(v.feature,v.mean*rows)
          }
          case list =>
            for (v <- list) yield {
              val toBeAdded = values.find(_.feature == v.feature).get
              Value(v.feature, v.mean + toBeAdded.mean * rows)
            }
        }

        computedRows(code) += rows

        checkComplete(code)
      } catch {
        case e:Throwable =>
          val error = ErrorStack(e)
          collector ! ThrowsError(error)
      }

    case DistributionComputed(dist, code) if waitingDistMessages.contains(code)=>
      try {
        waitingDistMessages(code) = waitingDistMessages(code) - 1

        computedDistributions(code) ::= dist

        checkComplete(code)
      } catch {
        case e:Throwable =>
          val error = ErrorStack(e)
          collector ! ThrowsError(error)
      }

    case ThrowsError(error) =>
      logger.info("Received ThrowsError message")
      collector ! ThrowsError(error)
      context.stop(routerActorRef)
      context.stop(self)
  }


  def sendMessages(toBeProcessed:Iterator[List[Sample]], code:Code): Unit = {
    for(elem <- toBeProcessed){
      waitingMessages(code) = waitingMessages(code) + 1
      routerActorRef ! ComputeBatch(self, it.necst.marc.toRichDouble2DArray(elem.map(_.sample.toArray).toArray), code)
    }
  }

  def sendDistMessages(dataset:RichDouble2DArray, toBeProcessed:List[Feature], code: Code): Unit = {
    for(elem <- toBeProcessed){
      waitingDistMessages(code) = waitingDistMessages(code) + 1
      routerActorRef ! ComputeDistribution(self, elem.name, it.necst.marc.toRichDouble1DArray(dataset.getCol(elem.column_index)), code)
    }
  }

  private def checkComplete(code: Code) = {
    if(waitingDistMessages(code) == 0 && waitingMessages(code) == 0) {
      val allCfgCodes = ongoing(code)
      val finalRows = computedRows(code)

      val means = computedMeans(code).map(v => Value(v.feature, v.mean / finalRows))
      val distributions = computedDistributions(code)

      val groupCodes = for (phase <- Phase2Subphase.groupingRules(Phase2Subphase.C)) yield allCfgCodes.find(_.subphase == phase).get.code
      senders(code) ! ValuesComputed(Mean(means, code, groupCodes), Entry(code, finalRows, distributions))
    }
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    subAS.terminate()
    subAS = null
  }
}
