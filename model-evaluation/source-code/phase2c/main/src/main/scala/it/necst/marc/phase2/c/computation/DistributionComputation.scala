package it.necst.marc.phase2.c.computation

import it.necst.marc.data.Feature
import it.necst.marc.phase2.c.data.{Distribution, Box}
import org.apache.commons.math3.stat.StatUtils

import scalaSci.RichDouble1DArray

/**
  * Created by andreadamiani on 08/02/16.
  */
object DistributionComputation {
  def apply(id:Feature.ID, feature:RichDouble1DArray) : Distribution = {
    val quartiles = {
        val col = feature.getv().sorted
        if(feature.length >=2 ){
          val min = col(0)
          val max = col(col.length-1)

          val quartileIndexLengths = (col.length-1).toDouble / 4
          val preciseQuartile = quartileIndexLengths.isWhole
          val p25 = if(preciseQuartile) col(quartileIndexLengths.toInt-1) else (col(quartileIndexLengths.ceil.toInt) + col(quartileIndexLengths.floor.toInt))/2
          val p50 = if(preciseQuartile) col(2*quartileIndexLengths.toInt-1) else (col((2*quartileIndexLengths).ceil.toInt) + col((2*quartileIndexLengths).floor.toInt))/2
          val p75 = if(preciseQuartile) col(3*quartileIndexLengths.toInt-1) else (col((3*quartileIndexLengths).ceil.toInt) + col((3*quartileIndexLengths).floor.toInt))/2

          Box(min, p25, p50, p75, max)
        }else{
          Box(col(0), col(0), col(0), col(0), col(0))
        }
      }

    val std_dev = Math.sqrt(StatUtils.variance(feature.getv()))

    Distribution(id, quartiles, std_dev)
  }
}
