package it.necst.marc.phase2.c.data

import it.necst.marc.data.Binding
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code

/**
 * Created by andreadamiani on 17/06/15.
 */
case class Result(means:List[Mean])

case class Mean(mean_values:List[Value], code:Code, groupCodes:List[Code])

case class Value(feature:it.necst.marc.data.Feature.ID, mean:Double)