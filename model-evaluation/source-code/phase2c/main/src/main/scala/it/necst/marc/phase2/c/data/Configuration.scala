package it.necst.marc.phase2.c.data

import it.necst.marc.data.AbstractConfiguration

/**
 * Created by andreadamiani on 14/07/15.
 */
case class Configuration(output:it.necst.marc.data.Feature.ID,tick: Double) extends AbstractConfiguration
