package it.necst.marc.phase2.c.actors

import akka.actor.{ActorRef, Actor, Props}
import akka.routing.BalancingPool
import it.necst.marc.data.PartialStat
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.ModelConfiguration
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase2.c.data.internal.MeanInputStats
import it.necst.marc.phase2.c.data.{Entry, Mean}

/**
 * Created by andrea on 01/11/15.
 */
object CollectorPhase2C{

  def props(modelConfigurations: List[ModelConfiguration],
            internalWorkerRef: ActorRef,
            features: List[it.necst.marc.data.Feature],
            totalActors: Int,
            indexTimeFeature: Int, indexOutputFeature: Int,
            tick: Double): Props = Props(new CollectorPhase2C(modelConfigurations,internalWorkerRef,features,totalActors,indexTimeFeature,indexOutputFeature,tick))

}

class CollectorPhase2C(modelConfigurations: List[ModelConfiguration],
                       internalWorkerRef: ActorRef,
                       features: List[it.necst.marc.data.Feature],
                       totalActors: Int,
                       indexTimeFeature: Int, indexOutputFeature: Int,
                       tick: Double) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private var waitingMessages: Int = 0
  private var means: List[Mean] = Nil
  private var entries: List[Entry] = Nil
  private val routerActorRef: scala.collection.mutable.Map[Code,ActorRef] = scala.collection.mutable.Map()

  private var initTimestamp: Long = 0l
  private var firstRequestSent = 0l
  private var allResponsesReceived = 0l
  private var actors: List[ActorRef] = Nil

  override def preStart() = {
    waitingMessages = modelConfigurations.size
    initTimestamp = System.currentTimeMillis()
    actors = for(mc <- modelConfigurations) yield{
      val supercode = (Phase2Subphase.C :: Phase2Subphase.groupingRules(Phase2Subphase.C)).map(phase => mc.codes.find(_.subphase == phase).get.code).mkString("||").replaceAll("[^\\w-_.*$+:@&=,!~';]","").replace(".","")

      context.system.actorOf(MeanInputActor.props(features,indexTimeFeature,indexOutputFeature,tick,supercode))

    }
  }

  def receive = initState

  def initState: Receive = {
    case StartCollectorPhase2CComputation() =>
      logger.info("Received StartCollectorPhase2CComputation message")
      try{
        firstRequestSent = System.currentTimeMillis()
        context.become(collectGeneratorsState)
        sendComputationRequests()
      }catch {
        case e: Throwable =>
          handleError(e)

      }

  }

  def collectGeneratorsState: Receive = {
    case ValuesComputed(mean,entry) =>
      logger.info(s"Received ValuesComputed for code ${entry.code}")
      try{
        waitingMessages = waitingMessages - 1
        means = mean :: means
        entries = entry :: entries

        if(waitingMessages == 0){
          logger.info("All values computed")
          allResponsesReceived = System.currentTimeMillis()
          val stats = List(PartialStat(MeanInputStats.FIRST_REQUEST_SENT.toString,firstRequestSent-initTimestamp),
            PartialStat(MeanInputStats.ALL_RESPONSES_RECEIVED.toString,allResponsesReceived-initTimestamp))
          internalWorkerRef ! ComputationMeansCompleted(means,entries,stats)
          for(ar <- routerActorRef){
            context.stop(ar._2)
          }
          context.stop(self)

        }
      }catch {
        case e:Throwable =>
          handleError(e)
      }


    case ThrowsError(error) =>
      logger.info("Received ThrowsError message")
      internalWorkerRef ! ThrowsError(error)
      for(ar <- routerActorRef){
        context.stop(ar._2)
      }
      context.stop(self)


  }

  private def handleError(e: Throwable) = {
    val error = ErrorStack(e)
    internalWorkerRef ! ThrowsError(error)
    for(ar <- routerActorRef){
      context.stop(ar._2)
    }
    context.stop(self)

  }

  private def sendComputationRequests() = {
    def recursiveRequest(toBeProcessed: List[ModelConfiguration],actors: List[ActorRef]): Unit = {
      if(toBeProcessed.nonEmpty){
        val model = toBeProcessed.head
        val supercode = (Phase2Subphase.C :: Phase2Subphase.groupingRules(Phase2Subphase.C)).map(phase => model.codes.find(_.subphase == phase).get.code).mkString("||").replaceAll("[^\\w-_.*$+:@&=,!~';]","!")
        routerActorRef.put(supercode, actors.head)
        actors.head ! Compute(toBeProcessed.head,self)
        recursiveRequest(toBeProcessed.tail,actors.tail)
      }
    }
    recursiveRequest(modelConfigurations,actors)
  }
}
