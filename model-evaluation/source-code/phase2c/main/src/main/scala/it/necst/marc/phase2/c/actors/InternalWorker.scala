package it.necst.marc.phase2.c.actors

import akka.actor.{ActorSystem, Actor, ActorRef, Props}
import com.typesafe.config.Config
import it.necst.marc.data.{PartialStat, PartialStats, PerfStats, Performance}
import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.infrastructure.actors.messages.{ComputationError, InternalComputationCompleted, StartInternalComputation}
import java.net.URI
import it.necst.marc.infrastructure.support.{DataEncoder, DataCompressor, TraitInternalActor}
import it.necst.marc.phase2.c.data.internal.DataManagementStats
import it.necst.marc.phase2.c.data.{MathStats, Report, Result}
import rapture.json._
import rapture.json.jsonBackends.jawn._
import sun.util.resources.cldr.es.CalendarData_es_EC

/**
 * Created by andrea on 01/06/15.
 */
object InternalWorker extends TraitInternalActor{

  def  props(currentConfiguration: String,actorToReply: ActorRef,
             mapData: Map[URI,(Array[Byte],Int)], configFile: Config): Props = Props(new InternalWorker(currentConfiguration,actorToReply,mapData,configFile))


}


class InternalWorker(val currentConfiguration:String,
                     val actorToReply: ActorRef,
                     val mapData:Map[URI,(Array[Byte],Int)],
                     val configFile: Config) extends Actor {

  private val logger = MarcLogger(getClass.getName)

  private val system = ActorSystem("InternalWorker")

  private var initialTimestamp: Long = 0l

  private var performanceStats: List[PartialStat] = Nil

  def receive = initState

  def initState: Receive = {
    case StartInternalComputation() =>
      logger.info("Received StartInternalComputation message")
      try{
        initialTimestamp = System.currentTimeMillis()
        val dataString = new String(DataCompressor.decompress(mapData.head._2._1,mapData.head._2._2))
        val dataForPhase = Json.parse(dataString).as[it.necst.marc.phase1.post.data.Result]
        val finishDecompressionTimestamp = System.currentTimeMillis()

        val configuration = Json.parse(currentConfiguration).as[it.necst.marc.phase2.c.data.Configuration]

        val epuratedFeatures = dataForPhase.features.filter(x => x.is_time.isEmpty && x.name != configuration.output)
        val indexTimeFeature = dataForPhase.features.find(_.is_time.isDefined).head.column_index
        val totalActors = configFile.getInt(configFile.getString("name")+".mean_input_actors")
        val collector = system.actorOf(CollectorPhase2C.props(dataForPhase.batches,self,epuratedFeatures,totalActors,indexTimeFeature, dataForPhase.features.find(_.name == configuration.output).get.column_index,configuration.tick))
        collector ! StartCollectorPhase2CComputation()
        context.become(collectorComputationCompleteState)
        performanceStats = PartialStat(DataManagementStats.DATA_DECOMPRESSION.toString,finishDecompressionTimestamp-initialTimestamp) :: performanceStats
      }catch {
        case e: Throwable =>
          val report = Json(Report(None,Some(ErrorStack(e)))).toString()
          actorToReply ! ComputationError(report,e.getMessage)
          system.terminate()
          context.stop(self)

      }


  }

  def collectorComputationCompleteState: Receive = {
    case ComputationMeansCompleted(means,entries,stats) =>
      logger.info("Received ComputationMeansCompleted message")

      val initDataCompressionTimestamp = System.currentTimeMillis()
      val result = Json(Result(means)).toString()
      val resultByte = DataCompressor.compress(result)
      val resultDim = DataEncoder.byteLength(result)

      val timestamp = System.currentTimeMillis()
      val compressionStat = PartialStat(DataManagementStats.DATA_COMPRESSION.toString,timestamp-initDataCompressionTimestamp)
      val performance = Performance(Some(PerfStats(timestamp-initialTimestamp,Some(PartialStats(compressionStat::performanceStats ++ stats)))))
      val report = Json(Report(Some(MathStats(entries,performance)),None)).toString()

      val reportByte = DataCompressor.compress(report)
      val reportDim = DataEncoder.byteLength(report)


      actorToReply ! InternalComputationCompleted(reportByte,reportDim,resultByte,resultDim)
      system.terminate()


    case ThrowsError(error) =>
      logger.info("Received ThrowsError message")
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      system.terminate()


  }

}
