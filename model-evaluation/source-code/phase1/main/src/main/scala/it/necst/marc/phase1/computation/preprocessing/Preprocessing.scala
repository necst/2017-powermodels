package it.necst.marc.phase1.computation.preprocessing

import it.necst.marc.infrastructure.support.CorruptionException
import it.necst.marc.phase1.data.internal.preprocessing.ReductionRule

import scala.annotation.tailrec
import scalaSci.{RichDouble1DArray, RichDouble2DArray}
import it.necst.marc.phase1.data.internal.coherence.{CoherenceRule}
import it.necst.marc.data.support.representability._
/**
 * @author andreadamiani
 */
object Preprocessing {
  def coherenceCorrection(sample: RichDouble1DArray, coherenceRules:List[R[CoherenceRule]]):(RichDouble1DArray,Option[List[R[CoherenceRule]]]) = {
    @tailrec def evaluateAll(row: Array[Double], toBeProcessed: List[R[CoherenceRule]],rulesActivated: List[R[CoherenceRule]]):(RichDouble1DArray,List[R[CoherenceRule]]) = {
        if (toBeProcessed.isEmpty) (new RichDouble1DArray(row),rulesActivated)
        else {
          val rule = toBeProcessed.head
          val newSample = rule.obj(row)
          if(newSample.contains(Double.NaN)) throw new CorruptionException("The rule " + rule + " caused dataset corruption.")
          val newActivatedRules = if(newSample.sameElements(row)){
            rulesActivated
          }else{
            rulesActivated :+ toBeProcessed.head
          }
          evaluateAll(newSample, toBeProcessed.tail,newActivatedRules)
        }
    }
    val evaluationResults = evaluateAll(sample.getv(), coherenceRules,Nil)
    (evaluationResults._1,if(evaluationResults._2.isEmpty) None else Option(evaluationResults._2))
  }
  
  def incoherentMask(sample: RichDouble1DArray, coherenceRules: List[R[CoherenceRule]]):(Option[RichDouble1DArray],Option[R[CoherenceRule]]) = {
    @tailrec def evaluateAll(underTest:RichDouble1DArray, toBeProcessed: List[R[CoherenceRule]]):(Boolean,Option[R[CoherenceRule]]) = {
        if (toBeProcessed.isEmpty) (true,None)
        else if (toBeProcessed.head.condition(underTest)) (false,Option(toBeProcessed.head))
        else evaluateAll(underTest, toBeProcessed.tail)
    }
    val resultEvaluatation = evaluateAll(sample, coherenceRules)
    if (resultEvaluatation._1)
      (Option(sample),None)
    else 
      (None,resultEvaluatation._2)
  }

  def outOfBoundMask(sample: RichDouble1DArray, featuresMapping: Map[String,Int],bounds:Map[String, (Option[Double], Option[Double])]):(Option[RichDouble1DArray],Option[String]) = {
    def checkAllBounds(underTest:RichDouble1DArray, bounds:Map[String, (Option[Double],Option[Double])] = bounds):(Boolean,Option[String]) = checkAllBoundsRecursive(underTest, bounds.toList)
    @tailrec def checkAllBoundsRecursive(underTest:RichDouble1DArray, toBeProcessed:List[(String, (Option[Double],Option[Double]))]):(Boolean,Option[String]) = {
      if (toBeProcessed.isEmpty) 
        return (true,None)
      else {
        val currentBounds = toBeProcessed.head._2
        val currentIndex = featuresMapping(toBeProcessed.head._1)
        if (currentBounds._1.isDefined)
           if (underTest(currentIndex) < currentBounds._1.get)
             return (false,Option(toBeProcessed.head._1))
        if (currentBounds._2.isDefined)
           if (underTest(currentIndex) > currentBounds._2.get)
             return (false,Option(toBeProcessed.head._1))
        return checkAllBoundsRecursive(underTest, toBeProcessed.tail)
      }
    }

    val resultCheckBounds = checkAllBounds(sample, bounds)
    if (resultCheckBounds._1)
      (Option(sample),None)
    else
      (None,resultCheckBounds._2)
  }
  
  def granularityReduction(sample: RichDouble1DArray, reductionRules: List[R[ReductionRule]]):(RichDouble1DArray,Option[List[R[ReductionRule]]]) = {
    @tailrec def applyAllReductions(vec: RichDouble1DArray, toBeProcessed:List[R[ReductionRule]], reductionActivated: List[R[ReductionRule]]):(RichDouble1DArray, List[R[ReductionRule]]) = {
      if (toBeProcessed.isEmpty) (vec, reductionActivated)
      else {
        val reduction = toBeProcessed.head
        val resultReduction = reduction(vec)
        if(resultReduction.getv().contains(Double.NaN)) throw new CorruptionException("The reduction rule " + reduction + "caused dataset corruption.")
        val newReductionActivated = if(resultReduction.equals(vec)){
          reductionActivated
        }else{
          reductionActivated :+ toBeProcessed.head
        }
        applyAllReductions(resultReduction,toBeProcessed.tail,newReductionActivated)
      }
    }
    val applyReductionResult = applyAllReductions(sample, reductionRules, Nil)
    (applyReductionResult._1,if(applyReductionResult._2.isEmpty) None else Option(applyReductionResult._2))
  }
}