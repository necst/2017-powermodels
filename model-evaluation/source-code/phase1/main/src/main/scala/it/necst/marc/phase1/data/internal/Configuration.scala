package it.necst.marc.phase1.data.internal

import it.necst.marc.data.Feature
import it.necst.marc.phase1.computation.CategoricalExpansion
import it.necst.marc.phase1.computation.dataManipulation.Operation
import it.necst.marc.phase1.data.internal.featureSelection.FusionRule
import it.necst.marc.phase1.data.internal.coherence.support.{CorrectionParser, ConditionParser}
import it.necst.marc.phase1.data.internal.coherence.CoherenceRule
import it.necst.marc.phase1.data.internal.coherence.CoherenceRule._
import it.necst.marc.data.support.representability._
import it.necst.marc.phase1.data.internal.featureSelection.support.GeneratorParser
import it.necst.marc.phase1.data.internal.preprocessing.ReductionRule

import scala.collection.SortedSet

/**
 * Created by Andrea on 12/08/2015.
 */

class Configuration( val features: List[Feature],
                     val timeFeature:R[Int],
                     val featureMappings:Map[String,Int],
                     val bounds:Map[String, (Option[Double], Option[Double])],
                     val reductions:List[R[ReductionRule]],
                     val coherenceRules:List[R[CoherenceRule]],
                     val manipulations:List[(R[Operation],Map[Operation.AttributeName,String])],
                     val fusions:List[R[FusionRule]],
                     val excluded:List[String]
                     ){

  val selectedColumnRanges = computeBounds(Set((for (selection <- fusions) yield selection.obj.destination.name): _*) ++
                                           Set((for (feature <- featureMappings.toList; if(!excluded.contains(feature._1))) yield feature._1): _*) )

  private def computeBounds(colsFromGfg:Set[Feature.ID]):SortedSet[(Int,Int)] = {

    val cols = SortedSet(colsFromGfg.flatMap{f =>
       CategoricalExpansion.obtainExpandedFeatures(f, features) ::: (fusions.find(_.destination.name == f) match {
      case Some(ef) =>
        ef.destination :: Nil
      case _ =>
        Nil
    })}
      .map(_.column_index)
      .toSeq:_*)

    def recursiveComputeUpperBound(cols:SortedSet[Int], prev:Int):SortedSet[Int] = {
      if (cols.size == 0) SortedSet(prev)
      else if (cols.head - prev > 1) cols+prev
      else recursiveComputeUpperBound(cols.tail, cols.head)
    }

    def recursiveComputeBounds(cols:SortedSet[Int], alreadyComputed: List[(Int,Int)]):List[(Int,Int)] = {
      if(cols.size == 0) alreadyComputed
      else {
        val upperBound = recursiveComputeUpperBound(cols.tail, cols.head)
        if (upperBound.size == 1) (cols.head, upperBound.head) :: alreadyComputed
        else recursiveComputeBounds(upperBound.tail, (cols.head, upperBound.head) :: alreadyComputed)
      }
    }

    SortedSet(recursiveComputeBounds(cols, Nil): _*)(TupleOrdering)
  }
}

object TupleOrdering extends Ordering[(Int,Int)]{
  def compare(a:(Int,Int),b:(Int,Int)) = {
    if (a._1 != b._1) a._1 compare b._1
    else a._2 compare b._2
  }
}


object Configuration{
  def apply(features: List[it.necst.marc.data.Feature], phaseConfiguration: it.necst.marc.phase1.data.Configuration, short: Boolean):Configuration ={
    apply(features, phaseConfiguration, 0, short)
  }

  def apply(features: List[it.necst.marc.data.Feature], phaseConfiguration: it.necst.marc.phase1.data.Configuration, columnsAdded: Int, short: Boolean):Configuration ={
    if(!short){
      apply(features, phaseConfiguration, columnsAdded)
    } else {
      val timeFeature = features.find(_.is_time.getOrElse(false)).orNull
      val featureMappings = (for(feature <- features) yield (feature.name, feature.column_index)).toMap
      val bounds = (for(feature <- features) yield (feature.name, (feature.specs.bounds.lower, feature.specs.bounds.upper))).toMap
      val reductions = Nil
      val coherenceRules = Nil
      val manipulations = for(manipulation <- phaseConfiguration.manipulations.getOrElse(Nil)) yield {
        val operation = Operation.withNameInsensitive(manipulation.function_name)
        val parameters = manipulation.parameters.getOrElse(Nil) map (x=> x.name -> x.value) toMap

        ((manipulation.function_name + parameters.mkString("(",",",")")) @: operation) -> parameters
      }
      val fusionParser = new GeneratorParser(features)

      val fusions = for(fusion <- phaseConfiguration.fusions.getOrElse(Nil)) yield (fusion.destination.name + ": " + fusion.generator) @: new FusionRule(if(columnsAdded == 0) {fusion.destination} else {Feature(fusion.destination.name, fusion.destination.column_index + columnsAdded, fusion.destination.is_time, fusion.destination.specs)}, fusionParser(fusion.generator).get)

      val excludedNonExpanded = phaseConfiguration.excluded.getOrElse(Nil)

      val excluded = excludedNonExpanded.flatMap{e =>
        if(featureMappings.contains(e)) {
          e :: Nil
        } else {
          CategoricalExpansion.obtainExpandedFeatures(e, features).map(_.name)
        }
      }

      new Configuration(features,timeFeature.name @: timeFeature.column_index, featureMappings, bounds, reductions, coherenceRules, manipulations, fusions, excluded)
    }
  }

  def apply(features: List[it.necst.marc.data.Feature], phaseConfiguration: it.necst.marc.phase1.data.Configuration, columnsAdded: Int = 0):Configuration = {
    val timeFeature = features.find(_.is_time.getOrElse(false)).orNull
    val featureMappings = (for(feature <- features) yield (feature.name, feature.column_index)).toMap
    val bounds = (for(feature <- features) yield (feature.name, (feature.specs.bounds.lower, feature.specs.bounds.upper))).toMap
    val reductions = for(rule <- phaseConfiguration.reduction.getOrElse(Nil)) yield (rule.feature + "->" + rule.mode.toString) @: (rule.mode match {
      case ReductionRule.Mode.Linear | ReductionRule.Mode.Log =>
        ReductionRule(featureMappings(rule.feature), rule.mode, bounds(rule.feature)._1.get , bounds(rule.feature)._2.get, rule.steps.getOrElse(1))
      case ReductionRule.Mode.Custom =>
        ReductionRule(featureMappings(rule.feature), rule.intervals.get:_*)
    })

    val conditionParser = new ConditionParser(features)
    val correctionParser = new CorrectionParser(features)
    val coherenceRules = for(rule <- phaseConfiguration.coherenceRules.getOrElse(Nil)) yield (rule.test + "==>" + rule.actuation) @: (conditionParser(rule.test).get ==> correctionParser(rule.actuation).get)

    val manipulations = for(manipulation <- phaseConfiguration.manipulations.getOrElse(Nil)) yield {
      val operation = Operation.withNameInsensitive(manipulation.function_name)
      val parameters = manipulation.parameters.getOrElse(Nil) map (x=> x.name -> x.value) toMap

      ((manipulation.function_name + parameters.mkString("(",",",")")) @: operation) -> parameters
    }

    val fusionParser = new GeneratorParser(features)
    
    val fusions = for(fusion <- phaseConfiguration.fusions.getOrElse(Nil)) yield (fusion.destination.name + ": " + fusion.generator) @: new FusionRule(if(columnsAdded == 0) {fusion.destination} else {Feature(fusion.destination.name, fusion.destination.column_index + columnsAdded, fusion.destination.is_time, fusion.destination.specs)}, fusionParser(fusion.generator).get)

    val excludedNonExpanded = phaseConfiguration.excluded.getOrElse(Nil)

    val excluded = excludedNonExpanded.flatMap{e =>
      if(featureMappings.contains(e)) {
        e :: Nil
      } else {
        CategoricalExpansion.obtainExpandedFeatures(e, features).map(_.name)
      }
    }

    new Configuration(features,timeFeature.name @: timeFeature.column_index, featureMappings, bounds, reductions, coherenceRules, manipulations, fusions, excluded)

  }
}