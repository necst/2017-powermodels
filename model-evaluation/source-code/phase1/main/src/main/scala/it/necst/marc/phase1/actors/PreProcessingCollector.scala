package it.necst.marc.phase1.actors

import akka.actor.{Actor, ActorRef, Props}
import akka.routing.BalancingPool
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.data.{PartialStat}
import it.necst.marc.phase1.data.internal.PreProcessingStats
import it.necst.marc.phase1.data.Entry
import scalaSci.{RichDouble1DArray, RichDouble2DArray}

object PreProcessingCollector {

  def props(data: RichDouble2DArray, numberOfActors: Int, config: it.necst.marc.phase1.data.internal.Configuration): Props = Props(new PreProcessingCollector(data,numberOfActors,config))


}

class PreProcessingCollector(var data: RichDouble2DArray,
                             numberOfActors: Int,
                             config: it.necst.marc.phase1.data.internal.Configuration) extends Actor{


  private val logger = MarcLogger(getClass.getName)

  private var size = data.numRows()

  private var mainActorRef: ActorRef = _
  
  private var routerActorRef: ActorRef = _


  private var amended:List[Entry] = Nil
  private var excluded:List[Entry] = Nil
  private var out_of_bound:List[Entry] = Nil
  private var granularity: List[Entry] = Nil

  private var samplesToKeep: List[Array[Double]] = Nil

  private var initialTimestamp: Long = 0l
  private var firstRequestSent: Long = 0l
  private var allResponsesReceived: Long = 0l


  override def preStart() = {
    initialTimestamp =  System.currentTimeMillis()
  }

  def receive: Receive = {
    case StartCollectorComputation() =>
      try{
        mainActorRef = sender()
        routerActorRef = context.system.actorOf(BalancingPool(numberOfActors).props(PreProcessingActor.props(config)), name = "PreProcessingActor")
        sendDataToCompute(data.getNativeMatrixRef())
      }catch{
        case e:Throwable =>
          val error = ErrorStack(e)
          mainActorRef ! ThrowsError(error)
          context.stop(routerActorRef)
          context.stop(self)
      }

      
    case ProcessedComplete(sample,row,amendedList,incoherentList,outOfBoundList,granularityReductions) =>
      try{
        size = size - 1
        amended = amendedList ++ amended
        excluded = incoherentList ++ excluded
        out_of_bound = outOfBoundList ++ out_of_bound
        granularity = granularityReductions ++ granularity
        if(sample.nonEmpty){
          samplesToKeep = sample.get.getv() :: samplesToKeep
        }
        if(size == 0){
          allResponsesReceived = System.currentTimeMillis()
          logger.info("SIZE = 0")
          require(samplesToKeep.nonEmpty,"all samples were deleted during preprocessing")
          data = new RichDouble2DArray(samplesToKeep.toArray)
          logger.info("CREATE ")
          val preProcessingCompleteTimestamp = System.currentTimeMillis()
          val intermediateStats = List(PartialStat(PreProcessingStats.FIRST_REQUEST_SENT.toString,firstRequestSent-initialTimestamp),
            PartialStat(PreProcessingStats.ALL_RESPONSES_RECEIVED.toString,allResponsesReceived-initialTimestamp),
            PartialStat(PreProcessingStats.PREPROCESSING_COMPLETED.toString,preProcessingCompleteTimestamp-initialTimestamp))
          mainActorRef ! PreProcessingComplete(data,intermediateStats,amended,excluded,out_of_bound,granularity)
          context.stop(routerActorRef)
          context.stop(self)
        }
      }catch {
        case e:Throwable =>
          val error = ErrorStack(e)
          mainActorRef ! ThrowsError(error)
          context.stop(routerActorRef)
          context.stop(self)
      }


    case ThrowsError(error) =>
      mainActorRef ! ThrowsError(error)
      context.stop(routerActorRef)
      context.stop(self)

  }


  private def sendDataToCompute(toBeProcessed: Array[Array[Double]]): Unit ={
    firstRequestSent = System.currentTimeMillis()
    for(i <- toBeProcessed.indices){
      routerActorRef ! PreProcessData(new RichDouble1DArray(toBeProcessed(i)),i,self)
    }

  }

}