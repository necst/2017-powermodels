package it.necst.marc.phase1.computation.dataManipulation

import enumeratum._
import it.necst.marc.data.Feature
import it.necst.marc.phase1.computation.CategoricalExpansion
import it.necst.marc.phase1.computation.dataManipulation.Operation.AttributeName
import scalaSci.RichDouble2DArray
import OperationRequirements._

abstract class OperationImplementation {
  def apply(dataset:RichDouble2DArray, attrs:Map[Operation.AttributeName,String],features: List[Feature]):(RichDouble2DArray, List[Feature])
}

class OperationRequirements(val requirements: Set[OperationRequirement]){
  def +(r:OperationRequirements) = OperationRequirements(this.requirements ++ r.requirements)
}

object OperationRequirements{
  def apply(requirements: OperationRequirement*) = new OperationRequirements(requirements.toSet)
  def apply(requirements: Set[OperationRequirement]) = new OperationRequirements(requirements)
  val NONE = OperationRequirements()
}

sealed abstract class OperationRequirement(val implementation:OperationImplementation)
object OperationRequirement{
  implicit def toOperationRequirements(from:OperationRequirement) = OperationRequirements(from)
}
case object TIME_SORTING extends OperationRequirement(new OperationImplementation {
  override def apply(dataset: RichDouble2DArray, attrs: Map[AttributeName, String], features: List[Feature]): (RichDouble2DArray, List[Feature]) = {
    val timeFeature = features.find(_.is_time.getOrElse(false))
    require(timeFeature.isDefined, "Time feature not defined!")
    val newDataset = it.necst.marc.toStandardDouble2DArray(dataset).sortBy(_(timeFeature.get.column_index))

    (it.necst.marc.toRichDouble2DArray(newDataset), features)
  }
})

case object CATEGORICAL_EXPANSION extends OperationRequirement(new OperationImplementation {
  override def apply(dataset: RichDouble2DArray, attrs: Map[AttributeName, String], features: List[Feature]): (RichDouble2DArray, List[Feature]) = CategoricalExpansion(dataset,features)
})

sealed abstract class Operation(val implementation:OperationImplementation, val preconditions:OperationRequirements = NONE) extends EnumEntry
object Operation extends Enum[Operation] {

  import OperationRequirement._

  type AttributeName = String
  type ID = String

  val values = findValues

  //Available Operations
  case object Standardization extends Operation(StandardizationImplementation)
  case object QuantizationCorrection extends Operation(QuantizationCorrectionImplementation, TIME_SORTING + CATEGORICAL_EXPANSION)
}