package it.necst.marc.phase1.computation.featureSelection

import it.necst.marc.infrastructure.support.CorruptionException
import it.necst.marc.phase1.data.internal.featureSelection.FusionRule

import scala.annotation.tailrec
import scala.collection.SortedSet
import scala.collection.mutable.ListBuffer
import scalaSci.{RichDouble1DArray, RichDouble2DArray}
import it.necst.marc.data.support.representability._
/**
 * Created by andrea on 24/08/15.
 */
object FeatureSelection {

  def apply(dataset: RichDouble2DArray, fusionRules: List[R[FusionRule]],selections: SortedSet[(Int, Int)]): RichDouble2DArray = {

    def applyToAllDataset(dataset: RichDouble2DArray, rules: List[R[FusionRule]]): RichDouble2DArray = {

      @tailrec def applyAllRules(line: RichDouble1DArray, prev: RichDouble1DArray, toBeProcessed: List[R[FusionRule]]): Array[Double] = {
        if (toBeProcessed.isEmpty) line.getv()
        else applyAllRules(toBeProcessed.head(line,prev), line, toBeProcessed.tail)
      }

      val fused = for (line <- dataset.getv()) yield
      applyAllRules(new RichDouble1DArray(line), new RichDouble1DArray(line.length), rules)

      if(fused.flatten.contains(Double.NaN)) throw new CorruptionException("Feature fusion causes dataset corruption.")
      else new RichDouble2DArray(fused)
    }

    def selectAll(dataset: RichDouble2DArray, selections: SortedSet[(Int, Int)]): RichDouble2DArray = {

      @tailrec def selectAllRecursive(building: RichDouble2DArray, toBeProcessed: List[(Int, Int)]): RichDouble2DArray = {
        if (toBeProcessed.isEmpty) building
        else {
          val prepends = dataset(0, dataset.numRows() - 1, toBeProcessed.head._1, toBeProcessed.head._2)
          selectAllRecursive(building <<< prepends, toBeProcessed.tail)
        }
      }

      val reverseSelList = selections.toList.reverse

      val first = reverseSelList.head

      val base = dataset(0, dataset.numRows() - 1, first._1, first._2)

      new RichDouble2DArray(selectAllRecursive(new RichDouble2DArray(base.getv()), reverseSelList.tail).getv())
    }

    selectAll(applyToAllDataset(dataset, fusionRules), selections)



  }

  //For parallel fusion
  //Returns a map Column_Index -> Column array containing the columns that have to be added to the dataset due to fusion.
  def fuse(dataset: RichDouble2DArray, fusionRule: List[R[FusionRule]]) = {
    def applyToAllDataset(dataset: RichDouble2DArray, rules: List[R[FusionRule]]): Map[Int, Array[Double]] = {

      @tailrec def applyAllRules(line: RichDouble1DArray, prev: RichDouble1DArray, toBeProcessed: List[R[FusionRule]], result:Map[Int,Double] = Map()): Map[Int,Double] = {
        if (toBeProcessed.isEmpty) result
        else {
          val rule = toBeProcessed.head
          val res = rule.applyWithSmallOutput(line, prev)
          if (res == Double.NaN) throw new CorruptionException("Fusion rule " + rule + " caused dataset corruption.")
          applyAllRules(line, prev, toBeProcessed.tail, result + (rule.destination.column_index -> res))
        }
      }

      val fusionRes = for (i <- 0 until dataset.numRows()) yield
        applyAllRules(new RichDouble1DArray(dataset.getRow(i)), if(i==0){new RichDouble1DArray(dataset.numColumns())}else{new RichDouble1DArray(dataset.getRow(i-1))}, rules)

      fusionRes.foldLeft((for(r <- fusionRule) yield (r.destination.column_index) -> ListBuffer[Double]()).toMap){
        (accumulator:Map[Int,ListBuffer[Double]], elem:Map[Int,Double]) =>
          for(entry <- elem) yield {
            val accEntry:ListBuffer[Double] = accumulator(entry._1).+=(entry._2)
            entry._1 -> accEntry
          }
      }.map{case (k,v) => k -> v.toArray}
    }

    applyToAllDataset(dataset, fusionRule)
  }

  def apply(dataset: RichDouble2DArray, fusedCols: Map[Int, Array[Double]], selections: SortedSet[(Int, Int)]): RichDouble2DArray = {
    val EMPTY_COL = if(fusedCols.nonEmpty) (for (i<-0 until dataset.numRows()) yield 0.0).toArray[Double] else Array[Double]()

    @tailrec def reconstructRecursive(dataset: RichDouble2DArray, toBeProcessed:List[(Int,Array[Double])]): RichDouble2DArray = {
      if (toBeProcessed.isEmpty) dataset
      else {
        val underProcess = toBeProcessed.head
        require(underProcess._1>=dataset.numColumns(), "MALFORMED FUSION RULE, CAUSING OVERWRITE ON COL #"+underProcess._1)
        @tailrec def fillWithEmptyColsRecursive(dataset:RichDouble2DArray, count:Int):RichDouble2DArray = {
          if(count<=0) dataset
          else fillWithEmptyColsRecursive(dataset.CA(EMPTY_COL), count-1)
        }
        val newDataset = fillWithEmptyColsRecursive(dataset, underProcess._1 - dataset.numColumns())
        reconstructRecursive(newDataset.CA(underProcess._2), toBeProcessed.tail)
      }
    }

    val fusedDataset = reconstructRecursive(dataset, fusedCols.seq.toList.sortBy(_._1))

    def selectAll(dataset: RichDouble2DArray, selections: SortedSet[(Int, Int)]): RichDouble2DArray = {

      def getFromRange(dataset:RichDouble2DArray, range: (Int,Int)):RichDouble2DArray = {
        if(range._1 == range._2){
          new RichDouble2DArray(dataset.getCol(range._1))
        } else {
          dataset(0, dataset.numRows() - 1, range._1, range._2)
        }
      }

      @tailrec def selectAllRecursive(building: RichDouble2DArray, toBeProcessed: List[(Int, Int)]): RichDouble2DArray = {
        if (toBeProcessed.isEmpty) building
        else {
          val prepends = getFromRange(dataset, toBeProcessed.head)
          selectAllRecursive(building <<< prepends, toBeProcessed.tail)
        }
      }

      val reverseSelList = selections.toList.reverse

      val first = reverseSelList.head

      val base = getFromRange(dataset, first)

      new RichDouble2DArray(selectAllRecursive(new RichDouble2DArray(base.getv()), reverseSelList.tail).getv())
    }

    selectAll(fusedDataset, selections)
  }
}
