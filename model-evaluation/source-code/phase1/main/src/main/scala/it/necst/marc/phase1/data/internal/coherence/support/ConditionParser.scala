package it.necst.marc.phase1.data.internal.coherence.support

import condition._
import it.necst.marc.data.Feature

import scala.util.parsing.combinator.JavaTokenParsers

class ConditionParser(
    val mappings:List[Feature]
        ) extends JavaTokenParsers {
  def formula:Parser[Formula] =     repsep(term, "||")             ^^ (x => new Formula(x))
  def term:Parser[Term] =           repsep(factor, "&&")           ^^ (x => new Term(x))
  def factor:Parser[Factor] = (
                                    direct_expression              ^^ (x => new Factor(x))
                                  | neg_expression                 ^^ (x => new Factor(x))
                              )
  def direct_expression:Parser[AnyOuterExpression] = (
                                    expression                     ^^ (x => new DirectExpression(x))
                                  | "("~>formula<~")"              ^^ (x => new DirectExpression(x))
                                                     )
  def neg_expression:Parser[AnyOuterExpression] = (
                                      "!"~>expression              ^^ (x => new NegExpression(x))
                                    | "!("~>formula<~")"           ^^ (x => new NegExpression(x))
                                                  )
  def expression:Parser[AnyCompareExpression] = (
                                      member~"="~member             ^^ (x => new AnyCompareExpression(x._1._1, _==_, x._2))
                                    | member~"!="~member            ^^ (x => new AnyCompareExpression(x._1._1, _!=_, x._2))
                                    | member~">="~member            ^^ (x => new AnyCompareExpression(x._1._1, _>=_, x._2))
                                    | member~"<="~member            ^^ (x => new AnyCompareExpression(x._1._1, _<=_, x._2))
                                    | member~">"~member             ^^ (x => new AnyCompareExpression(x._1._1, _>_,  x._2))
                                    | member~"<"~member             ^^ (x => new AnyCompareExpression(x._1._1, _<_,  x._2))
                                                )
  def member:Parser[Member] =         m_term~rep("""[+]|[-]""".r~m_term)       ^^ (x => new Member(TreeBuilder(x._1, for(m_term <- x._2) yield (m_term._1, m_term._2))))
  def m_term:Parser[MTerm] =          m_factor~rep("""[*]|[/]""".r~m_factor)   ^^ (x => new MTerm(TreeBuilder(x._1, for(m_factor <- x._2) yield (m_factor._1, m_factor._2))))
  def m_factor:Parser[AnyMFactor] = (
                                      "|"~>member<~"|"                  ^^ (x => new Abs(x))
                                    | "("~>member<~")"                  ^^ (x => x)
                                    | "[LOG]("~>number~","~member<~")"    ^^ (x => new Log(x._1._1, x._2))
                                    | "[POW]("~>member~","~member<~")"    ^^ (x => new Pow(x._1._1, x._2))
                                    | "-"~>member                       ^^ (x => new Neg(x))
                                    | identifier                        ^^ (x => x)
                                    | number                            ^^ (x => x)
                                    )
  def identifier:Parser[Identifier] = """[a-zA-Z_]\w*""".r              ^^ (x => new Identifier(mappings.find(_.name==x).orNull.column_index))
  def number:Parser[Number] = """(0|[1-9][0-9]*([.][0-9]+)?)""".r       ^^ (x => new Number(x.toDouble))
  
  
  def apply(in:String) = {
    parseAll(formula, in.replaceAll("""\s*""",""))
  }
}

package condition {

import scalaSci.RichDouble1DArray

class Formula(val terms: List[Term]) extends AnyInnerExpression {
  def apply(sample: RichDouble1DArray): Boolean = {
    val results = for (term <- terms) yield term(sample)
    results.reduce({
      _ || _
    })
  }
}

class Term(val factors: List[Factor]) {
  def apply(sample: RichDouble1DArray): Boolean = {
    val results = for (factor <- factors) yield factor(sample)
    results.reduce({
      _ && _
    })
  }
}

class Factor(val expression: AnyOuterExpression) {
  def apply(sample: RichDouble1DArray): Boolean = {
    expression(sample)
  }
}

abstract class AnyOuterExpression {
  def apply(sample: RichDouble1DArray): Boolean
}

class DirectExpression(val inner: AnyInnerExpression) extends AnyOuterExpression {
  def apply(sample: RichDouble1DArray): Boolean = {
    inner(sample)
  }
}

class NegExpression(val inner: AnyInnerExpression) extends AnyOuterExpression {
  def apply(sample: RichDouble1DArray): Boolean = {
    !inner(sample)
  }
}

abstract class AnyInnerExpression {
  def apply(sample: RichDouble1DArray): Boolean
}

class AnyCompareExpression(val lside: Member, val cmp: (Double, Double) => Boolean, val rside: Member) extends AnyInnerExpression {
  def apply(sample: RichDouble1DArray): Boolean = {
    cmp(lside(sample), rside(sample))
  }
}

object TreeBuilder {
  def apply(first: MTerm, remainder: List[(String, MTerm)]): MemberTreeNode = {
    if (remainder.length == 0) new DirectMemberTreeNode(first)
    else new InnerMemberTreeNode(new DirectMemberTreeNode(first), if (remainder(0)._1 == "+") _ + _ else _ - _, apply(remainder(0)._2, remainder.tail))
  }

  def apply(first: AnyMFactor, remainder: List[(String, AnyMFactor)]): MTermTreeNode = {
    if (remainder.length == 0) new DirectMTermTreeNode(first)
    else new InnerMTermTreeNode(new DirectMTermTreeNode(first), if (remainder(0)._1 == "*") _ * _ else _ / _, apply(remainder(0)._2, remainder.tail))
  }
}

class Member(val root: MemberTreeNode) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    root(sample)
  }
}

abstract class MemberTreeNode {
  def apply(sample: RichDouble1DArray): Double
}

class DirectMemberTreeNode(term: MTerm) extends MemberTreeNode {
  def apply(sample: RichDouble1DArray): Double = {
    term(sample)
  }
}

class InnerMemberTreeNode(lside: MemberTreeNode, op: (Double, Double) => Double, rside: MemberTreeNode) extends MemberTreeNode {
  def apply(sample: RichDouble1DArray): Double = {
    op(lside.apply(sample), rside.apply(sample))
  }
}

class MTerm(val root: MTermTreeNode) {
  def apply(sample: RichDouble1DArray): Double = {
    root(sample)
  }
}

abstract class MTermTreeNode {
  def apply(sample: RichDouble1DArray): Double
}

class DirectMTermTreeNode(factor: AnyMFactor) extends MTermTreeNode {
  def apply(sample: RichDouble1DArray): Double = {
    factor(sample)
  }
}

class InnerMTermTreeNode(lside: MTermTreeNode, op: (Double, Double) => Double, rside: MTermTreeNode) extends MTermTreeNode {
  def apply(sample: RichDouble1DArray): Double = {
    op(lside.apply(sample), rside.apply(sample))
  }
}

abstract class AnyMFactor {
  def apply(sample: RichDouble1DArray): Double
}

class Identifier(val id: Int) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    sample(id)
  }
}

class Number(val n: Double) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    n
  }
}

class Abs(val inner: Member) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    math.abs(inner(sample))
  }
}

class Log(val base: Number, val arg: Member) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    math.log10(arg(sample)) / math.log10(base(sample))
  }
}

class Pow(val base: Member, val exp: Member) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    math.pow(base(sample), exp(sample))
  }
}

class Neg(val inner: Member) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    -inner(sample)
  }
}

}