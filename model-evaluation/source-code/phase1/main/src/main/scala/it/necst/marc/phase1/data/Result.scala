package it.necst.marc.phase1.data

import it.necst.marc.data.Feature

import scalaSci.RichDouble2DArray

/**
 * Created by andreadamiani on 04/06/15.
 */
case class Result(features:List[Feature], data:List[List[Double]], isTimeOrdered:Boolean){
  require(features.nonEmpty, "Feature mappings must be defined.")
  require(data.nonEmpty, "Data must be present.")
  private val featuresInv = for (feature <- features) yield feature.name.toLowerCase
  require(featuresInv.size==featuresInv.toSet.size, "Each feature can be mapped only once.")

  val dataForProcessing:RichDouble2DArray = new RichDouble2DArray((for (row <- data) yield row.toArray).toArray)
}
