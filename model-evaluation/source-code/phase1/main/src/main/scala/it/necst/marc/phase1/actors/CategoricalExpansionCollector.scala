package it.necst.marc.phase1.actors

import akka.actor.{ActorRef, Actor, Props}
import akka.routing.BalancingPool
import it.necst.marc.data.{FeatureMonotony, FeatureType, Feature}
import it.necst.marc.data.support.{ErrorStack, MarcLogger}

import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
  * Created by andreadamiani on 02/02/16.
  */
object CategoricalExpansionCollector {
  def props(dataSet: RichDouble2DArray, features:List[Feature]): Props = Props(new CategoricalExpansionCollector(dataSet,features))

}

case class ExpansionRepresentation(originalName:Feature.ID,
                                   expansion:Map[Int, Double])

class CategoricalExpansionCollector( dataset: RichDouble2DArray,
                                      features:List[Feature]) extends Actor{


  private val logger = MarcLogger(getClass.getName)

  //private var routerRef: ActorRef = _
  private var waitingMessages: Int = _
  private var featuresExpanded: List[(RichDouble2DArray, ExpansionRepresentation)] = _
  private var callerRef: ActorRef = _
  private var actors: List[ActorRef] = _

  override def preStart() = {
    featuresExpanded = Nil
    waitingMessages = 0
    actors = for(feature <- features) yield context.system.actorOf(CategoricalExpansionActor.props())
    //routerRef = context.system.actorOf(BalancingPool(features.size).props(CategoricalExpansionActor.props()),s"categoricalExpansionActor-${System.currentTimeMillis()}")
  }
  def receive = initState

  def initState: Receive = {

    case StartCategoricalExpansion() =>
      logger.info("Received StartCategoricalExpansion message")
      try{
        callerRef = sender()
        context.become(collectResults)
        sendComputationRequests(features,actors)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          throw new IllegalArgumentException(error)
          for(actor <- actors)
            context.stop(actor)
          context.stop(self)
      }


  }

  def collectResults: Receive = {

    case CategoricalExpanded(data,infos) =>
      logger.info("Received CategoricalExpanded message")
      try{
        waitingMessages = waitingMessages - 1
        featuresExpanded ::= (data,infos)

        if(waitingMessages == 0){
          callerRef ! rebuildDataSet()
          for(actor <- actors)
            context.stop(actor)
          context.stop(self)
        }

      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          throw new IllegalArgumentException(error)
          for(actor <- actors)
            context.stop(actor)
          context.stop(self)
      }

    case ThrowsError(error) =>
      throw new IllegalArgumentException(error)
      for(actor<- actors)
        context.stop(actor)
      context.stop(self)

  }

  private def sendComputationRequests(toBeProcessed: List[Feature],actors: List[ActorRef]): Unit = {
    if(toBeProcessed.nonEmpty){
      val feature = toBeProcessed.head
      val index = feature.column_index
      waitingMessages = waitingMessages + 1
      actors.head ! ExpandCategorical(new RichDouble1DArray(dataset.getCol(index)),feature)
      sendComputationRequests(toBeProcessed.tail,actors.tail)
    }
  }

  private def rebuildDataSet(): (RichDouble2DArray, List[Feature]) = {
    val originalDataset = this.dataset
    val originalCategoricalFeatures = this.features

    def recursiveAddExpandedFeature(resultDataset: RichDouble2DArray, resultFeatures:List[Feature], toBeProcessed:List[(RichDouble2DArray, ExpansionRepresentation)]): (RichDouble2DArray, List[Feature]) = {
      def recursiveAddColumn(resultDataset: RichDouble2DArray, resultFeatures:List[Feature], toBeProcessed:Map[Int,Double], partialDataset:RichDouble2DArray, originalFeature:Feature): (RichDouble2DArray, List[Feature]) = {
        if(toBeProcessed.isEmpty) (resultDataset, resultFeatures)
        else {
          val currentExpansion = toBeProcessed.head
          val isNew = resultDataset == null
          val currentIndex = if(isNew) {0} else {resultDataset.numColumns()}
          val currentColumn = partialDataset.getCol(currentExpansion._1)
          val currentExpandedValue = currentExpansion._2
          val partialResultDataset = if(isNew){
            new RichDouble2DArray(currentColumn)
          } else {
            resultDataset.>>>(currentColumn)
          }
          val newFeature = Feature(originalFeature.name + "_" + currentExpandedValue, currentIndex, false, FeatureType.INSTANTANEOUS, originalFeature.name, currentExpandedValue, FeatureMonotony.NONE, (Some(0),Some(1)))
          recursiveAddColumn(partialResultDataset, newFeature :: resultFeatures, toBeProcessed.tail, partialDataset, originalFeature)
        }
      }

      if(toBeProcessed.isEmpty) (resultDataset, resultFeatures)
      else {
        val currentFeature = toBeProcessed.head
        val currentOriginalFeature = originalCategoricalFeatures.find(_.name == currentFeature._2.originalName)
        require(currentOriginalFeature.isDefined, "MALFORMED FEATURE EXPANSION - missing original feature")
        val partialResult = recursiveAddColumn(resultDataset, resultFeatures, currentFeature._2.expansion, currentFeature._1, currentOriginalFeature.get)
        recursiveAddExpandedFeature(partialResult._1, partialResult._2, toBeProcessed.tail)
      }
    }

    recursiveAddExpandedFeature(null, Nil, featuresExpanded)
  }
}

