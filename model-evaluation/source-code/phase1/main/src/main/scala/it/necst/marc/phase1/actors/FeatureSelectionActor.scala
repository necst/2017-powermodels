package it.necst.marc.phase1.actors

import akka.actor.{Props, Actor}
import it.necst.marc.data.support.ErrorStack
import it.necst.marc.phase1.computation.featureSelection.FeatureSelection

import scalaSci.RichDouble2DArray

/**
  * Created by andrea on 11/12/15.
  */

object FeatureSelectionActor{

  def props(dataset: RichDouble2DArray): Props = Props(new FeatureSelectionActor(dataset))

}


class FeatureSelectionActor(dataset: RichDouble2DArray) extends Actor{

  def receive = {
    case DoFeatureSelection(rules) =>
      try{
        val data = FeatureSelection.fuse(dataset,rules)
        sender() ! FeatureSelectionDone(data)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }
  }

}
