package it.necst.marc.phase1.actors

import akka.actor.ActorRef
import it.necst.marc.data.{Feature, PartialStat}
import it.necst.marc.data.support.representability._
import it.necst.marc.phase1.data.internal.featureSelection.FusionRule
import it.necst.marc.phase1.data.{Entry, Report}

import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
 * Created by andrea on 15/09/15.
 */
sealed trait Message

//Internal Worker messages
case class PreProcessingComplete(data: RichDouble2DArray, stats: List[PartialStat],amended: List[Entry],excluded: List[Entry],out_of_bound: List[Entry],granularity: List[Entry]) extends Message
case class FeatureSelectionCompleted(data: RichDouble2DArray,stats: List[PartialStat]) extends Message
case class ThrowsError(message: String) extends Message


//PreProcessing collector messages
case class StartCollectorComputation() extends Message
case class PreProcessData(data: RichDouble1DArray, index: Int, replyTo: ActorRef) extends Message
case class ProcessedComplete(data: Option[RichDouble1DArray],index: Int,
                             amended: List[Entry],
                             excluded: List[Entry],
                             out_of_bound: List[Entry],
                             granularity: List[Entry]) extends Message


//feature selection collection messages
case class StartFeatureSelection() extends Message
case class DoFeatureSelection(rules: List[R[FusionRule]]) extends Message
case class FeatureSelectionDone(result: Map[Int,Array[Double]]) extends Message


//quantization correction messages
case class StartQuantizationCorrection(featuresToBeModified: List[String]) extends Message
case class CorrectQuantization(feature: RichDouble1DArray,index: Int) extends Message
case class FeatureQuantizated(feature: RichDouble1DArray,index: Int) extends Message

//categorical expansion messages
case class StartCategoricalExpansion() extends Message
case class ExpandCategorical(column: RichDouble1DArray, feature: Feature) extends Message
case class CategoricalExpanded(partial: RichDouble2DArray, expansionInfo:ExpansionRepresentation) extends Message