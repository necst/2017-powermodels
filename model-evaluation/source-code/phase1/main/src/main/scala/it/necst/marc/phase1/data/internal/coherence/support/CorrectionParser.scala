package it.necst.marc.phase1.data.internal.coherence.support

import correction._
import it.necst.marc._
import it.necst.marc.data.Feature

import scala.util.parsing.combinator.JavaTokenParsers

class CorrectionParser(
                        val mappings: List[Feature]
                        ) extends JavaTokenParsers {
  def formula: Parser[Correction] = repsep(rule, ";") ^^ (x => new Correction(x))

  def rule: Parser[Rule] = identifier ~ "=" ~ member ^^ (x => new Rule(x._1._1, x._2))

  def member: Parser[Member] = m_term ~ rep( """[+]|[-]""".r ~ m_term) ^^ (x => new Member(TreeBuilder(x._1, for (m_term <- x._2) yield (m_term._1, m_term._2))))

  def m_term: Parser[MTerm] = m_factor ~ rep( """[*]|[/]""".r ~ m_factor) ^^ (x => new MTerm(TreeBuilder(x._1, for (m_factor <- x._2) yield (m_factor._1, m_factor._2))))

  def m_factor: Parser[AnyMFactor] = (
    "|" ~> member <~ "|" ^^ (x => new Abs(x))
      | "(" ~> member <~ ")" ^^ (x => x)
      | "[LOG](" ~> number ~ "," ~ member <~ ")" ^^ (x => new Log(x._1._1, x._2))
      | "[POW](" ~> member ~ "," ~ member <~ ")" ^^ (x => new Pow(x._1._1, x._2))
      | "[MIN](" ~> rep1sep(member, ",") <~ ")" ^^ (x => new Min(x))
      | "[MAX](" ~> rep1sep(member, ",") <~ ")" ^^ (x => new Max(x))
      | "-" ~> member ^^ (x => new Neg(x))
      | pointer ^^ (x => x)
      | number ^^ (x => x)
    )

  def pointer: Parser[Pointer] = identifier ^^ (x => new Pointer(x))

  def identifier: Parser[Identifier] = """[a-zA-Z_]\w*""".r ^^ (x => {
    new Identifier(mappings.find(_.name == x).orNull.column_index)
  })

  def number: Parser[Number] = """(0|[1-9][0-9]*([.][0-9]+)?)""".r ^^ (x => new Number(x.toDouble))


  def apply(in: String) = {
    parseAll(formula, in.replaceAll("""\s*""",""))
  }
}

package correction {

import scalaSci.RichDouble1DArray

class Correction(val rules: List[Rule]) {
  def apply(sample: RichDouble1DArray): RichDouble1DArray = {
    recursivelyApply(sample, rules)
  }

  def recursivelyApply(sample: RichDouble1DArray, remainder: List[Rule]): RichDouble1DArray = {
    if (remainder.isEmpty) sample
    else recursivelyApply(remainder.head(sample), remainder.tail)
  }
}

class Rule(val id: Identifier, val member: Member) {
  def apply(sample: RichDouble1DArray): RichDouble1DArray = {
    val theId = id(sample)
    val head = if (theId == 0) RichDouble1DArray()
    else
    if (theId == 1) RichDouble1DArray(sample(theId - 1))
    else sample(0, theId - 1)
    val elem = RichDouble1DArray(member(sample))
    val tail = if (theId == sample.length - 1) RichDouble1DArray()
    else
    if (theId == sample.length - 2) RichDouble1DArray(sample(theId + 1))
    else sample(theId + 1, sample.length - 1)

    tail :: elem :: head
  }
}

object TreeBuilder {
  def apply(first: MTerm, remainder: List[(String, MTerm)]): MemberTreeNode = {
    if (remainder.isEmpty) new DirectMemberTreeNode(first)
    else new InnerMemberTreeNode(new DirectMemberTreeNode(first), if (remainder.head._1 == "+") _ + _ else _ - _, apply(remainder.head._2, remainder.tail))
  }

  def apply(first: AnyMFactor, remainder: List[(String, AnyMFactor)]): MTermTreeNode = {
    if (remainder.isEmpty) new DirectMTermTreeNode(first)
    else new InnerMTermTreeNode(new DirectMTermTreeNode(first), if (remainder.head._1 == "*") _ * _ else _ / _, apply(remainder.head._2, remainder.tail))
  }
}

class Member(val root: MemberTreeNode) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    root(sample)
  }
}

abstract class MemberTreeNode {
  def apply(sample: RichDouble1DArray): Double
}

class DirectMemberTreeNode(term: MTerm) extends MemberTreeNode {
  def apply(sample: RichDouble1DArray): Double = {
    term(sample)
  }
}

class InnerMemberTreeNode(lside: MemberTreeNode, op: (Double, Double) => Double, rside: MemberTreeNode) extends MemberTreeNode {
  def apply(sample: RichDouble1DArray): Double = {
    op(lside.apply(sample), rside.apply(sample))
  }
}

class MTerm(val root: MTermTreeNode) {
  def apply(sample: RichDouble1DArray): Double = {
    root(sample)
  }
}

abstract class MTermTreeNode {
  def apply(sample: RichDouble1DArray): Double
}

class DirectMTermTreeNode(factor: AnyMFactor) extends MTermTreeNode {
  def apply(sample: RichDouble1DArray): Double = {
    factor(sample)
  }
}

class InnerMTermTreeNode(lside: MTermTreeNode, op: (Double, Double) => Double, rside: MTermTreeNode) extends MTermTreeNode {
  def apply(sample: RichDouble1DArray): Double = {
    op(lside.apply(sample), rside.apply(sample))
  }
}

abstract class AnyMFactor {
  def apply(sample: RichDouble1DArray): Double
}

class Pointer(val id: Identifier) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    sample(id(sample))
  }
}

class Abs(val inner: Member) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    math.abs(inner(sample))
  }
}

class Log(val base: Number, val arg: Member) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    math.log10(arg(sample)) / math.log10(base(sample))
  }
}

class Pow(val base: Member, val exp: Member) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    math.pow(base(sample), exp(sample))
  }
}

class Neg(val inner: Member) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    -inner(sample)
  }
}

class Min(val members: List[Member]) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    val values = for (member <- members) yield member(sample)
    values.reduce((x, y) => if (x < y) x else y)
  }
}

class Max(val members: List[Member]) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    val values = for (member <- members) yield member(sample)
    values.reduce((x, y) => if (x > y) x else y)
  }
}

class Identifier(val id: Int) {
  def apply(sample: RichDouble1DArray): Int = {
    id
  }
}

class Number(val n: Double) extends AnyMFactor {
  def apply(sample: RichDouble1DArray): Double = {
    n
  }
}

}