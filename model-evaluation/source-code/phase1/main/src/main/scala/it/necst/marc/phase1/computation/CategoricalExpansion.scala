package it.necst.marc.phase1.computation

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern._
import akka.util.Timeout
import it.necst.marc.data.{Feature, FeatureType}
import it.necst.marc.phase1.actors.{CategoricalExpansionCollector, ExpansionRepresentation, StartCategoricalExpansion}

import scala.collection.mutable
import scala.concurrent.Await
import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
  * Created by andrea on 08/11/15.
  */
object CategoricalExpansion {

  def apply(dataset: RichDouble2DArray, features:List[Feature]):(RichDouble2DArray, List[Feature]) = {
    val featuresPartition = features.partition(_.specs.feature_type == FeatureType.CATEGORICAL)
    val toBeExpanded = featuresPartition._1
    val toBeKept = featuresPartition._2

    val localActorSystem = ActorSystem("CategoricalExpansionActors").actorOf(CategoricalExpansionCollector.props(dataset, toBeExpanded))
    implicit val timeout =  Timeout(3,TimeUnit.HOURS)
    val results = try{
      Await.result(localActorSystem ? StartCategoricalExpansion(),timeout.duration).asInstanceOf[(RichDouble2DArray, List[Feature])]
    }catch {
      case e:Throwable =>
        throw e
    }

    def recursiveAddColumn(resultDataset: RichDouble2DArray, resultFeatures:List[Feature], toBeProcessed:List[Feature]): (RichDouble2DArray, List[Feature]) = {
      if(toBeProcessed.isEmpty) (resultDataset, resultFeatures)
      else {
        val currentFeature = toBeProcessed.head
        val isNew = resultDataset == null
        val currentIndex = if(isNew) {0} else {resultDataset.numColumns()}
        val currentColumn = dataset.getCol(currentFeature.column_index)
        val partialResultDataset = if(isNew){
          new RichDouble2DArray(currentColumn)
        } else {
          resultDataset.>>>(currentColumn)
        }
        val newFeature = Feature(currentFeature.name, currentIndex, currentFeature.is_time, currentFeature.specs)
        recursiveAddColumn(partialResultDataset, newFeature :: resultFeatures, toBeProcessed.tail)
      }
    }

    recursiveAddColumn(results._1, results._2, toBeKept)
  }

  def apply(column:RichDouble1DArray, feature:Feature): (RichDouble2DArray, ExpansionRepresentation) = {

    def nonFunctionalApply():(RichDouble2DArray, ExpansionRepresentation) = {
      val foundValues = mutable.Map[Double, Int]()
      for(elem <- column.getv()){
        if(!foundValues.contains(elem)){
          foundValues += (elem -> foundValues.size)
        }
      }
      val result = new RichDouble2DArray(column.length, foundValues.size)
      for(i <- column.getv().indices){
        result.update(i, foundValues(column(i)), 1.0)
      }
      (result, ExpansionRepresentation(feature.name, foundValues.map(_.swap).toMap))
    }

    nonFunctionalApply()

    //Needs more compiler optimization
    /*def listRecursiveApply(resultDataset:List[List[Double]], resultExpansions: Map[Double,Int], reversedColumnToBeProcessed:List[Double], count:Int):(RichDouble2DArray, ExpansionRepresentation) = {
      if(reversedColumnToBeProcessed.isEmpty){
        val reversedDataset = it.necst.marc.toRichDouble2DArray(resultDataset.map(_.toArray[Double]).reverse.toArray[Array[Double]])
        (reversedDataset.~, ExpansionRepresentation(feature.name, resultExpansions.map(_.swap)))
      } else {
        val currentVal = reversedColumnToBeProcessed.head
        val isNewVal = !resultExpansions.contains(currentVal)
        val currentIndex = if(isNewVal) resultDataset.size else resultExpansions(currentVal)
        val newExpansions = if(isNewVal){
          resultExpansions + (currentVal -> resultDataset.size)
        } else {
          resultExpansions
        }
        val intermediateDataset = if(isNewVal){
          List.fill(count)(0d) :: resultDataset
        } else {
          resultDataset
        }
        val newDataset =
          (for(i <- intermediateDataset.indices) yield {
            (if((intermediateDataset.size - 1 - currentIndex) == i){1d} else {0d}) :: intermediateDataset(i)
          }).toList
        listRecursiveApply(newDataset, newExpansions, reversedColumnToBeProcessed.tail, count + 1)
      }
    }

    listRecursiveApply(List[List[Double]](), Map[Double, Int](), it.necst.marc.toStandardDouble1DArray(column).toList.reverse, 0)*/

    /*def recursiveApply(resultDataset:RichDouble2DArray, resultExpansions:Map[Double,Int], i:Int):(RichDouble2DArray, ExpansionRepresentation) = {
      if(i >= column.length) (resultDataset, ExpansionRepresentation(feature.name, resultExpansions.map(_.swap)))
      else {
        val currentVal = column(i)
        val isNewVal = !resultExpansions.contains(currentVal)
        val isNew = resultDataset == null
        val currentIndex = if(isNewVal) {if(isNew) {0} else {resultDataset.numColumns()}} else {resultExpansions(currentVal)}
        val newExpansions = if(isNewVal){
          resultExpansions + (currentVal -> currentIndex)
        } else {
          resultExpansions
        }
        val newDataset = if(isNew){
          new RichDouble2DArray(Array(1.0))
        } else {
          val expandedDataset = if(isNewVal){
            val newColumn = new RichDouble1DArray(resultDataset.numRows())
            resultDataset >>> newColumn
          } else {
            resultDataset
          }
          val newValues = for(i<- 0 until expandedDataset.numColumns()) yield {
            if(i == currentIndex) {1.0} else {0.0}
          }
          val newRow = RichDouble1DArray(newValues:_*)
          expandedDataset >> newRow
        }

        recursiveApply(newDataset, newExpansions, i+1)
      }
    }

    recursiveApply(null, Map(), 0)*/
  }


  def obtainExpandedFeatures(name:Feature.ID, features:List[Feature]) = features.filter{
    f =>
      f.name == name || (f.specs.expanded_from match {
        case Some(ef) => ef.original_name == name
        case None => false
      })
  }

  //TODO Bypassed code
  /*
  def apply(sample: RichDouble1DArray,features: List[Feature]): RichDouble1DArray = {


    val featureToAdd = features.filter(_.specs.feature_type.equals(FeatureType.CATEGORICAL)).sortBy(_.column_index)

    val featureToRemove = featureToAdd.sortWith((x: Feature,y:Feature) => {x.column_index > y.column_index})

    def addColumns(toBeProcessed: List[it.necst.marc.data.Feature],newRow: List[Double]):  List[Double] = {
      if(toBeProcessed.isEmpty) newRow
      else{
        val feature = toBeProcessed.head
        val rowUpdated = addColumnForFeature(feature,feature.specs.bounds.lower.get.toInt,newRow,newRow(feature.column_index).toInt)
        addColumns(toBeProcessed.tail,rowUpdated)
      }
    }

    def addColumnForFeature(feature: Feature,currentIndex: Int,newRow: List[Double],currentValue: Int): List[Double] = {
      if(currentIndex > feature.specs.bounds.upper.get.toInt) newRow
      else{
        val number = if(currentIndex == currentValue) 1.0 else 0.0
        val rowUpdated = newRow :+ number
        addColumnForFeature(feature,currentIndex+1,rowUpdated,currentValue)
      }
    }


    def removeFeatures(toBeProcessed: List[Feature], updatedSample: List[Double]): List[Double] = {
      if(toBeProcessed.isEmpty) updatedSample
      else{
        val columnIndex = toBeProcessed.head.column_index
        val (first,second) = updatedSample.splitAt(columnIndex)

        removeFeatures(toBeProcessed.tail, first ++ second.tail)
      }
    }

    val expanded = addColumns(featureToAdd,sample.getv().toList)

    toRichDouble1DArray(removeFeatures(featureToRemove,expanded).toArray)


  }

  def apply(features: List[it.necst.marc.data.Feature]): List[it.necst.marc.data.Feature] = {

    def expandFeatures(toBeProcessed: List[it.necst.marc.data.Feature],
                       newFeaturesList: List[it.necst.marc.data.Feature],
                       index: Int): List[it.necst.marc.data.Feature] = {
      if(toBeProcessed.isEmpty) newFeaturesList

      else{
        val featureToExpand = toBeProcessed.head
        val dataNextIter = if(featureToExpand.specs.feature_type.equals(FeatureType.CATEGORICAL)){
          (expandFeature(featureToExpand.specs.bounds.lower.get.toInt,
            index, featureToExpand, newFeaturesList),
            index + Math.abs(featureToExpand.specs.bounds.upper.get.toInt - featureToExpand.specs.bounds.lower.get.toInt) + 1 )
        }else{
          (newFeaturesList :+ featureToExpand,index)
        }
        expandFeatures(toBeProcessed.tail,dataNextIter._1,dataNextIter._2)

      }
    }

    def expandFeature(currentValue: Int,
                      currentIndex: Int,
                      feature: it.necst.marc.data.Feature,
                      featuresList: List[it.necst.marc.data.Feature]):List[it.necst.marc.data.Feature] = {
      if(currentValue > feature.specs.bounds.upper.get) featuresList

      else{
        val newName = createNameCategoricalExpansion(feature.name,currentValue)
        val isTime = if(feature.is_time.isDefined) true else false
        val newFeature = it.necst.marc.data.Feature(newName,currentIndex,isTime,FeatureType.INSTANTANEOUS,
          feature.name,currentValue.toDouble,feature.specs.monotony,(Some(0),Some(1)))
        val newListFeature = featuresList :+ newFeature
        expandFeature(currentValue+1,currentIndex+1,feature,newListFeature)
      }
    }
    expandFeatures(features,Nil,features.size)
  }


  def apply(featuresToEpurate: List[it.necst.marc.data.Feature],
                          categoricFeatures: List[it.necst.marc.data.Feature]): List[it.necst.marc.data.Feature] = {


    def updateMap(toBeProcessed: List[it.necst.marc.data.Feature], map: Map[Int,Int]): Map[Int,Int] = {
      if(toBeProcessed.isEmpty) map
      else{
        val mapUpdated = increaseCounter(map,Map[Int,Int](),toBeProcessed.head.column_index)
        updateMap(toBeProcessed.tail,mapUpdated)
      }
    }

    def increaseCounter(toBeProcessed: Map[Int,Int],finalMap: Map[Int,Int],currentIndex: Int): Map[Int,Int] = {
      if(toBeProcessed.isEmpty) finalMap
      else{
        val newMap = if(toBeProcessed.head._1 > currentIndex){
          finalMap + (toBeProcessed.head._1 -> (toBeProcessed.head._2 + 1))
        }else{
          finalMap + toBeProcessed.head
        }
        increaseCounter(toBeProcessed.tail,newMap,currentIndex)
      }
    }

    def updateFeatures(toBeProcessed: Map[Int,Int], features: List[it.necst.marc.data.Feature]): List[it.necst.marc.data.Feature]  = {
      if(toBeProcessed.isEmpty) features
      else{
        val feature = features.find(_.column_index == toBeProcessed.head._1).head
        val newList = features.filter(!_.name.equals(feature.name)) :+
          it.necst.marc.data.Feature(feature.name,feature.column_index - toBeProcessed.head._2,feature.is_time,feature.specs)
        updateFeatures(toBeProcessed.tail,newList)
      }
    }

    val map = (for(feature <- featuresToEpurate) yield feature.column_index -> 0).toMap[Int,Int]
    val mapUpdated = updateMap(categoricFeatures,map)
    updateFeatures(mapUpdated,featuresToEpurate)
  }

  private def createNameCategoricalExpansion(name: String,value: Double) = name + "_" + value.toInt

*/


}
