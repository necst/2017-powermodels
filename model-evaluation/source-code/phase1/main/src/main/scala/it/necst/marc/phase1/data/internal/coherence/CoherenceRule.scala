package it.necst.marc.phase1.data.internal.coherence

import it.necst.marc._
import it.necst.marc.phase1.data.internal.coherence.support.condition.Formula
import it.necst.marc.phase1.data.internal.coherence.support.correction.Correction

class CoherenceRule(
    val condition:Formula, 
    val correction:Correction
    ) {
  require{
    condition != null
  }
  
  def this(condition:Formula) = this(condition, null)
  
  def ==>(correction:Correction): CoherenceRule = new CoherenceRule(this.condition, correction)

  def apply(sample:Array[Double]):Array[Double] = if (condition(sample)) {
    val resulting = correction(sample)
    resulting
  } else sample
}

object CoherenceRule{
  implicit def conditionToRule(x: Formula):CoherenceRule = new CoherenceRule(x)
}