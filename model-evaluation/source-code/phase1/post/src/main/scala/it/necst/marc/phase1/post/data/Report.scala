package it.necst.marc.phase1.post.data

import it.necst.marc.data.{Performance, AbstractReport}
import it.necst.marc.phase1.post.data.support.Phase2Subphase

/**
 * Created by andreadamiani on 05/06/15.
 */

case class Report(content:Option[MathStats], error:Option[String]) extends AbstractReport(content, error)

case class MathStats(phasesStats:List[PhaseStats],performance: Performance) extends it.necst.marc.data.MathStats {
  require(
    (
      for(value <- Phase2Subphase.values) yield phasesStats.count(_.phaseName == value) == 1
      ) reduce (_&&_))
}

case class PhaseStats(phaseName: Phase2Subphase.Value, expressedConfigurations:Int) extends it.necst.marc.data.MathStats{
  require(expressedConfigurations>=0, "The number of configuration expressed cannot be negative.")
}

