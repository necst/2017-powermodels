package it.necst.marc.phase1.post.actors

import java.net.URI

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import com.typesafe.config.Config
import it.necst.marc.data._
import it.necst.marc.data.support.{MarcLogger, ErrorStack, OptionalTag}
import it.necst.marc.infrastructure.actors.messages.{ComputationError, InternalComputationCompletedMultipleResults, StartInternalComputation}
import it.necst.marc.infrastructure.support.{DataEncoder, DataCompressor, TraitInternalActor}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification
import it.necst.marc.phase1.post.data.MathStats
import it.necst.marc.phase1.post.data.internal.{DataManagementStats, TemporalOrderingStats}
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase1.post.data._
import rapture.json._
import rapture.json.jsonBackends.jawn._

/**
 * Created by andrea on 01/06/15.
 */

object InternalWorker extends TraitInternalActor{

  def  props(currentConfiguration:String,actorToReply: ActorRef,
             mapData: Map[URI,(Array[Byte],Int)],configFile: Config): Props = Props(new InternalWorker(currentConfiguration,actorToReply,mapData,configFile))

}


class InternalWorker(val currentConfiguration:String,
                     val actorToReply: ActorRef,
                     val mapData:Map[URI,(Array[Byte],Int)],
                     val configFile: Config) extends Actor {

  private val logger = MarcLogger(getClass.getName)

  private val system = ActorSystem("InternalWorker")

  private var results: Map[Phase2Subphase.Value,Result] = Map()

  private var featuresList: List[it.necst.marc.data.Feature] = Nil

  private var listFrequencies: List[PhaseResultFrequencies] = Nil

  private var batchIdentificationCollector: ActorRef = _

  private var actors: List[ActorRef] = Nil

  private var performanceStats: List[PartialStat] = Nil
  private var initialTimestamp: Long = 0l

  private final val dictionaries:Map[Phase2Subphase.Value,ActorRef] = (
    for(value <- Phase2Subphase.values) yield {
      val actor = system.actorOf(DictionaryActor.props(),"dictionaryActor"+value)
      actors = actor :: actors
      value -> actor
    }).toMap

  private var dictionaryResults: Map[Phase2Subphase.Value,Map[SubphaseBatchIdentification.Code,List[Binding]]] = Map()

  def receive = initState

  def initState: Receive = {
    case StartInternalComputation() =>
      logger.info("Receive StartInternalComputation message")

      try{
        initialTimestamp = System.currentTimeMillis()
        val dataString = new String(DataCompressor.decompress(mapData.head._2._1,mapData.head._2._2))
        val previousResults = Json.parse(dataString).as[it.necst.marc.phase1.data.Result]
        val finishDataDecompressionTimestamp = System.currentTimeMillis()

        val userConfiguration = Json.parse(currentConfiguration).as[it.necst.marc.phase1.post.data.Configuration]

        val phaseConfigurations =
          for(phaseCfg <- userConfiguration.phasesConfigs) yield {
            PhaseConfiguration(phaseCfg.phaseName,
                                (for(f <- phaseCfg.configFeatures) yield{
                                  previousResults.features.find(_.name == f.name) match {
                                    case Some(feature) => f :: Nil
                                    case None =>
                                      val expanded = previousResults.features.filter(_.specs.expanded_from match{
                                        case Some(ef) => ef.original_name == f.name
                                        case None => false})
                                      require(expanded.nonEmpty, f.name + "is a configuration feature but is not defined.")
                                      for(ef <- expanded) yield ConfigurationFeature(ef.name, None)
                                  }
                                }).flatten)
          }

        val configuration = Configuration(phaseConfigurations, userConfiguration.outputFeature)

        val timestampColIndex = previousResults.features.find(_.is_time.isDefined).head.column_index
        featuresList = previousResults.features

        val initOrderingTimestamp = System.currentTimeMillis()
        val datasetOrdered = if(previousResults.isTimeOrdered){ previousResults.dataForProcessing} else {SubphaseBatchIdentification.temporalSorting(previousResults.dataForProcessing,timestampColIndex)}
        val finalTimestamp = System.currentTimeMillis()
        performanceStats = PartialStat(TemporalOrderingStats.TEMPORAL_ORDERING.toString,finalTimestamp-initOrderingTimestamp) :: performanceStats

        batchIdentificationCollector =
          system.actorOf(ActorsManager.props(datasetOrdered,self,configuration,configFile,previousResults.features,timestampColIndex,dictionaries))

        batchIdentificationCollector ! StartBatchIdentificationCollector()
        context.become(batchIdentificationCompleteState)

        performanceStats = PartialStat(DataManagementStats.DATA_DECOMPRESSION.toString,finishDataDecompressionTimestamp-initialTimestamp) :: performanceStats
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          val report = Json(Report(None,Some(error))).toString()
          actorToReply ! ComputationError(report,error)
          batchIdentificationCollector ! Deactivate()
          system.terminate()
          context.stop(self)

      }

  }

  def batchIdentificationCompleteState: Receive = {

    case ResultComputed(phaseName, result, frequencies,stats) =>
      logger.info("Received BatchesCompleted message")
      results = results + (phaseName -> result)
      listFrequencies = frequencies :: listFrequencies
      performanceStats = stats ++ performanceStats
      if(results.size == Phase2Subphase.values.size){
        for(dictionary <- dictionaries){
          dictionary._2 ! GiveMeDictionary()
        }
      }

    case TakeDictionary(dictionary) =>
      val phase = dictionaries.find{case (k,v) => v == sender()}.get._1
      dictionaryResults = dictionaryResults + (phase -> dictionary)

      if(dictionaryResults.size == Phase2Subphase.values.size){
        val maps = createMapResults()

        val dictionaries = (for(dictionary <- dictionaryResults) yield {
          Dictionary(dictionary._1, (for(entry <- dictionary._2) yield {
            Entry(entry._1, entry._2)
          }).toList)
        }).toList

        val finalTimestamp = System.currentTimeMillis()

        val resultsFrequenciesString = Json(ResultFrequencies(listFrequencies)).toString()
        val resultsDictionary = Json(ResultDictionary(dictionaries)).toString()

        val resultsFrequenciesByte = DataCompressor.compress(resultsFrequenciesString)
        val resultsFrequenciesDim = DataEncoder.byteLength(resultsFrequenciesString)

        val resultsDictionariesByte = DataCompressor.compress(resultsDictionary)
        val resultsDictionariesDim = DataEncoder.byteLength(resultsDictionary)

        val finalResultsMap = maps._1 + (OptionalTag.PRE.toString -> (resultsFrequenciesByte,resultsFrequenciesDim)) + (OptionalTag.DICT.toString -> (resultsDictionariesByte,resultsDictionariesDim))
        val finishCompressionDataTimestamp = System.currentTimeMillis()

        val compressionStat = PartialStat(DataManagementStats.DATA_COMPRESSION.toString,finalTimestamp-finishCompressionDataTimestamp)
        val performance = Performance(Some(PerfStats(finalTimestamp-initialTimestamp,Some(PartialStats(compressionStat::performanceStats)))))
        val mathStats = MathStats((for(item <- maps._2) yield PhaseStats(item._1,item._2)).toList,performance)
        val reportString = Json(Report(Some(mathStats),None)).toString

        val reportByte = DataCompressor.compress(reportString)
        val reportDim = DataEncoder.byteLength(reportString)
        actorToReply ! InternalComputationCompletedMultipleResults(finalResultsMap,reportByte,reportDim)
        batchIdentificationCollector ! Deactivate()
        for(actor <- actors)
          system.stop(actor)
        system.terminate()
        context.stop(self)
      }

    case ThrowsError(error) =>
      logger.info("Received ThrowsError message")
      batchIdentificationCollector ! Deactivate()
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      for(actor <- actors)
        system.stop(actor)
      system.terminate()
      context.stop(self)

  }



  private def createMapResults(): (Map[String,(Array[Byte],Int)],Map[Phase2Subphase.Value,Int]) = {


    def scanResults(toBeProcessed: Map[Phase2Subphase.Value,Result],
                    resultsMap: Map[String,(Array[Byte],Int)],
                    mathStatsMap: Map[Phase2Subphase.Value,Int]): (Map[String,(Array[Byte],Int)],Map[Phase2Subphase.Value,Int]) = {
      if(toBeProcessed.isEmpty) (resultsMap,mathStatsMap)
      else{
        val result = toBeProcessed.head._2
        val newMathStatsItem = toBeProcessed.head._1 -> result.batches.size
        val resultString = Json(result).toString()

        val resultByte = DataCompressor.compress(resultString)
        val resultDim = DataEncoder.byteLength(resultString)

        val newItemResult = toBeProcessed.head._1.toString -> (resultByte,resultDim)

        scanResults(toBeProcessed.tail,resultsMap + newItemResult,mathStatsMap + newMathStatsItem)
      }
    }
    scanResults(results,Map[String,(Array[Byte],Int)](),Map[Phase2Subphase.Value,Int]())
  }



}