package it.necst.marc.phase1.post.actors

import akka.actor.{Props, ActorRef, Actor}
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.phase1.post.computation.BatchEntry
import it.necst.marc.phase1.post.data.support.Phase2Subphase

/**
  * Created by andrea on 08/11/15.
  */
object LoadBalancerProducers{

  def props(actors: Map[Phase2Subphase.Value,ActorRef]): Props = Props(new LoadBalancerProducers(actors))
}

class LoadBalancerProducers(actors: Map[Phase2Subphase.Value,ActorRef]) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  def receive: Receive = {
    case UpdateBatches(batchfyResult) =>
      sendMessage(batchfyResult)

    case InvalidSampleFound() =>
      for(actor <- actors.values) actor ! InvalidSampleFound()
  }

  private def sendMessage(data: Map[Phase2Subphase.Value,BatchEntry]): Unit ={

    def send(toBeProcessed: Map[Phase2Subphase.Value,BatchEntry]): Unit = {
      if(toBeProcessed.nonEmpty){
        actors(toBeProcessed.head._1) ! UpdateBatch(toBeProcessed.head._2)
        send(toBeProcessed.tail)
      }
    }
    send(data)
  }
}
