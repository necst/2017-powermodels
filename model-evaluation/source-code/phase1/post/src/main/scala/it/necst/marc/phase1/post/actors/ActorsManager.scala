package it.necst.marc.phase1.post.actors

import akka.routing.{BroadcastGroup, BalancingPool}
import it.necst.marc.data.{FeatureMonotony, Feature}
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import akka.actor.{ActorRef, Props, Actor}
import it.necst.marc.infrastructure.actors.messages.ComputationError
import it.necst.marc.phase1.post.computation.{CodifyResult, SubphaseBatchIdentification}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.Configuration
import com.typesafe.config.Config
import it.necst.marc.phase1.post.data.support.Phase2Subphase


import scalaSci.{RichDouble1DArray, RichDouble2DArray}

/**
 * Created by andrea on 21/10/15.
 */

object ActorsManager{

  def props(dataset: RichDouble2DArray,
            internalWorkerRef: ActorRef,
            configuration: Configuration,
            configFile: Config,
            mappings: List[Feature],
            timeFeatureColIndex: Int,
            dictionaries: Map[Phase2Subphase.Value,ActorRef]): Props = Props(new ActorsManager(dataset,internalWorkerRef,configuration,configFile,mappings,timeFeatureColIndex,dictionaries))

}


class ActorsManager(dataset: RichDouble2DArray,
                                   internalWorkerRef: ActorRef,
                                   configuration: Configuration,
                                   configFile: Config,
                                   mappings: List[Feature],
                                   timeFeatureColIndex: Int,
                                   dictionaries:Map[Phase2Subphase.Value,ActorRef]) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private var codesAlreadySent: Map[Phase2Subphase.Value,List[String]] = _

  private val system = context.system
  private var actors: List[ActorRef] = Nil
  private var router: ActorRef = _

  override def preStart() = {
    codesAlreadySent = Phase2Subphase.values.toList.map{x:Phase2Subphase.Value => x -> Nil}.toMap
  }

  def receive: Receive = {

    case StartBatchIdentificationCollector() =>
      logger.info("Received StartBatchIdentificationCollector message")

      try {
        val phaseName = configFile.getString("name")

        //TODO check and remove also in configuration file
        //val codingActorsSize = configFile.getInt(phaseName+".coding_actors")
        val extendActorsSize = configFile.getInt(phaseName + ".extend_actors")

        val loadBalancerProducers = createActors(extendActorsSize)

        router = system.actorOf(BalancingPool(13).props(CodingActor.props(configuration, mappings, dictionaries, internalWorkerRef, loadBalancerProducers)))


        val splitSize = dataset.numRows()/20
        SubphaseBatchIdentification.codifyDispatcher(dataset, configuration, mappings, timeFeatureColIndex, if(splitSize < 100) 100 else splitSize) {
            router ! StartScanDataset(_)

        }
      } catch {
        case e:Throwable =>
          internalWorkerRef ! ThrowsError(ErrorStack(e))
          context.system.stop(router)
          context.system.stop(self)
      }


    case Deactivate() =>
      logger.info("Received Deactivate message")
      for(actor <- actors)
        system.stop(actor)
      context.system.stop(router)
      context.stop(self)
  }


  //TODO check and remove
  //serial codify
  /*
  private def serialCodify(loadBalancerProducers: ActorRef): Unit = {
    require(dataset.numRows()>2, "The dataset is too small for computation.")
    val timeFeat = mappings.find(_.is_time.getOrElse(false))
    require(timeFeat.isDefined, "No time feature defined.")

    val first = SubphaseBatchIdentification.codify(new RichDouble1DArray(dataset.getRow(1)), new RichDouble1DArray(dataset.getRow(0)), Map(), Map(), configuration, mappings, timeFeat.get.column_index)

    def recursiveApply(i:Int, prevResultCodification:CodifyResult): Unit = {
      if(i<dataset.numRows()){
        val resultCodification = SubphaseBatchIdentification.codify(new RichDouble1DArray(dataset.getRow(i)), prevResultCodification.sample, prevResultCodification.codes, prevResultCodification.monotony, configuration, mappings, timeFeat.get.column_index)
        loadBalancerProducers ! UpdateBatches(SubphaseBatchIdentification.batchify(resultCodification.sample,resultCodification.codes,resultCodification.initial,configuration,mappings))
        sendResultToDictionaries(resultCodification)
        recursiveApply(i+1, resultCodification)
      }
    }

    recursiveApply(2, first)
  }
 */
  private def createActors(extendActors: Int): ActorRef= {
    val smallBatchMappings = (for(phase <- Phase2Subphase.values.toList)
        yield phase -> SubphaseBatchIdentification.computeSmallBatchMappings(phase, configuration, mappings)).toMap[Phase2Subphase.Value, List[Feature]]

    val extendedBatchMappings = (for(phase <- Phase2Subphase.values.toList)
      yield phase -> SubphaseBatchIdentification.computeExpandedMappings(phase, configuration, smallBatchMappings(phase))).toMap[Phase2Subphase.Value, List[Feature]]

    def createProducerActors(toBeProcessed: Phase2Subphase.ValueSet,
                             actors: Map[Phase2Subphase.Value,ActorRef]): Map[Phase2Subphase.Value,ActorRef] = {
      if(toBeProcessed.isEmpty) actors
      else{
        val phase = toBeProcessed.head
        val newActorRef = system.actorOf(ProducerActor.props(phase,configuration,dataset.numRows(),extendedBatchMappings(phase),internalWorkerRef,dictionaries(phase)), "producerActor"+phase)
        val listActors =  actors + (toBeProcessed.head -> newActorRef )
        createProducerActors(toBeProcessed.tail,listActors)
      }
    }


    /*def createCodingActor(currentActorIndex: Int,
                          actorsNames: List[String],
                          batchSize: Int,
                          dictionaries: Map[Phase2Subphase.Value,ActorRef],
                          loadbalancerProducers: ActorRef): List[String] = {
      if(currentActorIndex >= codingActors) actorsNames
      else{
        def selectRow(rowIndex: Int): Boolean = if (rowIndex >= batchSize*currentActorIndex && rowIndex<(batchSize*(currentActorIndex+1))+2) true
                                                else false
        val newListName = if(batchSize*currentActorIndex <= dataset.numRows()-1){
          val partialDataset = dataset.filterRows(selectRow)
          actors = system.actorOf(CodingActor.props(partialDataset,configuration,mappings,dictionaries,internalWorkerRef,loadbalancerProducers), "codingActor"+currentActorIndex) :: actors
          val nameActor = "/user/codingActor"+currentActorIndex
          nameActor :: actorsNames
        }else{
          actorsNames
        }

        createCodingActor(currentActorIndex + 1,newListName,batchSize,dictionaries,loadbalancerProducers)
      }
    }*/


    val producers: Map[Phase2Subphase.Value,ActorRef] = createProducerActors(Phase2Subphase.values,Map())

    def createRouters(phases: Phase2Subphase.ValueSet,actorsMap: Map[Phase2Subphase.Value,ActorRef]):Map[Phase2Subphase.Value,ActorRef] = {
      if(phases.isEmpty) actorsMap
      else{
        val actor = if(Phase2Subphase.requiresExpandedBatch(phases.head)){
          system.actorOf(BalancingPool(extendActors).props(ExtendActor.props(producers(phases.head),smallBatchMappings(phases.head),internalWorkerRef)),name="extendActor_"+phases.head)
        }else{
          producers(phases.head)
        }
        createRouters(phases.tail,actorsMap + (phases.head->actor))
      }

    }

    val routers = createRouters(Phase2Subphase.values,Map())
    actors = (for(item<-routers) yield item._2).toList ++ actors

    val loadBalancerProducers = system.actorOf(LoadBalancerProducers.props(routers),"LoadBalancerProducers")
    actors = loadBalancerProducers :: actors

    /*val batchSize = dataset.numRows() / codingActors +1
    logger.info(s"BATCH SIZE: $batchSize")
    val codingActorRef = createCodingActor(0,Nil,batchSize,dictionaries,loadBalancerProducers)
    val routerCodify = system.actorOf(BroadcastGroup(codingActorRef).props(),"broadcastRouterCodify")
    actors = routerCodify :: actorsv

    routerCodify*/

    loadBalancerProducers
  }

  //TODO clean old version codify
  /*
  private def sendResultToDictionaries(resultCodification: CodifyResult) = {

    def sendMessages(toBeProcessed: Map[Phase2Subphase.Value,ActorRef]): Unit = {
      if(toBeProcessed.nonEmpty){
        val code = resultCodification.codes.getOrElse(toBeProcessed.head._1,"")
        if(!codesAlreadySent.get(toBeProcessed.head._1).get.contains(code.toString)) {
          toBeProcessed.head._2 ! AddEntryIntoDictionary(code,resultCodification.bindings.getOrElse(toBeProcessed.head._1,Nil))
          val newList: List[String] = codesAlreadySent.get(toBeProcessed.head._1).get :+ code.toString
          codesAlreadySent = codesAlreadySent.filter(!_._1.equals(toBeProcessed.head._1)) + (toBeProcessed.head._1 -> newList)
        }
        sendMessages(toBeProcessed.tail)
      }
    }

    sendMessages(dictionaries)
  }
*/


}
