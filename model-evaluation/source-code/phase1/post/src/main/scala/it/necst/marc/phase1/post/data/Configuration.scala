package it.necst.marc.phase1.post.data

import it.necst.marc.data.{Feature, AbstractConfiguration}
import it.necst.marc.data.support.ExtractableEnumeration
import it.necst.marc.phase1.post.data.support.Phase2Subphase

/**
 * Created by andreadamiani on 07/10/15.
 */
case class Configuration(phasesConfigs:List[PhaseConfiguration], outputFeature:Feature.ID) extends AbstractConfiguration{
  require(
      (
        for(value <- Phase2Subphase.values) yield phasesConfigs.count(_.phaseName == value) == 1
      ) reduce (_&&_), "Each subphase configuration must be unique.")
  require(outputFeature != null && outputFeature.nonEmpty, "An Output Feaure is required.")
}

case class PhaseConfiguration(phaseName:Phase2Subphase.Value, configFeatures:List[ConfigurationFeature]) {
  require(configFeatures!=null, "Configuration feature list must be defined.")
  require(configFeatures.map(_.name).toSet.size == configFeatures.size, "Configuration features must be unique within the same subphase.")
}

case class ConfigurationFeature(name:it.necst.marc.data.Feature.ID, is_monotony_related:Option[Boolean]){
  require(name!=null, "The configuration feature name must be defined.")
  require(is_monotony_related.getOrElse(true), "is_monotony_related is a flag, either set it at true or omit it.") //is a flag => only TRUE allowed
}

