package it.necst.marc.phase1.post.data.internal

import it.necst.marc.data.support.ExtractableEnumeration

/**
  * Created by andrea on 25/11/15.
  */

object TemporalOrderingStats extends ExtractableEnumeration{

  val TEMPORAL_ORDERING = Value("TemporalOrdering")
}
object BatchIdentificationStats extends ExtractableEnumeration{

  val INSERTION_TIME  = Value("Insertion Time")
  val DFS_TIME        = Value("Dfs time")

}

object DataManagementStats extends ExtractableEnumeration{

  val DATA_DECOMPRESSION           = Value("DataDecompression")
  val DATA_COMPRESSION             = Value("DataCompression")

}
