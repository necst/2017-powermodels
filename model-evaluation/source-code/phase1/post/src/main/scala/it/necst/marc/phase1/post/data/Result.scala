package it.necst.marc.phase1.post.data

import it.necst.marc.data.FeatureMonotony
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.data.support.Phase2Subphase

import scalaSci.RichDouble2DArray

/**
 * Created by andreadamiani on 05/06/15.
 */
case class Result(batches:List[ModelConfiguration], features:List[it.necst.marc.data.Feature])

case class ModelConfiguration(codes:List[ConfigurationCode], data:List[Sample], outputFeatureMonotony:FeatureMonotony.Value){
  require(codes != null && codes.nonEmpty, "The configuration code set cannot be empty.")
  require(codes.map(_.subphase).toSet.size == codes.map(_.subphase).size, "There can be at most one code for each subphase.")
  //require(features.nonEmpty, "At least one feature must define the configuration.")
  require(data.nonEmpty, "Empty configurations must not be reported")
  val dataForProcessing:RichDouble2DArray = new RichDouble2DArray((for (row <- data) yield row.sample.toArray).toArray)
}

case class ConfigurationCode(subphase:Phase2Subphase.Value, code:Code) {
  require(subphase != null, "Subphase key must be defined.")
  require(code != null, "Code value must be defined.")
}

//TODO remove when rapture option divergent implicit solved
case class Sample(sample:List[Double])