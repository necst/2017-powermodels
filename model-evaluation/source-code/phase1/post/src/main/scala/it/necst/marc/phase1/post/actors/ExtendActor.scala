package it.necst.marc.phase1.post.actors

import akka.actor.{ActorRef, Actor, Props}
import it.necst.marc.data.Feature
import it.necst.marc.data.support.ErrorStack
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification

/**
  * Created by andrea on 08/11/15.
  */

object ExtendActor{

  def props(producerActor: ActorRef,
            mappings: List[Feature],
            internalWorkerRef: ActorRef): Props = Props(new ExtendActor(producerActor,mappings,internalWorkerRef))

}

class ExtendActor(producerActor: ActorRef,
                 mappings: List[Feature],
                  internalWorkerRef: ActorRef) extends Actor{


  def receive: Receive = {
    case UpdateBatch(batchfyResult) =>
      try{
        val update = SubphaseBatchIdentification.extendSample(batchfyResult,mappings)
        producerActor ! UpdateBatch(update)
      }catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          internalWorkerRef ! ThrowsError(error)
      }

    case InvalidSampleFound() =>
      producerActor ! InvalidSampleFound()

  }

}
