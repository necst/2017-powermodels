package it.necst.marc.phase1.post.actors

import it.necst.marc.data.{PartialStat, Binding}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase1.post.computation.{SubphaseBatchIdentification, BatchEntry, CodifyResult}
import it.necst.marc.phase1.post.data.{PhaseResultFrequencies, Result}
import it.necst.marc.phase1.post.data.support.Phase2Subphase

import scalaSci.RichDouble2DArray


/**
 * Created by andrea on 28/09/15.
 */

sealed trait Message

//internal worker
case class StartBatchIdentificationCollector() extends Message
case class ResultComputed(phaseName: Phase2Subphase.Value,result: Result,frequencies: PhaseResultFrequencies,stats: List[PartialStat]) extends Message
case class ThrowsError(message: String) extends Message

//batchidentification collector
case class BatchIdentificationCompleted() extends Message
case class Deactivate() extends Message

//CodingActor actor
case class StartScanDataset(dataset: RichDouble2DArray) extends Message

//batchfy actor
case class Batchfy(codifyResult: CodifyResult) extends Message

//producer actor
case class UpdateBatches(batchfyResult: Map[Phase2Subphase.Value,BatchEntry]) extends Message
case class UpdateBatch(batch: BatchEntry) extends Message
case class InvalidSampleFound() extends Message

//DictionaryActor
case class AddEntryIntoDictionary(code: SubphaseBatchIdentification.Code, bindings: List[Binding]) extends Message
case class GiveMeDictionary() extends Message
case class TakeDictionary(dictionary: Map[SubphaseBatchIdentification.Code,List[Binding]]) extends Message
