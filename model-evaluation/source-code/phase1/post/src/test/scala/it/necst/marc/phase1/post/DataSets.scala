package it.necst.marc.phase1.post

import it.necst.marc.data._
import it.necst.marc.phase1.data.Result

/**
 * Created by andreadamiani on 01/11/15.
 */
object DataSets {
  val feat0 = Feature("feat0", 0, Some(true), FeatureSpec(FeatureType.INSTANTANEOUS, None, FeatureMonotony.ASCENDANT, Bounds(Some(1000.0), None)))
  val feat1 = Feature("feat1", 1, None, FeatureSpec(FeatureType.CATEGORICAL, None, FeatureMonotony.NONE, Bounds(Some(0.0), Some(2.0))))
  val feat2 = Feature("feat2", 2, None, FeatureSpec(FeatureType.INSTANTANEOUS, None, FeatureMonotony.NONE, Bounds(Some(100.0), Some(200.0))))
  val feat3 = Feature("feat3", 3, None, FeatureSpec(FeatureType.INSTANTANEOUS, None, FeatureMonotony.NONE, Bounds(Some(0.0), Some(100.0))))
  val feat4 = Feature("feat4", 4, None, FeatureSpec(FeatureType.CATEGORICAL, None, FeatureMonotony.NONE, Bounds(Some(3.0), Some(5.0))))
  val feat5 = Feature("feat5", 5, None, FeatureSpec(FeatureType.CATEGORICAL, None, FeatureMonotony.NONE, Bounds(Some(6.0), Some(8.0))))
  val feat6 = Feature("feat6", 6, None, FeatureSpec(FeatureType.INSTANTANEOUS, None, FeatureMonotony.NONE, Bounds(Some(10.0), Some(99.0))))

  val feats = feat6 :: feat4 :: feat2 :: feat0 :: feat5 :: feat3 :: feat1 :: Nil

  val line00 = 1000.0 :: 0.0 :: 100.0 :: 3.0 :: 3.0 :: 6.0 :: 10.0 :: Nil
  val line01 = 1001.0 :: 0.0 :: 101.0 :: 2.0 :: 3.0 :: 6.0 :: 11.0 :: Nil
  val line02 = 1002.0 :: 0.0 :: 102.0 :: 1.0 :: 5.0 :: 6.0 :: 12.0 :: Nil
  val line03 = 1003.0 :: 0.0 :: 103.0 :: 2.0 :: 5.0 :: 6.0 :: 13.0 :: Nil
  val line04 = 1004.0 :: 0.0 :: 104.0 :: 3.0 :: 3.0 :: 6.0 :: 14.0 :: Nil
  val line05 = 1005.0 :: 0.0 :: 105.0 :: 3.0 :: 3.0 :: 6.0 :: 15.0 :: Nil
  val line06 = 1006.0 :: 0.0 :: 104.0 :: 2.0 :: 5.0 :: 6.0 :: 16.0 :: Nil
  val line07 = 1007.0 :: 0.0 :: 103.0 :: 1.0 :: 5.0 :: 6.0 :: 17.0 :: Nil
  val line08 = 1008.0 :: 0.0 :: 102.0 :: 1.0 :: 3.0 :: 6.0 :: 18.0 :: Nil
  val line09 = 1009.0 :: 0.0 :: 101.0 :: 2.0 :: 3.0 :: 6.0 :: 19.0 :: Nil
  val line10 = 1010.0 :: 1.0 :: 100.0 :: 3.0 :: 5.0 :: 7.0 :: 20.0 :: Nil
  val line11 = 1011.0 :: 1.0 :: 101.0 :: 2.0 :: 5.0 :: 7.0 :: 21.0 :: Nil
  val line12 = 1012.0 :: 1.0 :: 102.0 :: 1.0 :: 3.0 :: 7.0 :: 22.0 :: Nil
  val line13 = 1013.0 :: 1.0 :: 103.0 :: 2.0 :: 3.0 :: 7.0 :: 23.0 :: Nil
  val line14 = 1014.0 :: 1.0 :: 104.0 :: 3.0 :: 5.0 :: 7.0 :: 24.0 :: Nil
  val line15 = 1015.0 :: 1.0 :: 105.0 :: 3.0 :: 5.0 :: 7.0 :: 25.0 :: Nil
  val line16 = 1016.0 :: 1.0 :: 104.0 :: 2.0 :: 3.0 :: 7.0 :: 26.0 :: Nil
  val line17 = 1017.0 :: 1.0 :: 103.0 :: 1.0 :: 3.0 :: 7.0 :: 27.0 :: Nil
  val line18 = 1018.0 :: 1.0 :: 102.0 :: 2.0 :: 5.0 :: 7.0 :: 28.0 :: Nil
  val line19 = 1019.0 :: 1.0 :: 101.0 :: 3.0 :: 5.0 :: 7.0 :: 29.0 :: Nil
  val line20 = 1020.0 :: 2.0 :: 100.0 :: 2.0 :: 3.0 :: 8.0 :: 30.0 :: Nil
  val line21 = 1021.0 :: 2.0 :: 101.0 :: 1.0 :: 3.0 :: 8.0 :: 31.0 :: Nil
  val line22 = 1022.0 :: 2.0 :: 102.0 :: 1.0 :: 5.0 :: 8.0 :: 32.0 :: Nil
  val line23 = 1023.0 :: 2.0 :: 103.0 :: 2.0 :: 5.0 :: 8.0 :: 33.0 :: Nil
  val line24 = 1024.0 :: 2.0 :: 104.0 :: 3.0 :: 3.0 :: 8.0 :: 34.0 :: Nil
  val line25 = 1025.0 :: 2.0 :: 105.0 :: 2.0 :: 3.0 :: 8.0 :: 35.0 :: Nil
  val line26 = 1026.0 :: 2.0 :: 104.0 :: 1.0 :: 5.0 :: 8.0 :: 36.0 :: Nil
  val line27 = 1027.0 :: 2.0 :: 103.0 :: 2.0 :: 5.0 :: 8.0 :: 37.0 :: Nil
  val line28 = 1028.0 :: 2.0 :: 102.0 :: 3.0 :: 3.0 :: 8.0 :: 38.0 :: Nil
  val line29 = 1029.0 :: 2.0 :: 101.0 :: 2.0 :: 3.0 :: 8.0 :: 39.0 :: Nil

  val data = line00 :: line01 :: line02 :: line03 :: line04 :: line05 :: line06 :: line07 :: line08 :: line09 :: line10 :: line11 :: line12 :: line13 :: line14 :: line15 :: line16 :: line17 :: line18 :: line19 :: line20 :: line21 :: line22 :: line23 :: line24 :: line25 :: line26 :: line27 :: line28 :: line29 :: Nil

  val result = Result(feats, data, false)
}
