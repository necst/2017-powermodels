//TODO - Outdated code
/*package it.necst.marc.phase1.post

import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase1.post.data.{ConfigurationFeature, PhaseConfiguration, Configuration}

/**
 * Created by andreadamiani on 01/11/15.
 */
object Configurations {
  val feat1 = ConfigurationFeature("feat1", None)
  val feat2 = ConfigurationFeature("feat2", Some(true))
  val feat3 = ConfigurationFeature("feat3", Some(true))
  val feat4 = ConfigurationFeature("feat4", None)
  val feat5 = ConfigurationFeature("feat5", None)

  val phase2Acfg = PhaseConfiguration(Phase2Subphase.A, feat3 :: feat4 :: feat1 :: feat2 :: feat5 :: Nil)
  val phase2Bcfg = PhaseConfiguration(Phase2Subphase.B, feat3 :: feat1 :: feat2 :: Nil)
  val phase2Ccfg = PhaseConfiguration(Phase2Subphase.C, feat3 :: feat4 :: feat2 :: Nil)

  val cfg = Configuration(phase2Acfg :: phase2Bcfg :: phase2Ccfg :: Nil, "feat5")

  val emptyCfg= Configuration(PhaseConfiguration(Phase2Subphase.A, Nil) :: PhaseConfiguration(Phase2Subphase.B, Nil) :: PhaseConfiguration(Phase2Subphase.C, Nil) :: Nil, "feat5")
}
*/