package it.necst.marc.phase0.data

import it.necst.marc.data.{AbstractConfiguration, Feature}
import it.necst.marc.data.support.URIHelper

/**
 * Created by andreadamiani on 03/07/15.
 */
case class Configuration(dataURI:String) extends AbstractConfiguration{
  require(dataURI!=null && URIHelper.isValid(dataURI), "A valid MARC URI is required.")
}