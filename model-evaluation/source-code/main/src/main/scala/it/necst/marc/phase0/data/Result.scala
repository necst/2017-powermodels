package it.necst.marc.phase0.data

import it.necst.marc.data.{Sample, Feature}

import scalaSci.RichDouble2DArray

/**
 * Created by andreadamiani on 04/06/15.
 */
case class Result(data:List[Sample], features:List[Feature]){
  require(features!=null && features.nonEmpty, "Feature mappings must be defined.")
  require(data!=null && data.nonEmpty, "Data must be present.")
  private val featuresInv = for (feature <- features) yield feature.name.toLowerCase
  require(featuresInv.size==featuresInv.toSet.size, "Each feature can be mapped only once.")
  require(features.count(_.is_time.isDefined)==1, "There must be one and only one time feature.")

  val dataForProcessing:RichDouble2DArray = new RichDouble2DArray((for (row <- data) yield row.sample.toArray).toArray)
}


//TODO remove when rapture option divergent implicit solved
//case class Sample(sample:List[Double])