package it.necst.marc.phase0.data.internal

import it.necst.marc.data.support.ExtractableEnumeration

/**
  * Created by andrea on 01/01/16.
  */
object Phase0Stats extends ExtractableEnumeration {
  val DATA_MANAGEMENT         = Value("DataManagement")
}
