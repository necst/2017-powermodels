package it.necst.marc.phase0.data

/**
 * Created by andreadamiani on 04/06/15.
 */
case class MathStat(processedRowCount:Long, rowSize:Int) extends it.necst.marc.data.MathStats