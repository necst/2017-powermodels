package it.necst.marc.api

import java.nio.file.{Files, Paths, Path}

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import it.necst.marc.api.actors.{StartInsertDataInMarc, LoaderActor}
import it.necst.marc.api.helpers.FieldValidator
import it.necst.marc.data.support.MarcLogger
import org.apache.commons.validator.routines.EmailValidator
import org.scalatra.{AsyncResult, ScalatraServlet}
import org.scalatra.FutureSupport

import scala.concurrent.{Future, ExecutionContext}

/**
  * Created by andrea on 14/01/16.
  */
class FileServlet() extends ScalatraServlet with FutureSupport {

  val system = ActorSystem("FileServlet",ConfigFactory.load("actor_configuration"))


  protected implicit def executor: ExecutionContext = system.dispatcher



  get("/") {
    getXML
  }


  //chiedere la mail su cui mandare l'URI dei dati
  post("/insert"){
    try {
      val folder: String = FieldValidator.validate(params.get("folder"), true, "Folder not found in FTP server.") {
        (folder: String) =>
          val path = Paths.get(LoaderActor.FTP_DIRECTORY, folder)
          Files.exists(path) && Files.isDirectory(path) && Files.isReadable(path)
      }

      val email: String = FieldValidator.validate(params.get("email"), true, "Email address is not valid.") {
        EmailValidator.getInstance().isValid
      }

      val username: String = FieldValidator.require(params.get("username"))

      val jobname: String = FieldValidator.require(params.get("jobname"))

      val separator: String = FieldValidator.require(params.get("separator"))

      val timeFeatureIndex: Int = FieldValidator.validate(params.get("time"), true, "Please insert a non-negative integer indicating the column representing time in your datasets.") {
        f => try {
          f.toInt
          true
        } catch {
          case e: NumberFormatException => false
        }
      }.toInt

      new AsyncResult {
        val is =
          Future {
            val actor = system.actorOf(LoaderActor.props(folder, separator, timeFeatureIndex, username, jobname, email))
            actor ! StartInsertDataInMarc()
          }
      }
      redirect("/")
    } catch {
      case e:IllegalArgumentException =>
        FieldValidator.getErrorPage(getXML, e.getMessage)
    }
  }

  private val getXML =
    <html>
      <head>
      </head>
      <body>
        <form action="/file/insert" method="post">
          <div class="row text-center">
            <h1>Insert all data to start writing your data in MARC system</h1>
          </div>
          <div>
            <label for="folder" class="col-md-2">
              Your FTP folder:
            </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="folder" id="folder" placeholder="Enter your FTP folder name"/>
              </div>
              <div class="col-md-1">
                <i class="fa fa-lock fa-2x"></i>
              </div>
            </div>
            <div>
              <label for="jobname" class="col-md-2">
                Job Name:
              </label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="jobname" id="jobname" placeholder="Enter jobname"/>
                </div>
                <div class="col-md-1">
                  <i class="fa fa-lock fa-2x"></i>
                </div>
              </div>
              <div>
                <label for="time" class="col-md-2">
                  Index time feature:
                </label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="time" id="time" placeholder="Enter the time feature index"/>
                  </div>
                  <div class="col-md-1">
                    <i class="fa fa-lock fa-2x"></i>
                  </div>
                </div>
                <div>
                  <label for="username" class="col-md-2">
                    Username:
                  </label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="username" id="username" placeholder="Enter username"/>
                    </div>
                    <div class="col-md-1">

                    </div>
                  </div>
              <div>
                <label for="separator" class="col-md-2">
                  Separator:
                </label>
            <div class="col-md-9 form-group">
              <label class="radio-inline">
                <input type="radio" name="separator" id="oneday" value=",">
                  ,
                </input>
              </label>
              <label class="radio-inline">
                <input type="radio" name="separator" id="oneday" value=";">
                  ;
                </input>
              </label>
              <label class="radio-inline">
                <input type="radio" name="separator" id="oneweek" value="-">
                  -
                </input>
              </label>
              <label class="radio-inline">
                <input type="radio" name="separator" id="oneweek" value=":">
                  :
                </input>
              </label>
            </div>
                </div>
                  <div>
                    <label for="email" class="col-md-2">
                      Email:
                    </label>
                    <div class="col-md-9">
                      <input type="email" class="form-control" name="email" id="email" placeholder="Enter email address"/>
                        <p class="help-block">
                          Example: yourname@domain.com
                        </p>
                      </div>
                      <div class="col-md-1">
                        <i class="fa fa-lock fa-2x"></i>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2">
                      </div>
                      <div class="col-md-10">
                        <input type="submit" value="Submit" class="btn btn-info" />
                          <input type="reset" value="Reset" class="btn btn-info"/>
                        <p class="help-block">
                          WARNING!!! FTP folder is not permanent: as soon as a dataset gets correctly loaded in MARC, it may be permanently deleted from your FTP folder!
                        </p>
                          </div>
                        </div>
                      </form>
      </body>
    </html>
}
