package it.necst.marc.api

import java.io.File
import java.nio.file.{Files, Path}
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import it.necst.marc.api.actors.{ResultActor, StartDownloadResults}
import org.scalatra.{FutureSupport, ScalatraServlet}

import scala.collection.mutable
import scala.concurrent.{Await, ExecutionContext, Future}
import akka.pattern._

import scala.util.{Success, Try}

object ResultComputation {
  private val ongoing = mutable.Map[String, Future[Path]]()
  private val available = mutable.Map[String, Result]()

  private val system = ActorSystem("ResultServlet",ConfigFactory.load("actor_configuration"))

  private object CleanupThread extends Thread{

    final val timeoutMillis: Long = 3600000 //1 Hour

    override def run(): Unit = {
      super.run()
      val currentTime = System.currentTimeMillis()
      for(a <- available.values){
        if(currentTime - a.creation > timeoutMillis){
          a.tryDelete()
        }
      }
      for(a <- available.filter(!_._2.isValid())){
        available.remove(a._1)
      }
      Thread.sleep(timeoutMillis)
    }
  }
  CleanupThread.setDaemon(true)
  CleanupThread.start()

  private object FutureResult {


    protected implicit def executor: ExecutionContext = system.dispatcher

    def apply(uri:String)(body: => Path) = {
      val future = Future(body)
      ongoing.put(uri, future)
      future onComplete{
        value:Try[Path] =>

          if(ongoing contains uri){
            ongoing remove uri
          }

          value match {
            case Success(path) =>
              available.put(uri, new Result(path, System.currentTimeMillis()))
            case _ => ;
          }
      }
    }
  }
  class Result(val path:Path, val creation:Long) {
    private var inuse = false
    private var valid = true

    def get(): Option[File] = {
      synchronized{
        if(!valid){
          return None
        }
        inuse = true
        Some(path.toFile)
      }
    }

    def isValid() = valid

    def release() = {
      synchronized{
        inuse = false
      }
    }

    def tryDelete() = {
      synchronized{
        if(!inuse){
          valid = false
          Files.deleteIfExists(path)
        }
      }
    }
  }

  def get(uri:String): Option[Result] = {
    val config = ConfigFactory.load("api_configuration")

    synchronized{
      if(available contains uri){
        return Some(available(uri))
      } else {
        if(!(ongoing contains uri)){
          FutureResult(uri) {
            val redisExternalLoadBalancer:String = config.getString(config.getString("name")+".remote_addresses.external")
            val actor = system.actorOf(ResultActor.props(redisExternalLoadBalancer))
            implicit val timeout = Timeout(10, TimeUnit.MINUTES)

            val dataFiles = Await.result(actor ? StartDownloadResults(uri), timeout.duration).asInstanceOf[(List[File],Path)]
            val fileNames = (for(file <- dataFiles._1) yield file.getAbsolutePath).toIterable
            val zipName = System.currentTimeMillis()+".zip"
            generateZipResult("/run/jetty/"+zipName,fileNames)  match {
              case Some(file) =>
                for(file<-dataFiles._1) file.delete()
                Files.delete(dataFiles._2)
                file.toPath
              case _ =>
                for(file<-dataFiles._1) file.delete()
                Files.delete(dataFiles._2)
                throw new RuntimeException("Unable to create file")
            }
          }
        }
        None
      }
    }
  }

  private def generateZipResult(out: String, files: Iterable[String]) = {
    import java.io.{ BufferedInputStream, FileInputStream, FileOutputStream }
    import java.util.zip.{ ZipEntry, ZipOutputStream }

    val zip = new ZipOutputStream(new FileOutputStream(out))

    files.foreach { name =>
      zip.putNextEntry(new ZipEntry(name))
      val in = new BufferedInputStream(new FileInputStream(name))
      var b = in.read()
      while (b > -1) {
        zip.write(b)
        b = in.read()
      }
      in.close()
      zip.closeEntry()
    }
    zip.close()
    Some(new File(out))
  }
}

/**
  * Created by andrea on 18/01/16.
  */
class ResultServlet(redisExternalLoadBalancer: String) extends ScalatraServlet with FutureSupport{

  val system = ActorSystem("ResultServlet",ConfigFactory.load("actor_configuration"))

  protected implicit def executor: ExecutionContext = system.dispatcher

  get("/download"){
    params.get("uri") match {
      case Some(uri) if uri.nonEmpty =>
        //TODO check if dataUri available
          ResultComputation.get(uri) match {
            case Some(result) =>
              try {
                result.get() match {
                  case Some(file) =>
                      contentType = "application/zip"
                      response.setHeader("Content-Disposition", "attachment; filename=" + file.getName)
                      file
                  case None =>
                    contentType = "text/html"
                    <html>
                      <head>
                        <title>RESULTS EXPIRED</title>
                        <script>
                          function refresh() {{
                          window.location.reload(true);
                          }}
                        </script>
                      </head>
                      <body>
                        <p>Your results have expired.</p>
                        <p>
                          <a href="#" onClick="refresh()">Click here to require the generation of a new bundle of results.</a>
                        </p>
                      </body>
                    </html>
                  }
                } finally {
                  result.release()
                }
            case _ =>
              contentType = "text/html"
              <html>
                <head>
                  <title>WAIT...</title>
                  <script>
                    function refresh() {{
                      window.location.reload(true);
                    }}

                    setTimeout(refresh, 10000);
                  </script>
                </head>
                <body>
                  <p>Wait while we prepare your result bundle.<br/>
                    This may take a while.</p>
                  <p>The page is going to automacially refresh and,
                    as soon as your result are ready, the download
                    will automatically start.</p>
                  <p><a href="#" onClick="refresh()">If the page does not reload in 10 seconds, please click here.</a>
                  </p>
                </body>
              </html>
          }
      case _ =>
        contentType = "text/html"
        <html>
          <head>
            <title>ERROR</title>
          </head>
          <body>
            <p>URI is required. <a href="/result">Try Again</a></p>
          </body>
        </html>
    }
  }

  get("/"){
    getXML
  }

  private val getXML =
    <html>
      <head>
      </head>
      <body>
        <form action="/result/download" method="get">
          <div class="row text-center">
            <h1>View your results</h1>
          </div>
          <div class="row text-center">
            <h2></h2>
          </div>
          <br></br>
          <div class="row">
            <label for="file" class="col-md-2">
              Data URI received:
            </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="uri" id="uri" placeholder="Enter the data uri"/>
            </div>
            <div class="col-md-1">
              <i class="fa fa-lock fa-2x"></i>
            </div>
          </div>
          <br></br>
          <input type="submit" value="Submit" class="btn btn-info" />
          <input type="reset" value="Reset" class="btn btn-info"/>
        </form>
      </body>
    </html>
}
