package it.necst.marc.api

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import it.necst.marc.api.helpers.FieldValidator
import it.necst.marc.entrypoint.actors.UserRequest
import org.apache.commons.validator.routines.EmailValidator
import org.scalatra.servlet.{MultipartConfig, FileUploadSupport}
import org.scalatra.{FutureSupport, ScalatraServlet}

import scala.concurrent.ExecutionContext
import scala.xml.XML

/**
  * Created by andrea on 17/01/16.
  */

class SimulationServlet(entryPointLoadBalancer: String) extends ScalatraServlet with FutureSupport with FileUploadSupport{

  final val disabledPhases = "phase2b" :: "phase3" :: Nil

  val system = ActorSystem("SimulationServlet",ConfigFactory.load("actor_configuration"))


  configureMultipartHandling(MultipartConfig(maxFileSize = Some(10*1024*1024)))

  protected implicit def executor: ExecutionContext = system.dispatcher

  private val loadbalancer = system.actorSelection(entryPointLoadBalancer)

  post("/start") {

    try {
      val phaseRequested: String = FieldValidator.validate(params.get("phase"), true, "The phase you selected is currently not available.") {
        f => f.nonEmpty && !disabledPhases.contains(f)
      }
      val email: String = FieldValidator.validate(params.get("email"), true, "Email address is not valid.") {
        EmailValidator.getInstance().isValid
      }


      val file = fileParams("config-file")
      val xmlFile = XML.loadString(new String(file.get()))

      loadbalancer ! UserRequest(xmlFile.toString(), phaseRequested, email)

      redirect("/")
    } catch {
      case e:IllegalArgumentException =>
        FieldValidator.getErrorPage(getXML, e.getMessage)
    }

  }

  get("/"){
    getXML
  }

  private val getXML =
    <html>
      <head>
      </head>
      <body>
        <form action="/simulate/start" method="post" enctype="multipart/form-data">
          <div class="row text-center">
            <h1>Start a simulation.</h1>
          </div>
          <div class="row text-center">
            <h2>Choose your configuration file and the result you are interested in</h2>
          </div>
          <br></br>
          <div class="row">
            <label for="file" class="col-md-2">
              Choose file:
            </label>
            <div class="col-md-9">
              <input type="file" name="config-file" />
            </div>
            <div class="col-md-1">
              <i class="fa fa-lock fa-2x"></i>
            </div>
          </div>
          <br></br>
          <div class="row">
            <label for="file" class="col-md-2">
              Email:
            </label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="email" id="email" placeholder="Enter your email"/>
            </div>
            <div class="col-md-1">
              <i class="fa fa-lock fa-2x"></i>
            </div>
          </div>
          <br></br>
          <div class="row">
            <label for="phase" class="col-md-2">
              Phase to compute:
            </label>
            <div class="col-md-9">
              <select name="phase">
                <option disabled="disabled">PHASE 1</option>
                <option value="phase1">- [MAIN] Preprocessing</option>
                <option value="phase1post">- [POST] Data Batching</option>
                <option disabled="disabled">PHASE 2</option>
                <option value="phase2a">- [A] Device model</option>
                <option value="phase2b">- [B] User model</option>
                <option value="phase2c">- [C] Environment model</option>
                <option disabled="disabled">PHASE 3</option>
                <option value="phase3pre">- [PRE ]Per-Configuration Simulation</option>
                <option value="phase3">- [MAIN] Full Simulation</option>
              </select>
            </div>
            <div class="col-md-1">
              <i class="fa fa-lock fa-2x"></i>
            </div>
          </div>
          <br></br>
          <input type="submit" value="Submit" class="btn btn-info" />
          <input type="reset" value="Reset" class="btn btn-info"/>
        </form>
      </body>
  </html>



}
