package it.necst.marc.api.helpers

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.routing.BalancingPool
import it.necst.marc.data.Sample

/**
  * Created by andrea on 24/12/15.
  */

object CSVCollector{

  def props(dataString: List[String],
            separator: String,
            timeFeatureIndex: Int): Props = Props(new CSVCollector(dataString,separator,timeFeatureIndex))
}


class CSVCollector(dataString: List[String],
                  separator: String,
                  timeFeatureIndex: Int) extends Actor{

  private var waiting: Int = 0
  private var routerRef: ActorRef = _
  private var dataCreated: List[Sample] = Nil
  private var caller: ActorRef = _

  def receive = initState

  def initState: Receive = {
    case StartCollector() =>
      try {
        routerRef = ActorSystem("Test").actorOf(BalancingPool(200).props(CSVActor.props(separator, timeFeatureIndex)), name = "csvActor")
        caller = sender()
        sendRequests()
        context.become(collectState)
      } catch {
        case e:Throwable =>
          caller ! e
      }

    case e:Throwable =>
      caller ! e

  }

  def collectState: Receive = {
    case DataCreated(samples) =>
      try {
        waiting = waiting - 1
        dataCreated = dataCreated ::: samples
        if (waiting == 0) {

          val timeIndex = 0
          dataCreated = dataCreated.sortWith(_.sample(timeIndex) < _.sample(timeIndex))

          caller ! dataCreated
        }
      } catch {
        case e:Throwable =>
          caller ! e
      }

    case e:Throwable =>
      caller ! e
  }

  private def sendRequests(): Unit = {

    val group = 100

    val size = if(dataString.length < group ) dataString.length else (dataString.length.toDouble / group).ceil.toInt

    val startGroup = if(dataString.length < group ) group else 0

    def send(current: Int,startIndex: Int, finishIndex: Int): Unit = {
      if(current <= group){
        routerRef ! StartScan(dataString.slice(startIndex,finishIndex))
        waiting = waiting + 1
        send(current+1,startIndex+size,finishIndex+size)
      }
    }

    send(startGroup,0,size)
  }

}
