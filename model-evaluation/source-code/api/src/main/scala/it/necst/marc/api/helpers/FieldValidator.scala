package it.necst.marc.api.helpers

import scala.xml.transform.{RuleTransformer, RewriteRule}
import scala.xml._

/**
  * Created by andreadamiani on 25/02/16.
  */
object FieldValidator {
  def validate(field:Option[String], required: Boolean, error: String)(validator: (String)=>Boolean):String = {
    val chkd4req = if(required){Some(require(field))}else{field}
    chkd4req match {
      case Some(field) =>
        if(validator(field)){
          field
        } else {
          throw new IllegalArgumentException(error)
        }
      case None => "";
    }
  }

  def require(field:Option[String]):String = {
    field match {
      case Some(field) => field
      case None => throw new IllegalArgumentException("Please, fill in all required fields.");
    }
  }

  def getErrorPage(page:Node, errorMessage:String):Node = {
    new RuleTransformer(
      AddChildrenTo(
        "head",
        <script language="Javascript">
          window.onload = function(event) {{
            alert({Unparsed(s"'$errorMessage'")});
          }};
        </script>)).transform(page).head
  }

  private class AddChildrenTo(label: String, newChild: Node) extends RewriteRule {
    override def transform(n: Node) = n match {
      case n @ Elem(_, `label`, _, _, _*) => addChild(n, newChild)
      case other => other
    }

    def addChild(n: Node, newChild: Node) = n match {
      case Elem(prefix, label, attribs, scope, child @ _*) =>
        Elem(prefix, label, attribs, scope, true, child ++ newChild : _*)
      case _ => throw new IllegalArgumentException("Can only add children to elements!")
    }
  }

  private class AddAttributeTo(label: String, newAttribute:(String, Node)) extends RewriteRule {
    override def transform(n:Node) = n match {
      case n @ Elem(_, `label`, _, _, _*) => addAttribute(n, newAttribute)
      case other => other
    }

    def addAttribute(n:Node, newAttribute: (String, Node)) = n match {
      case elem:Elem => elem % Attribute(None, newAttribute._1, newAttribute._2, Null)
      case _ => throw new IllegalArgumentException("Can only add attributes to elements!")
    }
  }

  private object AddChildrenTo{
    def apply(label: String, newChild: Node) = new AddChildrenTo(label, newChild)
  }

  private object AddAttributeTo{
    def apply(label: String, newAttributeKey: String, newAttributeValue: Node) = new AddAttributeTo(label, (newAttributeKey, newAttributeValue))
  }
}
