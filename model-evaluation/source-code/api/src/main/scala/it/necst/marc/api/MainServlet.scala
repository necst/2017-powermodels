package it.necst.marc.api

import org.scalatra.ScalatraServlet
import org.scalatra.scalate.ScalateSupport

/**
  * Created by andrea on 14/01/16.
  */
class MainServlet extends ScalatraServlet with ScalateSupport {
  get("/") {
    getXML
  }


  private val getXML =
    <html>
      <body>
        <div class="row text-center">
          <h1>Welcome in MARC!</h1>
        </div>
        <div class="row col-md-12">
          <a href="/file">Click here to insert your data</a>
        </div>
        <div class="row col-md-12">
          <a href="/simulate">Click here to start your computation</a>.
        </div>
        <div class="row col-md-12">
          <a href="/result">Click here to see your results</a>.
        </div>

      </body>
    </html>
}