package it.necst.marc.phase3.data

import it.necst.marc.data.AbstractConfiguration

/**
 * Created by andreadamiani on 14/07/15.
 */
case class Configuration(levelSize:Double, limit:Int) extends AbstractConfiguration{
  require(levelSize>0)
  require(limit>0)
}
