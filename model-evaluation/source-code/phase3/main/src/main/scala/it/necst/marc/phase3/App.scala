package it.necst.marc.phase3

import akka.actor.{ActorRef, ActorSystem}
import com.typesafe.config.{Config, ConfigFactory, ConfigValue}
import it.necst.marc.data.support.{MarcLogger, NamesPhases}
import it.necst.marc.infrastructure.actors.{CommunicationActor, LoadBalancer}
import it.necst.marc.phase3.actors.InternalWorker
import scala.collection.JavaConversions._

import scala.annotation.tailrec

/**
 * Created by andreadamiani on 30/04/15.
 */
object App {

  private val logger = MarcLogger(getClass.getName)

  def main(args : Array[String]) {
    logger.info("Set up Phase3")

    val phase3Configuration = ConfigFactory.load("phase3_configuration")
    val phaseName = phase3Configuration.getString("name")
    val numberOfCommunicationActors = phase3Configuration.getInt(phaseName+".number_communication_actors")
    val timeoutRequestPreviousPhase = phase3Configuration.getInt(phaseName+".timeout_request_previous_phase")
    val timeoutReadyState = phase3Configuration.getInt(phaseName+".timeout_ready_state")

    val remoteAddresses = phase3Configuration.getConfig(phaseName+".remote_addresses")

    val remoteAddressMap: Map[NamesPhases.Value,String] = loadMapRemoteAddresses(remoteAddresses)

    //Initialization communication actors system
    val _systemCommunicationActors = ActorSystem("CommunicationActorSystem",ConfigFactory.load("communicationActor"))
    val informationCommunicationActors =
      initializeCommunicationActors(_systemCommunicationActors,numberOfCommunicationActors,
        remoteAddressMap,timeoutRequestPreviousPhase,
        timeoutReadyState,phaseName,phase3Configuration)

    //initialization load balancer
    val _systemLoadBalancer = ActorSystem("LoadBalancerSystem",ConfigFactory.load("loadBalancer"))
    val loadBalancer = _systemLoadBalancer.actorOf(LoadBalancer.props(informationCommunicationActors._2),"loadBalancerPhase3")
    System.gc()


  }

  private def initializeCommunicationActors(system: ActorSystem,numberOfActors: Int,
                                            remoteAddressMap:Map[NamesPhases.Value,String], timeoutRequestPreviousPhase: Int,
                                            timeoutReadyState:Int, phaseName:String, phaseConfig: Config):(List[String],List[ActorRef]) = {
    @tailrec def createLists(currentIteration: Int, numberOfActors: Int, system: ActorSystem,
                             paths: List[String], actorsRef: List[ActorRef]):(List[String],List[ActorRef]) = {
      if(currentIteration >= numberOfActors) (paths,actorsRef)

      else{
        val newActorRef = system.actorOf(CommunicationActor.props(remoteAddressMap,timeoutRequestPreviousPhase,
          timeoutReadyState,phaseName,None,InternalWorker,phaseConfig),"communicationActor"+currentIteration)
        val actorName = "/user/communicationActor"+currentIteration
        val newPaths = paths :+ actorName
        val newRefs = actorsRef :+ newActorRef
        createLists(currentIteration+1,numberOfActors,system,newPaths,newRefs)
      }
    }

    createLists(0,numberOfActors,system,Nil,Nil)

  }

  private def loadMapRemoteAddresses(configuration: Config): Map[NamesPhases.Value,String] = {

    @tailrec def scanConfiguration(toBeProcessed: java.util.Set[java.util.Map.Entry[String, ConfigValue]], currentMap: Map[NamesPhases.Value,String]): Map[NamesPhases.Value,String] = {
      if (toBeProcessed.isEmpty) currentMap

      else {
        val newMap = currentMap + (NamesPhases.withName(toBeProcessed.head.getKey) -> toBeProcessed.head.getValue.render().replace("\"",""))
        scanConfiguration(toBeProcessed.tail, newMap)
      }
    }
    scanConfiguration(configuration.entrySet(), Map[NamesPhases.Value,String]())
  }
}
