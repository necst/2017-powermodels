package it.necst.marc.phase3.actors

import akka.actor.{Actor, ActorRef, Props}
import com.typesafe.config.Config
import it.necst.marc.infrastructure.actors.messages.{InternalComputationCompleted, StartInternalComputation}
import it.necst.marc.infrastructure.support.TraitInternalActor

import scalaSci.RichDouble2DArray
import java.net.URI


/**
 * Created by andrea on 01/06/15.
 */
object InternalWorker extends TraitInternalActor{

  def  props(currentConfiguration: String,actorToReply: ActorRef,
             mapData: Map[URI,(Array[Byte],Int)], configFile: Config): Props = Props(new InternalWorker(currentConfiguration,actorToReply,mapData,configFile))


}


class InternalWorker(val currentConfiguration: String,
                     val actorToReply: ActorRef,
                     val mapData:Map[URI,(Array[Byte],Int)],
                     val configFile: Config) extends Actor{



  def receive: Receive = {
    case StartInternalComputation() =>
      val dataForPhase = parseData(mapData)




      //when computation is finishedv
      val result = null
      val report= null
    actorToReply ! InternalComputationCompleted(report,0,result,0)

  }

  private def parseData(mapData:Map[URI,(Array[Byte],Int)]):RichDouble2DArray = {
    //TODO parse
    null
  }
}
