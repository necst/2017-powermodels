package it.necst.marc.phase3.pre.actors

import akka.actor.{Props, ActorRef, Actor}
import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase3.pre.data.support.SimulationDirection

import scala.collection.mutable.ListBuffer

/**
 * Created by andrea on 31/10/15.
 */

object Collector{

  def props(internalActorRef: ActorRef,
            waitingModels: Int): Props = Props(new Collector(internalActorRef,waitingModels))

}

class Collector(internalActorRef: ActorRef,
                 waitingModels: Int) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private val results: Map[SimulationDirection.Value, ListBuffer[(Code, Array[Long])]] = Map(SimulationDirection.CHARGE -> ListBuffer(), SimulationDirection.DISCHARGE -> ListBuffer())

  private var waitingMessages: Int = 0

  def receive = collectState

  override def preStart() = {
    waitingMessages = waitingModels
  }

  def collectState: Receive = {

    case CollectResults(direction, data, code) =>
      logger.info("Received CollectResults message")
      try{
        waitingMessages = waitingMessages - 1
        logger.info(s"COLLECTOR - STILL WAITING FOR ${waitingMessages} ACTOR B TO COMPLETE")
        if(data.isDefined)
          (code, data.get) +=: results(direction)

        if(waitingMessages == 0){
          val finalized = for (elem<-results) yield elem._1 -> elem._2.toList
          internalActorRef ! SimulationsCompleted(finalized)
        }
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }

    case Deallocate() =>
      context.stop(self)

  }

}
