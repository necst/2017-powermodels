package it.necst.marc.phase3.pre.actors

import java.net.URI

import akka.actor.{ActorSystem, Actor, Props, ActorRef}
import com.typesafe.config.Config
import it.necst.marc.data.{PartialStats, PartialStat, PerfStats, Performance}
import it.necst.marc.data.support.{MarcLogger, ErrorStack, NamesPhases}
import it.necst.marc.infrastructure.support.{DataEncoder, DataCompressor, TraitInternalActor, URIManager}
import it.necst.marc.infrastructure.actors.messages.{ComputationError, InternalComputationCompleted, StartInternalComputation}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification._
import it.necst.marc.phase1.post.data.{ResultFrequencies}
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase2.a.data.Model
import it.necst.marc.phase2.c.data.Mean
import it.necst.marc.phase3.pre.computation.Simulation
import it.necst.marc.phase3.pre.data._
import it.necst.marc.phase3.pre.data.internal.{DataManagementStats, SimulationCodes, MergeConfiguration}
import it.necst.marc.phase3.pre.data.support.SimulationDirection
import rapture.json._
import rapture.json.jsonBackends.jawn._


/**
 * Created by andrea on 29/10/15.
 */

object InternalWorker extends TraitInternalActor{

  def  props(currentConfiguration:String,actorToReply: ActorRef,
             mapData: Map[URI,(Array[Byte],Int)],configFile: Config): Props = Props(new InternalWorker(currentConfiguration,actorToReply,mapData,configFile))

}


class InternalWorker(val currentConfiguration:String,
                     val actorToReply: ActorRef,
                     val mapData:Map[URI,(Array[Byte],Int)],
                     val configFile: Config) extends Actor {

  private val logger = MarcLogger(getClass.getName)
  private val system = ActorSystem("InternalWorker")

  private var configuration: Configuration = _

  private var actors: List[ActorRef] = Nil

  private var initialTimestamp: Long = 0l
  private var performanceStats: List[PartialStat] = Nil

  def receive = initState

  def initState: Receive = {
    case StartInternalComputation() =>
      logger.info("Receive StartInternalComputation message")
      initialTimestamp = System.currentTimeMillis()
      try{
        configuration = Json.parse(currentConfiguration).as[it.necst.marc.phase3.pre.data.Configuration]

        val map = for(item <- mapData) yield (URIManager.getPhaseNameFromURI(item._1)->item._2)

        val initDecompressionTimestamp = System.currentTimeMillis()
        val dataPhase1Post = map.filter(_._1.equals(NamesPhases.PHASE1POST.toString)).head
        val dataStringPhase1Post = new String(DataCompressor.decompress(dataPhase1Post._2._1,dataPhase1Post._2._2))
        val resultPhase1Post = Json.parse(dataStringPhase1Post).as[it.necst.marc.phase1.post.data.ResultFrequencies]

        val dataPhase2a = map.filter(_._1.equals(NamesPhases.PHASE2A.toString)).head
        val dataStringPhase2a = new String(DataCompressor.decompress(dataPhase2a._2._1,dataPhase2a._2._2))
        val resultPhase2a = Json.parse(dataStringPhase2a).as[it.necst.marc.phase2.a.data.Result]

        val dataPhase2c = map.filter(_._1.equals(NamesPhases.PHASE2C.toString)).head
        val dataStringPhase2c = new String(DataCompressor.decompress(dataPhase2c._2._1,dataPhase2c._2._2))
        val resultPhase2c = Json.parse(dataStringPhase2c).as[it.necst.marc.phase2.c.data.Result]
        val finishDecompressionTimestamp = System.currentTimeMillis()

        val mergeConfigurations = Simulation.computeMergeConfigurations(resultPhase1Post)
        val totalActorsC = mergeConfigurations(Phase2Subphase.C).size
        val totalActorsB = mergeConfigurations(Phase2Subphase.B).size

        val loadBalancerActorsC = createInternalActors(totalActorsC,totalActorsB,resultPhase1Post,mergeConfigurations)
        sendModelsC(resultPhase2c.means, loadBalancerActorsC)
        sendModelsA(resultPhase2a.models,loadBalancerActorsC)
        loadBalancerActorsC ! StartSimulation()
        context.become(collectState)
        performanceStats = PartialStat(DataManagementStats.DATA_DECOMPRESSION.toString,finishDecompressionTimestamp-initDecompressionTimestamp)::performanceStats
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          val report = Json(Report(None,Some(error))).toString()
          actorToReply ! ComputationError(report,error)
          for(actor <- actors){
            actor ! Deallocate()
          }
          system.terminate()
          context.stop(self)
      }

    case ThrowsError(error) =>
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      for(actor <- actors){
        actor ! Deallocate()
      }
      system.terminate()
      context.stop(self)

  }

  def collectState: Receive = {
    case SimulationsCompleted(data)=>
      logger.info("Received SimulationCompleted message")
      for(actor <- actors){
        actor ! Deallocate()
      }
      val simulationsList = createPoints(data)
      val finalTimestamp = System.currentTimeMillis()

      val result = Json(Result(simulationsList,configuration.tick,configuration.initial_configurations)).toString()
      val resultByte = DataCompressor.compress(result)
      val resultDim = DataEncoder.byteLength(result)
      val finishCompressionData = System.currentTimeMillis()

      //TODO create report
      performanceStats = PartialStat(DataManagementStats.DATA_COMPRESSION.toString,finalTimestamp-finishCompressionData) :: performanceStats
      val performance = Performance(Some(PerfStats(finalTimestamp-initialTimestamp,Some(PartialStats(performanceStats)))))
      val report = Json(Report(Some(MathStats(Nil,performance)),None)).toString()

      val reportByte = DataCompressor.compress(report)
      val reportDim = DataEncoder.byteLength(report)



      actorToReply ! InternalComputationCompleted(reportByte,reportDim,resultByte,resultDim)
      system.terminate()

    case ThrowsError(error) =>
      val report = Json(Report(None,Some(error))).toString()
      actorToReply ! ComputationError(report,error)
      for(actor <- actors){
        actor ! Deallocate()
      }
      system.terminate()
      context.stop(self)

  }

  private def createPoints(data: Map[SimulationDirection.Value,List[(Code, Array[Long])]]): List[Simulation] = {


    val flatten = data.flatMap{case (k,v) => for (res <- v) yield (res._1, k, res._2)}
    val grouped = flatten.groupBy(_._1).map{case (k,v) => (k, v.map(x => x._2 -> x._3).toMap)}

    val simulations = for(entry <- grouped) yield {
      val code = entry._1
      val charge = entry._2.get(SimulationDirection.CHARGE)
      val discharge = entry._2.get(SimulationDirection.DISCHARGE)
      require(charge.isDefined || discharge.isDefined, "MALFORMED COLLECTED RESULTS")
      val length = Math.max(charge.getOrElse(Array[Long]()).length, discharge.getOrElse(Array[Long]()).length)
      val points = for(i <- 0 until length) yield {
        val chargePoint = charge match {
          case None => None
          case Some(x) => if(i<x.length) Some(x(i)) else None
        }
        val dischargePoint = discharge match {
          case None => None
          case Some(x) => if(i<x.length) Some(x(i)) else None
        }
        Point(i,chargePoint, dischargePoint)
      }
      it.necst.marc.phase3.pre.data.Simulation(code, points.toList)
    }

    simulations.toList
  }

  private def createInternalActors(totalActorsC: Int,
                                   totalActorsB: Int,
                                  resultFrequencies: ResultFrequencies,
                                   mergeConfigurations: Map[Phase2Subphase.Value,Set[MergeConfiguration]]): ActorRef = {

    def createCollector(): ActorRef = {
      system.actorOf(Collector.props(self,totalActorsB * 2),"collector")
    }

    def createLoadBalancerB(codes: List[SimulationCodes],
                            collector: ActorRef,
                            tag: String,
                            simulationDirection: SimulationDirection.Value): ActorRef = {
      system.actorOf(LoadBalancerActorsB.props(totalActorsC,collector,codes,simulationDirection, resultFrequencies),"loadBalancerB"+tag)
    }

    def createLoadBalancerC(loadBalancerActorsBCharge: ActorRef,
                            loadBalancerActorsBDischarge: ActorRef,
                            codes: List[SimulationCodes]): ActorRef = {
      system.actorOf(LoadBalancerActorsC.props(loadBalancerActorsBCharge,loadBalancerActorsBDischarge,codes,resultFrequencies,configuration),"loadBalancerC")
    }

    val collector = createCollector()

    val simulationCodesB = (for(item <- mergeConfigurations(Phase2Subphase.C)) yield item.get).toList
    val loadBalancerBCharge = createLoadBalancerB(simulationCodesB,collector,"charge",SimulationDirection.CHARGE)

    val loadBalancerBDischarge = createLoadBalancerB(simulationCodesB,collector,"discharge",SimulationDirection.DISCHARGE)

    val simulationCodesC = (for(item <- mergeConfigurations(Phase2Subphase.A)) yield item.get).toList
    val loadBalancerC = createLoadBalancerC(loadBalancerBCharge,loadBalancerBDischarge,simulationCodesC)
    actors = collector :: loadBalancerBCharge :: loadBalancerBDischarge :: loadBalancerC :: actors
    loadBalancerC
  }

  private def sendModelsC(means: List[Mean], loadBalancerActorsC: ActorRef) = {
    def send(toBeProcessed: List[Mean]): Unit = {
      if(toBeProcessed.nonEmpty) {
        loadBalancerActorsC ! AddModelC(toBeProcessed.head)
        send(toBeProcessed.tail)
      }
    }
    send(means)
  }

  private def sendModelsA(models: List[Model],loadBalancerActorsC: ActorRef) = {
    def send(toBeProcessed: List[Model]): Unit = {
      if(toBeProcessed.nonEmpty){
        loadBalancerActorsC ! AddModelA(toBeProcessed.head)
        send(toBeProcessed.tail)
      }
    }
    send(models)
  }

  //TODO clean
  /*private def createCodesMaps(mergeConfigurations: List[MergeConfiguration], actors: List[ActorRef]): WildcardMap[SimulationCodes,ActorRef] = {

    def createMap(toBeProcessed: List[MergeConfiguration],actorsRef: List[ActorRef],
                  map: Map[SimulationCodes,ActorRef]): Map[SimulationCodes,ActorRef] = {
      if(toBeProcessed.isEmpty)map
      else{
        createMap(toBeProcessed.tail,actorsRef.tail,map + (toBeProcessed.head.get -> actorsRef.head))
      }
    }

    createMap(mergeConfigurations,actors,Map())
  }*/

}
