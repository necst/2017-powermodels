package it.necst.marc.phase3.pre.data

import it.necst.marc.data.{Performance, AbstractReport}
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code

/**
 * Created by andreadamiani on 26/10/15.
 */

case class Report(content:Option[MathStats], error:Option[String]) extends AbstractReport(content, error)

case class MathStats(byConfiguration:List[ConfigurationMathStats],performance: Performance) extends it.necst.marc.data.MathStats

case class ConfigurationMathStats(groupingCode:Code, linearModelsCount:Int){
  require(linearModelsCount>0, "At least a linear model must be defined for each configuration.")
}
