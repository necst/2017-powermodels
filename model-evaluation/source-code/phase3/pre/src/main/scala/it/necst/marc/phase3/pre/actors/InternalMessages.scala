package it.necst.marc.phase3.pre.actors

import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase2.a.data.Model
import it.necst.marc.phase2.c.data.{Mean, Value}
import it.necst.marc.phase3.pre.data.internal.SimulationCodes
import it.necst.marc.phase3.pre.data.support.SimulationDirection

import scala.collection.mutable.ListBuffer

/**
 * Created by andrea on 29/10/15.
 */
sealed trait Message

//internal worker messages
case class Deallocate() extends Message
case class ThrowsError(error: String) extends Message

//loadbalancerActorsC messages
case class AddModelC(data: Mean) extends Message
case class AddModelA(data: Model) extends Message

//actorC messages
case class ConfigureActorC(data: List[Value]) extends Message
case class AddCodesAndFreq(model: Model,frequency: Double) extends Message
case class StartSimulation() extends Message

//simulatorC messages
case class SimulateC(models: List[(Model,Double)], modelC: List[Value]) extends Message

//loadbalancerActorsB messages
case class AddModelAC(simulation:Option[Array[Long]], code:SimulationCodes) extends Message

//actorB messages
case class AddWeightedModelAC(simulation: Option[Array[Long]], frequency: Double) extends Message
//case class StartSimulation() extends Message  [Already Declared in actorC with same semantics]

//collector
case class CollectResults(direction:SimulationDirection.Value, data: Option[Array[Long]], code:Code) extends Message
case class SimulationsCompleted(data: Map[SimulationDirection.Value, List[(Code, Array[Long])]]) extends Message