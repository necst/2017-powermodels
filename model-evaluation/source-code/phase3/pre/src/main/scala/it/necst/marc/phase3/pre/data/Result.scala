package it.necst.marc.phase3.pre.data

import it.necst.marc.data.Binding
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andreadamiani on 26/10/15.
 */
case class Result(simulations:List[Simulation], tickSize:Double, initial_configuration:SimConfiguration){
  require(tickSize>0, "Tick size must be positive.")
}

case class Simulation(code:Code,points:List[Point], totalCharge:Option[Long], totalDischarge:Option[Long]){
  require(points.nonEmpty, "Empty entries not allowed.")
  private val expressedLevels = for (point <- points) yield point.level
  require(expressedLevels.size == (0 to 100).size, "Every level from 0 to 100 must be uniquely (for both charge and discharge) simulated.")
}

object Simulation{
  def apply(code:Code, point:List[Point]):Simulation = {
    val totalCharge =
      if(point.exists(_.chargeTicks.isDefined))
        Some({(for(p <- point) yield p.chargeTicks.getOrElse(0l)).sum})
      else
        None
    val totalDischarge =
      if(point.exists(_.dischargeTicks.isDefined))
        Some({(for(p <- point) yield p.dischargeTicks.getOrElse(0l)).sum})
      else
        None
    Simulation(code, point, totalCharge, totalDischarge)
  }
}

case class Point(level:Int, chargeTicks:Option[Long], dischargeTicks:Option[Long]){
  require(level>=0 && level<=100)
  require(chargeTicks.isDefined || dischargeTicks.isDefined, "Points require at least one y (level) value.")
  require(chargeTicks.getOrElse(0l)>=0, "Cannot reverse time.")
  require(dischargeTicks.getOrElse(0l)>=0, "Cannot reverse time.")
}
