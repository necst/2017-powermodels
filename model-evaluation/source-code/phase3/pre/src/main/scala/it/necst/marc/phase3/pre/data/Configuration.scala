package it.necst.marc.phase3.pre.data

import it.necst.marc.data.AbstractConfiguration
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andreadamiani on 07/10/15.
 */
case class Configuration(output:it.necst.marc.data.Feature.ID, initial_configurations: SimConfiguration, tick:Double, limit:Int) extends AbstractConfiguration{
  require(tick>0)
  require(limit>0)

  val levels = {
    val list:List[(SimulationDirection.Value, Option[Array[Double]])] = for(direction <- SimulationDirection.values.toList)
      yield{
        direction -> (if(initial_configurations.get(direction).isDefined){
          val initialVal = initial_configurations.get(direction).get.initial_values(0)
          val finalVal = initial_configurations.get(direction).get.final_value
          Some((for(i <- 0 to 100) yield {
            initialVal + ((finalVal-initialVal)/100)*i
          }).toArray)
        } else {
          None
        })
      }
    list.toMap
  }

  val levelSize = (for(direction <- SimulationDirection.values.toList) yield {
    direction -> {
      levels(direction) match {
        case None => None
        case Some(x) => Some((direction match {
          case SimulationDirection.CHARGE => 1
          case SimulationDirection.DISCHARGE => -1
        })*(x(1)-x(0)))
      }
    }
  }).toMap
}

case class SimConfiguration(charge:Option[BaseConf], discharge:Option[BaseConf]){
  def get(direction:SimulationDirection.Value) = direction match {
    case SimulationDirection.CHARGE => charge
    case SimulationDirection.DISCHARGE => discharge
  }
}

case class BaseConf(initial_values:List[Double], final_value:Double)
