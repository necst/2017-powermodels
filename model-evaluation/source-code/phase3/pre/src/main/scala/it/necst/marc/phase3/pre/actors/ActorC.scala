package it.necst.marc.phase3.pre.actors

import akka.actor.{ActorRef, ActorSystem, Props, Actor}
import it.necst.marc.data.FeatureMonotony
import it.necst.marc.data.support.MarcLogger
import it.necst.marc.phase2.a.data.Model
import it.necst.marc.phase2.c.data.Value
import it.necst.marc.phase3.pre.data.Configuration
import it.necst.marc.phase3.pre.data.internal.SimulationCodes


/**
 * Created by andrea on 31/10/15.
 */
object ActorC{

  def props(simulationCodes: SimulationCodes,
            loadBalancerActorsBCharge: ActorRef,
            loadBalancerActorsBDischarge: ActorRef,
            configuration: Configuration): Props = Props(new ActorC(simulationCodes, loadBalancerActorsBCharge,loadBalancerActorsBDischarge,configuration))

}


class ActorC(simulationCodes: SimulationCodes,
             loadBalancerActorsBCharge: ActorRef,
             loadBalancerActorsBDischarge: ActorRef,
             configuration: Configuration) extends Actor{

  private val logger = MarcLogger(getClass.getName)

  private val name = simulationCodes.toString

  private val system = ActorSystem()
  private var chargeSimulator: ActorRef = _
  private var dischargeSimulator: ActorRef = _

  private var modelsA: List[(Model,Double)] = Nil
  private var modelC: List[Value] = Nil

  override def preStart() = {
    chargeSimulator = system.actorOf(ActorCCharge.props(simulationCodes,loadBalancerActorsBCharge,configuration),"chargeSimulator")
    dischargeSimulator = system.actorOf(ActorCDischarge.props(simulationCodes, loadBalancerActorsBDischarge,configuration),"dischargeSimulator")

  }

  def receive: Receive = {

    case ConfigureActorC(mean) =>
      logger.info(s"ACTOR C (${name})\nReceived ConfigureActorC")
      require(modelC == Nil, "ACTOR C ALREADY CONFIGURED!!!!")
      modelC = mean

    case AddCodesAndFreq(model,frequency) =>
      logger.info(s"ACTOR C (${name})\nReceived AddModelA message")
      modelsA = (model,frequency) :: modelsA


    case StartSimulation() =>
      logger.info(s"ACTOR C (${name})\nReceived StartSimulation message")

      chargeSimulator ! SimulateC(modelsA.filter(_._1.outputMonotony == FeatureMonotony.ASCENDANT),modelC)
      dischargeSimulator ! SimulateC(modelsA.filter(_._1.outputMonotony == FeatureMonotony.DESCENDANT),modelC)
  }


}
