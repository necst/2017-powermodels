package it.necst.marc.phase3.pre.actors

import it.necst.marc.data.support.{MarcLogger, ErrorStack}
import akka.actor.{ActorRef, Actor}
import it.necst.marc.phase3.pre.computation.Simulation
import it.necst.marc.phase3.pre.data.Configuration
import it.necst.marc.phase3.pre.data.internal.SimulationCodes
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andrea on 31/10/15.
 */

object CSimulator{

}

abstract class CSimulator(simulationCodes: SimulationCodes,
                           loadBalancerActorsB: ActorRef,
                           simulationDirection: SimulationDirection.Value,
                           configuration: Configuration) extends Actor{

  private val logger = MarcLogger(getClass.getName)
  private val name = simulationCodes + " " + simulationDirection

  def receive: Receive = {
    case SimulateC(data,modelC)=>
      logger.info(s"ACTOR C SIMULATOR ($name)\nReceived Simulate message")
      try{
        loadBalancerActorsB ! AddModelAC(
          if(data.nonEmpty) {
            val alphas = Simulation.computeMeanAlphas(data)
            Simulation.simulateModel(simulationDirection, alphas, modelC, configuration)
          } else
            None,
          simulationCodes)
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }


  }

}
