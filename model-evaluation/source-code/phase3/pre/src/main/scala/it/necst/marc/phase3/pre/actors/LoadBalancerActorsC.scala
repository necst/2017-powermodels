package it.necst.marc.phase3.pre.actors

import akka.actor.{ActorSystem, ActorRef, Actor, Props}
import it.necst.marc.data.support.{MarcLogger, ErrorStack, WildcardMap, Wildcard}
import it.necst.marc.phase1.post.data.ResultFrequencies
import it.necst.marc.phase1.post.data.support.Phase2Subphase
import it.necst.marc.phase2.a.data.Model
import it.necst.marc.phase2.c.data.Value
import it.necst.marc.phase3.pre.computation.Simulation
import it.necst.marc.phase3.pre.data.Configuration
import it.necst.marc.phase3.pre.data.internal._

import scala.annotation.tailrec

/**
 * Created by andrea on 31/10/15.
 */
object LoadBalancerActorsC{

  def props(loadBalancerActorsBCharge: ActorRef,
            loadBalancerActorsBDischarge: ActorRef,
            codes: List[SimulationCodes],
            sourceFrequencies: ResultFrequencies,
            configuration: Configuration): Props =
                                Props(new LoadBalancerActorsC(loadBalancerActorsBCharge,
                                                loadBalancerActorsBDischarge,codes,sourceFrequencies,configuration))

}


class LoadBalancerActorsC(loadBalancerActorsBCharge: ActorRef,
                          loadBalancerActorsBDischarge: ActorRef,
                          codes: List[SimulationCodes],
                          sourceFrequencies: ResultFrequencies,
                          configuration: Configuration) extends Actor{

  private val logger = MarcLogger(getClass.getName)
  private val system = ActorSystem("LoadBalancerActorsC")
  private var mapActorsCodes: WildcardMap[SimulationCodes,ActorRef] = WildcardMap()

  override def preStart(): Unit = {
    mapActorsCodes = createMapActorsCodes()
  }

  def receive: Receive = {
    case AddModelC(mean) =>
      logger.info("Received AddModelC message")
      try{
        sendConfigureActorC(mean.code, mean.groupCodes, mean.mean_values)
      }catch{
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }

    case AddModelA(model) =>
      logger.info("Received AddModelA message")
      try{
        val codesAndFreq = Simulation.computeMergeSites(Phase2Subphase.A,model.code,sourceFrequencies)
        sendAddData(codesAndFreq,model)
      } catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          sender() ! ThrowsError(error)
      }


    case StartSimulation() =>
      logger.info("Received StartSimulation message")
      sendStartSimulation()

    case Deallocate() =>
      for(actor <- mapActorsCodes)
        system.stop(actor._2)
      context.stop(self)
  }

  private def createMapActorsCodes(): Map[SimulationCodes,ActorRef] = {

    def createActors(toBeProcessed: List[SimulationCodes],
                     map: Map[SimulationCodes,ActorRef],
                      index: Int):Map[SimulationCodes,ActorRef] = {
      if(toBeProcessed.isEmpty) map
      else{
        val actorRef = system.actorOf(ActorC.props(toBeProcessed.head, loadBalancerActorsBCharge,loadBalancerActorsBDischarge,configuration),"actorC"+index)
        createActors(toBeProcessed.tail,map + (toBeProcessed.head -> actorRef),index+1)
      }
    }
    logger.info(s"LOAD BALANCER C - CREATING ${codes.size} ACTOR C")
    createActors(codes,Map(),0)
  }

  private def sendConfigureActorC(code:it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code, groupCodes:List[it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code], data: List[Value]) = {

    def sendToAll(toBeProcessed:Iterable[ActorRef]): Unit = {
      if(toBeProcessed.nonEmpty){
        toBeProcessed.head ! ConfigureActorC(data)
        sendToAll(toBeProcessed.tail)
      }
    }

    val grouping = Phase2Subphase.groupingRules(Phase2Subphase.C)
    val simulationCodesMap:Map[Phase2Subphase.Value, Code] = Map(Phase2Subphase.C -> Code(code)) ++ (for (i <- groupCodes.indices) yield grouping(i) -> Code(groupCodes(i)))
    val simulationCodes = SimulationCodes(simulationCodesMap ++ (for(phase <- Phase2Subphase.values if !simulationCodesMap.contains(phase)) yield phase -> ***))

    mapActorsCodes.getAll(simulationCodes) match {
      case None => Unit
      case Some(recipients) => sendToAll(recipients)
    }
  }

  private def sendAddData(data: List[(SimulationCodes,Double)],model: Model) = {

    def sendData(toBeProcessed: List[(SimulationCodes,Double)]): Unit = {
      if(toBeProcessed.nonEmpty){
        mapActorsCodes(toBeProcessed.head._1) ! AddCodesAndFreq(model,toBeProcessed.head._2)
        sendData(toBeProcessed.tail)
      }
    }

    sendData(data)

  }

  private def sendStartSimulation() = {
    @tailrec def send(toBeProcessed: List[ActorRef]): Unit = {
      if(toBeProcessed.nonEmpty){
        toBeProcessed.head ! StartSimulation()
        send(toBeProcessed.tail)
      }
    }
    val actors = (for(item <- mapActorsCodes) yield item._2).toList

    send(actors)
  }

}
