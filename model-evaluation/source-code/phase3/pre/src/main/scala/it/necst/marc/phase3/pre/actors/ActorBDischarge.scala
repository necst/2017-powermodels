package it.necst.marc.phase3.pre.actors

import akka.actor.{Props, ActorRef}
import it.necst.marc.phase3.pre.data.internal.SimulationCodes
import it.necst.marc.phase3.pre.data.support.SimulationDirection

/**
 * Created by andrea on 31/10/15.
 */

object ActorBDischarge{
  def props(simulationCodes: SimulationCodes, collectorBSimulations: ActorRef): Props = Props(new ActorBDischarge(simulationCodes, collectorBSimulations))
}

class ActorBDischarge(simulationCodes: SimulationCodes, collectorBSimulations: ActorRef) extends BSimulator(simulationCodes, collectorBSimulations, SimulationDirection.DISCHARGE){
  override protected val direction:String = "DISCHARGE"
}
