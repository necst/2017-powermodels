package it.necst.marc.infrastructure.actors

import java.util.concurrent.atomic.AtomicLong

import akka.actor.{Actor, ActorRef}


/**
 * Created by andrea on 02/06/15.
 */
object RedisLoadBalancer {}

abstract class RedisLoadBalancer(redisClientActors: List[ActorRef]) extends Actor{

  val next = new AtomicLong(0)

  def receive: Receive


  protected def chooseOneActor(): ActorRef = {
    redisClientActors((next.getAndIncrement % redisClientActors.size).asInstanceOf[Int])
  }



}
