package it.necst.marc.data.support

import java.io.{PrintWriter, StringWriter}

/**
  * Created by andrea on 15/11/15.
  */
object ErrorStack {

  private val logger = MarcLogger(getClass.getName)

  def apply(e:Throwable):String = {
    val sw = new StringWriter()
    e.printStackTrace(new PrintWriter(sw))
    logger.warn(sw.toString)
    sw.toString
  }

}
