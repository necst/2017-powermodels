package it.necst.marc.infrastructure.support

import it.necst.marc.data.support.ExtractableEnumeration

/**
  * Created by andrea on 25/01/16.
  */
object ConstantsKeys extends ExtractableEnumeration{

  val REDIS_OUT_IP    = Value(CURRENT_IP)
  val REDIS_OUT_PORT  = Value(CURRENT_PORT)

}
