package it.necst.marc.infrastructure.support

import net.jpountz.lz4.LZ4Factory

/**
 * Created by andrea on 16/09/15.
 */
object DataCompressor {

  private val compressor = LZ4Factory.fastestInstance().highCompressor()
  private val decompressor = LZ4Factory.fastestInstance().fastDecompressor()

  def compress(data: Array[Byte]):Array[Byte] = {
    compressor.compress(data)
  }

  def compress(data: String): Array[Byte] = {
    compress(data.getBytes("UTF-8"))
  }

  def decompress(data: Array[Byte], originalDim: Int): Array[Byte] = {
    decompressor.decompress(data,originalDim)
  }



}
