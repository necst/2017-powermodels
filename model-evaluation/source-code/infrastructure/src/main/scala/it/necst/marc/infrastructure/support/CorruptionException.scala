package it.necst.marc.infrastructure.support

/**
  * Created by andreadamiani on 10/12/15.
  */
class CorruptionException(message:String) extends RuntimeException(message)
