package it.necst.marc.data.support

import java.net.URI

/**
 * Created by andreadamiani on 03/07/15.
 */
object URIHelper {
  def isValid(str:String) = try{
      val uri = URI.create(str)
      if(uri.getScheme.toLowerCase == "marc") true else false
    } catch {
      case nullEx:NullPointerException => false
      case illegalEx:IllegalArgumentException => false
    }
}