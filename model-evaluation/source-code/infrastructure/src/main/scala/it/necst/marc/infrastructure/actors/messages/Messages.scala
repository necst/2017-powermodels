package it.necst.marc.infrastructure.actors.messages

import java.net.URI

import akka.actor.ActorRef


sealed trait Message

//LoadBalancer messages
@SerialVersionUID(1234l) case class SendJobCFG(job: String, configuration: String,dataSet:String,replyTo: ActorRef) extends Message
@SerialVersionUID(1234l) case class SendReqComputation(job: String,computationUnit: String, phaseTag: Option[String],replyTo: ActorRef) extends Message
@SerialVersionUID(1234l) case class ComputationComplete(job: String,computationUnit: String) extends Message
@SerialVersionUID(1234l) case class DeallocateActorFromConfig(actorToDeallocate: ActorRef) extends Message
@SerialVersionUID(1234l) case class ErrorMsgNoWorkerFree() extends Message
@SerialVersionUID(1234l) case class ErrorMsgMissingCfg() extends Message
@SerialVersionUID(1234l) case class JobCFGReceived() extends Message

//Communication actor messages
@SerialVersionUID(1234l) case class GoToPreambleState() extends Message
@SerialVersionUID(1234l) case class ErrorWorkerMsg(error: String) extends Message
@SerialVersionUID(1234l) case class ResultReady(resultUri:URI) extends Message
@SerialVersionUID(1234l) case class StartComputation() extends Message
@SerialVersionUID(1234l) case class DataNotFound() extends Message
@SerialVersionUID(1234l) case class DataFound(uri:URI) extends Message
@SerialVersionUID(1234l) case class StartFetchData(resultURI:URI) extends Message
@SerialVersionUID(1234l) case class StartInternalComputation() extends Message
@SerialVersionUID(1234l) case class InternalComputationCompleted(report: Array[Byte],reportDim: Int,result: Array[Byte],resultDim: Int) extends Message
@SerialVersionUID(1234l) case class ComputationError(report: String,error:String) extends Message
@SerialVersionUID(1234l) case class InternalComputationCompletedMultipleResults(mapResults: Map[String,(Array[Byte],Int)],report: Array[Byte],reportDim: Int) extends Message

//redis messages
@SerialVersionUID(1234l) case class KeyReportInserted(uri:URI) extends Message
@SerialVersionUID(1234l) case class KeyResultInserted(uri:URI,phaseTag: Option[String]) extends Message
@SerialVersionUID(1234l) case class DataReady(uri:URI,data: Array[Byte], originalDim: Int) extends Message
@SerialVersionUID(1234l) case class DataNotPresent(uri:URI) extends Message
@SerialVersionUID(1234l) case class KeysInserted(uriReport:URI,uriResult:URI) extends Message
@SerialVersionUID(1234l) case class ReadFromURI(uri:URI,replyTo: ActorRef) extends Message
@SerialVersionUID(1234l) case class ErrorOperationUnknown() extends Message
@SerialVersionUID(1234l) case class CollectAll(uri: URI,replyTo: ActorRef,phaseResult: String) extends Message
@SerialVersionUID(1234l) case class FinalUriGenerated(uri: URI,result: Option[Array[Byte]],dim: Option[Int]) extends Message

//redis load balancer internal
@SerialVersionUID(1234l) case class WriteReport(job:String,computationUnit: String, phaseName: String,dataSet: String,
                                                report: Array[Byte], originalDimension: Int, replyTo: ActorRef) extends Message

@SerialVersionUID(1234l) case class WriteResult(job:String,computationUnit: String, phaseName: String,dataSet: String,
                                                result: Array[Byte], originalDimension: Int, replyTo: ActorRef,
                                                optionalTag: Option[String] = None) extends Message

@SerialVersionUID(1234l) case class WriteDataFromExternalRedis(job:String, phaseName: String,dataSet: String,
                                                result: Array[Byte], originalDimension: Int, replyTo: ActorRef,
                                                optionalTag: Option[String] = None) extends Message

@SerialVersionUID(1234l) case class AreResultAlreadyComputedURI(uri:URI,replyTo: ActorRef) extends Message

@SerialVersionUID(1234l) case class AreResultAlreadyComputedConfig(job:String,computationUnit: String, phaseName: String,dataset: String,optionalTags: Option[String],replyTo: ActorRef) extends Message

@SerialVersionUID(1234l) case class WriteReportAndResult(job:String, computationUnit: String,
                                                         phaseName: String, dataSet: String,report: Array[Byte],
                                                         reportOriginalDim: Int, result: Array[Byte],
                                                         resultOriginalDim: Int, replyTo: ActorRef,
                                                         optionalTag: Option[String] = None) extends Message
@SerialVersionUID(1234l) case class GiveMeDictionary(job:String,computationUnit: String,dataSet: String,replyTo: ActorRef) extends Message
@SerialVersionUID(1234l) case class TakeDictionary(data: Array[Byte], dim: Int) extends Message
@SerialVersionUID(1234l) case class DictionaryNotFound() extends Message
@SerialVersionUID(1234l) case class UriReportAfterError(uriString: String) extends Message
@SerialVersionUID(1234l) case class CollectReportError(phase: String,jobString: String,dataset: String,computationUnit: String,replyTo: ActorRef) extends Message
@SerialVersionUID(1234l) case class GiveMeAllResults(uri: String,replyTo: ActorRef) extends Message
@SerialVersionUID(1234l) case class TakeAllResults(results: List[(String,(Array[Byte],Int))]) extends Message