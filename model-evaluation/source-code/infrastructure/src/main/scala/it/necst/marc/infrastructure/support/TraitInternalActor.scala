package it.necst.marc.infrastructure.support

import java.net.URI

import akka.actor.{ActorRef, Props}
import com.typesafe.config.Config


object TraitInternalActor{}

/**
 * Created by andrea on 04/06/15.
 */
trait TraitInternalActor{

  def props(currentConfiguration: String,
            actorToReplyRef: ActorRef,
            mapDataString:Map[URI,(Array[Byte],Int)],
            configFile: Config):Props


}
