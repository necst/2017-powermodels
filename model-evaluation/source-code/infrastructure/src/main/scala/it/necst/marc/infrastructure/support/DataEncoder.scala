package it.necst.marc.infrastructure.support

import java.util.Base64

/**
  * Created by andrea on 02/01/16.
  */
object DataEncoder {

  def encode(data: Array[Byte]): String = Base64.getEncoder.encodeToString(data)

  def decode(data: String): Array[Byte] = Base64.getDecoder.decode(data)

  def byteLength(data: String) = data.getBytes("UTF-8").length

}
