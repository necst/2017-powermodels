package it.necst.marc.data.support

import org.slf4j.LoggerFactory
/**
  * Created by andrea on 14/12/15.
  */
object MarcLogger {

  def apply(className: String) = LoggerFactory.getLogger(className)

}
