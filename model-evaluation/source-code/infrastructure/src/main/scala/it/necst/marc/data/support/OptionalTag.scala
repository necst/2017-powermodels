package it.necst.marc.data.support

/**
 * Created by andrea on 29/10/15.
 */
object OptionalTag extends ExtractableEnumeration {
  val A = Value("A")
  val B = Value("B")
  val C = Value("C")
  val PRE = Value("3PRE")
  val DICT = Value("DICT")
}
