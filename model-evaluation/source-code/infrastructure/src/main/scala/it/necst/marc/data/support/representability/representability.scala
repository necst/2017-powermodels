package it.necst.marc.data.support

/**
 * Created by Andrea on 12/08/2015.
 */
package object representability {
  class Representation[T](val obj:T, override val toString:String){
    def internalToString = obj.toString
  }
  object Representation{
    def apply[T](obj:T, representation:String) = new Representation(obj, representation)
  }

  implicit class Representable[T](val obj:T){
    def @:(representation:String) = Representation(obj, representation)
  }

  implicit def unwrapRepresentation[T](representation:Representation[T]):T = representation.obj
  
  type R[T] = Representation[T]
}
