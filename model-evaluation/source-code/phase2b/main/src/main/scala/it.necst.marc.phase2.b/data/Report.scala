package it.necst.marc.phase2.b.data

import it.necst.marc.data.{AbstractReport}

/**
 * Created by andreadamiani on 17/06/15.
 */

case class Report(content:Option[MathStats], error:Option[String]) extends AbstractReport(content, error)

case class MathStats() extends it.necst.marc.data.MathStats
