package it.necst.marc.phase2.b.data

import it.necst.marc.data.Binding
import it.necst.marc.phase1.post.computation.SubphaseBatchIdentification.Code

case class Result(transition_from:List[TransitionRow], state_dictionary:List[State]){
  private val rowStates = transition_from.map(_.stateID)
  require(rowStates.size == rowStates.toSet.size, "Each state may appear in one row at most.")
  require((rowStates ::: transition_from.flatMap(x => x.to.map(_.stateID))).toSet[Int].map(s => state_dictionary.exists(_.stateID == s)).reduce(_ && _), "Each stateID must be defined in state_dictionary.")
}

case class TransitionRow(stateID:Int, to:List[TransitionCell]){
  private val colStates = to.map(_.stateID)
  require(colStates.size == colStates.toSet.size, "Each state may appear in one column at most.")
}

case class TransitionCell(stateID:Int, probability:Double){
  require(probability>=0 && probability<=1, "probability value must be in [0,1].")
}

case class State(stateID:Int, code:Code)
