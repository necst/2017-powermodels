package it.necst.marc.phase2.b.actors

import akka.actor.{ActorSystem, Actor, ActorRef, Props}
import com.typesafe.config.Config
import it.necst.marc.data.support.{ErrorStack, MarcLogger}
import it.necst.marc.infrastructure.actors.messages.{ComputationError, StartInternalComputation}
import it.necst.marc.infrastructure.support.{DataCompressor, TraitInternalActor}
import it.necst.marc.phase2.b.data.Report

import rapture.json.Json
import rapture.json.jsonBackends.jawn._
import java.net.URI


/**
 * Created by andrea on 01/06/15.
 */
object InternalWorker extends TraitInternalActor{

  def  props(currentConfiguration:String,actorToReply: ActorRef,
             mapData: Map[URI,(Array[Byte],Int)], configFile: Config): Props = Props(new InternalWorker(currentConfiguration,actorToReply,mapData,configFile))


}


class InternalWorker(val currentConfiguration: String,
                     val actorToReply: ActorRef,
                     val mapData:Map[URI,(Array[Byte],Int)],
                     val configFile: Config) extends Actor{


  private val system = ActorSystem("InternalWorker")

  private val logger = MarcLogger(getClass.getName)

  def receive = startInternalComputationState

  def startInternalComputationState: Receive = {
    case StartInternalComputation() =>
      logger.info("Received StartInternalComputation message")
      try{
        val dataString = new String(DataCompressor.decompress(mapData.head._2._1,mapData.head._2._2))
        val dataForPhase = Json.parse(dataString).as[it.necst.marc.phase1.post.data.Result]
        val configuration = Json.parse(currentConfiguration).as[it.necst.marc.phase2.b.data.Configuration]


      } catch {
        case e: Throwable =>
          val error = ErrorStack(e)
          val report = Json(Report(None,Some(error))).toString()
          actorToReply ! ComputationError(report,error)
          system.terminate()
          context.stop(self)
      }




  }



}
