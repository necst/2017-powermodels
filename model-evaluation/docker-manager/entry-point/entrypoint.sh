#!/bin/bash
set -e
java -XX:+UseG1GC -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000  -agentpath:/opt/jprofiler/bin/linux-x64/libjprofilerti.so=port=9000,nowait -Dorg.slf4j.simpleLogger.defaultLogLevel=debug -cp /home/phase.jar $MAIN_CLASS
