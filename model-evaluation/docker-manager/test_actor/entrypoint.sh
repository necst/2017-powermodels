#!/bin/bash
set -e
#while ! nc -z redis_internal.marc.docker 6379; do sleep 3; done
#while ! nc -z redis_external.marc.docker 6379; do sleep 3; done
echo "infrastructure"
while ! nc -z infrastructure.marc.docker 4001; do sleep 3; done
echo "phase0.marc.docker"
while ! nc -z phase0.marc.docker 5554; do sleep 3; done
echo "phase1.marc.docker"
while ! nc -z phase1.marc.docker 5555; do sleep 3; done
echo "phase1post.marc.docker"
while ! nc -z phase1post.marc.docker 5556; do sleep 3; done
echo "phase2a.marc.docker"
while ! nc -z phase2a.marc.docker 5557; do sleep 3; done
#TODO add follow line after having implemented phase2b
#while ! nc -z phase2b.marc.docker 5558; do sleep 3; done
#echo "phase2c.marc.docker"
#while ! nc -z phase2c.marc.docker 5559; do sleep 3; done
#echo "phase3pre.marc.docker"
#while ! nc -z phase3pre.marc.docker 5560; do sleep 10; done
#echo "phase3.marc.docker"
#while ! nc -z phase3.marc.docker 5561; do sleep 3; done
echo "Starting test..."
java -XX:+UseG1GC -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000  -agentpath:/opt/jprofiler/bin/linux-x64/libjprofilerti.so=port=9000,nowait -Dorg.slf4j.simpleLogger.defaultLogLevel=debug -cp /home/phase.jar $MAIN_CLASS
