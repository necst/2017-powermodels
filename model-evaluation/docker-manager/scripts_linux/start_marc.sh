#!/bin/bash

DOCKER_DIR="DOCKER_MANAGER_FOLDER"
cd $DOCKER_DIR
docker rm -f $(docker ps -a -q)
./scripts_linux/copy.sh
docker-compose build
./scripts_linux/start_dns.sh
docker-compose up redisinternal redisexternal infrastructure &
sleep 20
docker-compose up phase0 phase1 phase1post &
sleep 20
docker-compose up phase2a phase2b phase2c &
sleep 20
docker-compose up phase3pre entrypoint &
sleep 10
docker-compose up webapp
