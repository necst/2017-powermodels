function [array_without_overflow] = manage_overflow_rapl(source,energy_unit)
    % Copy first column
    array_without_overflow(:,1)=source(:,1);
    % Initialize first value
    array_no_overflow(1)=source(1,2);
    
    for i=2:length(source(:,2))
        counter = array_no_overflow(i-1);
        if source(i,2)>=source(i-1,2)
            array_no_overflow(i)=source(i,2)-source(i-1,2)+counter;
        else
            array_no_overflow(i)= counter
        end
        % XENTRACE BUG - sometimes TSC are related with incorrect data
        % TSC1 -> VALUE2
        % TSC2 -> VALUE1
        % FIXED WITH VALUE SWITCH
        if (array_no_overflow(i)-array_no_overflow(i-1))>0.6*10^5/energy_unit
            disp('Found data corruption at TCS:');
            array_no_overflow(i)=source(i-1,2)-source(i,2)+counter;
        end
    end
    array_without_overflow(:,2)=array_no_overflow(:);

end