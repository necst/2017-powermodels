clc
clear

% Constant variables
energy_unit = 1/(2^16);                 % RAPL specification for Intel i7-2600
cpu_frequency = 2.8*10^9;               % 3.4GHz for Intel i7-2600
resample_delta = 1;                     % Granularity of the resampling (seconds)
pmc_to_plot = [1 2 3 4];                % PMCs to plot on the graphs
pmc_to_consider_first = [1 2 3 4];      % PMCs to analize from pmc.csv
base_wattsup_power = 30;               % Wattsup power idle state
ctr_to_consider = [1 2 3];              % Fixed ctr to be analized
ctr_to_plot = [1 2 3];                  % Fixed ctr to be plotted
idle_domain = 32767;                    % id of the idle domain
contribution_matrix_filter = 2;         % filter PMCs noise while plotting contributions
number_core_per_socket = 4;            % number of cores for each socket in the machine
number_of_socket = 1;

% Import data -------------------------------------------------------------
disp('- Import data');
rapl_struct = importdata('rapl.csv');
rapl_raw = rapl_struct.data;
pmc_struct = importdata('pmc.csv');
pmc_raw = pmc_struct.data;
wattsup_raw = importdata('wattsup-watts');
ctr_struct = importdata('ctr.csv');
ctr_raw = ctr_struct.data;
freq_struct = importdata('freq.csv');
freq_raw = freq_struct.data;
domain_mapping_struct = importdata('domains-mapping');
domain_mapping_raw = domain_mapping_struct.data;

% Get pmc names from pmc.csv
label_pmc = pmc_struct.colheaders(5:8);

% Order data considering the TSC column
rapl_raw = sortrows(rapl_raw,1);
pmc_raw = sortrows(pmc_raw,1);
ctr_raw = sortrows(ctr_raw,1);
freq_raw = sortrows(freq_raw,1);


% Preprocessing -----------------------------------------------------------
disp('- Preprocessing');
% zero-tsc values
zero_tsc_rapl_bitmask = rapl_raw(:,1)==0;              % bitmask: valid TSC values
rapl_raw(zero_tsc_rapl_bitmask,:)=[];                  % matrix filtered
zero_tsc_pmc_bitmask = pmc_raw(:,1)==0;                % bitmask: valid TSC values
pmc_raw(zero_tsc_pmc_bitmask,:)=[];                    % matrix filtered
zero_tsc_ctr_bitmask = ctr_raw(:,1)==0;                % bitmask: valid TSC values
ctr_raw(zero_tsc_ctr_bitmask,:)=[];
zero_tsc_freq_bitmask = freq_raw(:,1)==0;              % bitmask: valid TSC values
freq_raw(zero_tsc_freq_bitmask,:)=[];


map_rapl = containers.Map('KeyType','int32', 'ValueType','int32');

unique_domains_ids = unique(rapl_raw(:,3))';
for domain_id = unique_domains_ids
    domain_bitmask = rapl_raw(:,3) == domain_id;
    filtered = rapl_raw(domain_bitmask,2);
    cpu_id = filtered(1,1);
    map_rapl(domain_id) = cpu_id;
end


zero_tsc_rapl_bitmask = rapl_raw(:,2)==0;              % bitmask: filter domain 0 data
rapl_raw(zero_tsc_rapl_bitmask,:)=[];                  % matrix filtered

% probe domains now are pinned on 9 and 19 core 
filter_socket_1_bitmask = rapl_raw(:,2)~=3;            % bitmask: filter all different from 9

rapl_raw(filter_socket_1_bitmask,:)=[];                 % matrix filtered

zero_tsc_rapl_bitmask = rapl_raw(:,3)==idle_domain;    % bitmask: valid TSC values
rapl_raw(zero_tsc_rapl_bitmask,:)=[]; 

% Compute real frequency on each context switch
for i=1:length(freq_raw)
   freq_data(i)=(freq_raw(i,5)/freq_raw(i,6))*cpu_frequency; 
end

% remove idle domain
unique_domain_ids = unique(pmc_raw(:,3))';
unique_domain_ids(unique_domain_ids == idle_domain) = [];
map_domain = containers.Map('KeyType','double', 'ValueType','int32');


% Convert counters to the right unit
rapl_raw(:,[5 6 7 8])=energy_unit*rapl_raw(:,[5 6 7 8]);
rapl_raw(:,1)=rapl_raw(:,1)/cpu_frequency; %TSC counter has a fixed clock
pmc_raw(:,1)=pmc_raw(:,1)/cpu_frequency;
ctr_raw(:,1)=ctr_raw(:,1)/cpu_frequency;

% Time incremental wrt the first value
tmp_result = min(rapl_raw(1,1), pmc_raw(1,1));
base_of_times = min(tmp_result,ctr_raw(1,1));
rapl_raw(:,1)=rapl_raw(:,1)-base_of_times;
pmc_raw(:,1)=pmc_raw(:,1)-base_of_times;
ctr_raw(:,1)=ctr_raw(:,1)-base_of_times;

%slipt rapl between sockets
for i=1:number_of_socket
    first_cpu = (i-1)*number_core_per_socket;
    last_cpu = i*number_core_per_socket-1;
    core_bitmask = rapl_raw(:,2)>= first_cpu & rapl_raw(:,2)<= last_cpu;
    rapl_per_socket(i).data = rapl_raw(core_bitmask,:);
end

%remove empty structure
empty_elems = arrayfun(@(s) all(structfun(@isempty,s)), rapl_per_socket);
rapl_per_socket(empty_elems) = [];
% managing rapl overflow
for i=1:length(rapl_per_socket)
    rapl_per_socket(i).data(:,[1 5]) = manage_overflow_rapl(rapl_per_socket(i).data(:,[1 5]),energy_unit);
    rapl_per_socket(i).data(:,[1 6]) = manage_overflow_rapl(rapl_per_socket(i).data(:,[1 6]),energy_unit);
    rapl_per_socket(i).data(:,[1 7]) = manage_overflow_rapl(rapl_per_socket(i).data(:,[1 7]),energy_unit);
    rapl_per_socket(i).data(:,[1 8]) = manage_overflow_rapl(rapl_per_socket(i).data(:,[1 8]),energy_unit);
end



% Grouping and conditioning -----------------------------------------------------------
disp('- Grouping and conditioning');
% Merge RAPL information gathered on all cores
for i=1:length(rapl_per_socket)
    rapl_per_socket(i).rapl_pkg_all = rapl_per_socket(i).data(:,[1 5]);
    rapl_per_socket(i).rapl_pkg_ts = timeseries(rapl_per_socket(i).rapl_pkg_all(:,2), rapl_per_socket(i).rapl_pkg_all(:,1), 'Name', strcat('rapl_pkg_socket_',int2str(i)));

    rapl_per_socket(i).rapl_pp0_all = rapl_per_socket(i).data(:,[1 6]);
    rapl_per_socket(i).rapl_pp0_ts = timeseries(rapl_per_socket(i).rapl_pp0_all(:,2), rapl_per_socket(i).rapl_pp0_all(:,1), 'Name', strcat('rapl_pp0_socket_',int2str(i)));

    rapl_per_socket(i).rapl_pp1_all = rapl_per_socket(i).data(:,[1 7]);
    rapl_per_socket(i).rapl_pp1_ts = timeseries(rapl_per_socket(i).rapl_pp1_all(:,2), rapl_per_socket(i).rapl_pp1_all(:,1), 'Name', strcat('rapl_pp1_socket_',int2str(i)));

    rapl_per_socket(i).rapl_dram_all = rapl_per_socket(i).data(:,[1 8]);
    rapl_per_socket(i).rapl_dram_ts = timeseries(rapl_per_socket(i).rapl_dram_all(:,2), rapl_per_socket(i).rapl_dram_all(:,1), 'Name', strcat('rapl_dram_socket_',int2str(i))); 
end

len_wattsup = length(wattsup_raw);
len_rapl = length(unique(floor(rapl_raw(:,1))));
if len_wattsup > len_rapl
    difference = len_wattsup - len_rapl;
    wattsup_raw = wattsup_raw(difference:end);
end

tests_length = min(length(wattsup_raw),length(unique(floor(rapl_raw(:,1)))));

% Resample all the timeseries
for i=1:length(rapl_per_socket)
    rapl_per_socket(i).rapl_pkg_ts_resample=resample(rapl_per_socket(i).rapl_pkg_ts,1:resample_delta:tests_length);
    rapl_per_socket(i).rapl_pp0_ts_resample=resample(rapl_per_socket(i).rapl_pp0_ts,1:resample_delta:tests_length);
    rapl_per_socket(i).rapl_pp1_ts_resample=resample(rapl_per_socket(i).rapl_pp1_ts,1:resample_delta:tests_length);
    rapl_per_socket(i).rapl_dram_ts_resample=resample(rapl_per_socket(i).rapl_dram_ts,1:resample_delta:tests_length);
end

% Uniform Wattsup measurements
wattsup_ts = timeseries(wattsup_raw,1:length(wattsup_raw),'Name','wattsup');
wattsup_ts = setinterpmethod(wattsup_ts,'zoh');
wattsup_ts_resample=resample(wattsup_ts, 1:resample_delta:tests_length);
wattsup_ts_resample_no_empty_data = manage_lost_data_wattsup(wattsup_ts_resample.data,wattsup_ts_resample.time,base_wattsup_power);

wattsup_energy_data(1) = wattsup_ts_resample_no_empty_data.data(1);
wattsup_energy_time(:) = wattsup_ts_resample_no_empty_data.time(:);
for i=2:length(wattsup_ts_resample_no_empty_data.data)
   wattsup_energy_data(i) = wattsup_ts_resample_no_empty_data.data(i)*(wattsup_ts_resample_no_empty_data.time(i)-wattsup_ts_resample_no_empty_data.time(i-1)) + wattsup_energy_data(i-1);
end
% Estimate power on the RAPL_PKG counter and resample

for i=1:length(rapl_per_socket)
    dt = diff(rapl_per_socket(i).rapl_pkg_ts_resample.time); % differential time
    
    dE_pkg = diff(rapl_per_socket(i).rapl_pkg_ts_resample.data); % differential data
    power_pkg = dE_pkg./dt;
    power_per_socket(i).power_pkg_ts_resample = timeseries([power_pkg' power_pkg(end)]', rapl_per_socket(i).rapl_pkg_ts_resample.time, 'Name',strcat('power_pkg_socket',int2str(i)));

    dE_pp0 = diff(rapl_per_socket(i).rapl_pp0_ts_resample.data); % differential data
    power_pp0 = dE_pp0./dt; 
    power_per_socket(i).power_pp0_ts_resample = timeseries([power_pp0' power_pp0(end)]', rapl_per_socket(i).rapl_pp0_ts_resample.time, 'Name',strcat('power_pp0_socket',int2str(i)));

    dE_pp1 = diff(rapl_per_socket(i).rapl_pp1_ts_resample.data); % differential data
    power_pp1 = dE_pp1./dt;
    power_per_socket(i).power_pp1_ts_resample = timeseries([power_pp1' power_pp1(end)]', rapl_per_socket(i).rapl_pp1_ts_resample.time, 'Name',strcat('power_pp1_socket',int2str(i)));

    dE_dram = diff(rapl_per_socket(i).rapl_dram_ts_resample.data); % differential data
    power_dram = dE_dram./dt;
    power_per_socket(i).power_dram_ts_resample = timeseries([power_dram' power_dram(end)]', rapl_per_socket(i).rapl_dram_ts_resample.time, 'Name',strcat('power_dram_socket',int2str(i)));
   
end

% Per-core information filtering ------------------------------------------
% Split measures per unique cores
disp('- Split measures per unique cores');
unique_core_ids = unique(rapl_raw(:,2))';    
i = 1;
for core_id = unique_core_ids
    core_bitmask = rapl_raw(:,2)== core_id;   % bitmask: core_id data
    counter_core(i).id = core_id;             % Counter wrt the first
    counter_core(i).pkg = manage_overflow(rapl_raw(core_bitmask,[1 5]));  
    counter_core(i).pp0 = manage_overflow(rapl_raw(core_bitmask,[1 6]));
    counter_core(i).pp1 = manage_overflow(rapl_raw(core_bitmask,[1 7]));
    counter_core(i).dram = manage_overflow(rapl_raw(core_bitmask,[1 8]));

    i = i+1;
end


% Cumulate counters for all domain ----------------------------------------
disp('- Cumulate counters for all domain');
all_domain_bitmask = pmc_raw(:,3) ~= idle_domain; % bitmask: domain_id data
last_column = 5;

for pmc_index = pmc_to_consider_first
    global_pmc(pmc_index).raw =pmc_raw(all_domain_bitmask,[1 last_column]);
    [global_pmc(pmc_index).pmc_ts, global_pmc(pmc_index).pmc_cumulated_ts] = cumulate_and_resample(global_pmc(pmc_index).raw(:,1), global_pmc(pmc_index).raw(:,2), 1, resample_delta, tests_length);
    last_column = last_column + 1;
end

last_column = 5;
for ctr_index = ctr_to_consider
    global_ctr(ctr_index).raw =ctr_raw(all_domain_bitmask,[1 last_column]);
    [global_ctr(ctr_index).ctr_ts, global_ctr(ctr_index).ctr_cumulated_ts] = cumulate_and_resample(global_ctr(ctr_index).raw(:,1), global_ctr(ctr_index).raw(:,2), 1, resample_delta, tests_length);
    last_column = last_column + 1;

end


% Per-domain information filtering ----------------------------------------
% Split measures per unique domain
disp('- Split measures per unique domain');
i = 1;
for domain_id = unique_domain_ids
    
    domain_bitmask = pmc_raw(:,3)== domain_id;   % bitmask: domain_id data
    map_domain(domain_id) = i;
    last_column = 5;
    counter_domain(i).id = domain_id;           
    for pmc_index = pmc_to_consider_first
        counter_domain(i).pmc(pmc_index).raw = pmc_raw(domain_bitmask,[1 last_column]);
        [counter_domain(i).pmc(pmc_index).pmc_ts, counter_domain(i).pmc(pmc_index).pmc_cumulated_ts] = cumulate_and_resample(counter_domain(i).pmc(pmc_index).raw(:,1), counter_domain(i).pmc(pmc_index).raw(:,2), 1, resample_delta, tests_length);
        counter_domain(i).pmc(pmc_index).pmc_percent_ts = counter_domain(i).pmc(pmc_index).pmc_ts./global_pmc(pmc_index).pmc_ts;
       % counter_domain(i).power_pkg_ts = counter_domain(i).pmc(pmc_index).pmc_percent_ts.*power_pkg_ts_resample;
        last_column = last_column + 1;
    end
    
    last_column = 5;
    for ctr_index = ctr_to_consider
        counter_domain(i).ctr(ctr_index).raw = ctr_raw(domain_bitmask,[1 last_column]);
        [counter_domain(i).ctr(ctr_index).ctr_ts, counter_domain(i).ctr(ctr_index).ctr_cumulated_ts] = cumulate_and_resample(counter_domain(i).ctr(ctr_index).raw(:,1), counter_domain(i).ctr(ctr_index).raw(:,2), 1, resample_delta, tests_length);
        counter_domain(i).ctr(ctr_index).ctr_percent_ts = counter_domain(i).ctr(ctr_index).ctr_ts./global_ctr(ctr_index).ctr_ts;
        last_column = last_column + 1;
    end
    
    
    i = i+1;
end

%File writing

for i=1:length(rapl_per_socket)
    name_file = strcat('rapl_socket_',int2str(i),'.csv');
    fileID = fopen(name_file,'w+');
    fprintf(fileID,'time,wattsup,rapl_pkg,rapl_ppo,rapl_pp1,rapl_dram\n');
    for j=1:length(rapl_per_socket(i).rapl_pkg_ts_resample.time)
        fprintf(fileID,'%g,%g,%g,%g,%g,%g\n',wattsup_ts_resample_no_empty_data.time(j),rapl_per_socket(i).rapl_pkg_ts_resample.time(j),rapl_per_socket(i).rapl_pkg_ts_resample.data(j),rapl_per_socket(i).rapl_pp0_ts_resample.data(j),rapl_per_socket(i).rapl_pp1_ts_resample.data(j),rapl_per_socket(i).rapl_dram_ts_resample.data(j));
    end
    fclose(fileID);
end

%Write file for each domain
for j=1:length(counter_domain)
    name_file = strcat(int2str(counter_domain(j).id),'.csv');
    fileID = fopen(name_file,'w+');
    fprintf(fileID,'time,dom,');
    for pmc_index = pmc_to_consider_first
        name_hpc = strcat(char(label_pmc(pmc_index)),',');
        fprintf(fileID,name_hpc);
    end
    for ctr_index = ctr_to_consider
        name_ctr = strcat('ctr',int2str(ctr_index),',');
        fprintf(fileID,name_ctr);
    end
    fprintf(fileID,'wattsup_power,wattsup_energy,');
    fprintf(fileID,'rapl_pkg,rapl_pp0,rapl_pp1,rapl_dram\n');
    for i=1:tests_length
        fprintf(fileID,'%g,%g,',wattsup_ts_resample_no_empty_data.time(i),counter_domain(j).id);    
        for pmc_index = pmc_to_consider_first
           fprintf(fileID,'%g,',counter_domain(j).pmc(pmc_index).pmc_ts.data(i));
        end
        for ctr_index = ctr_to_consider
           fprintf(fileID,'%g,',counter_domain(j).ctr(ctr_index).ctr_ts.data(i));
        end
        fprintf(fileID,'%g,',wattsup_ts_resample_no_empty_data.data(i));
        fprintf(fileID,'%g,',wattsup_energy_data(i));
        
        go = true;
        cpuId = map_rapl(counter_domain(j).id);
        index_rapl = 1;
        while go
            diff = number_core_per_socket - cpuId;
            if diff <= number_core_per_socket
                go = false;
            else
                index_rapl = index_rapl + 1;
            end
        end
        fprintf(fileID,'%g,%g,%g,%g\n',rapl_per_socket(index_rapl).rapl_pkg_ts_resample.data(i),rapl_per_socket(index_rapl).rapl_pp0_ts_resample.data(i),rapl_per_socket(index_rapl).rapl_pp1_ts_resample.data(i),rapl_per_socket(index_rapl).rapl_dram_ts_resample.data(i));
       
        
    end
    fclose(fileID);
end



fileID = fopen('all_domains.csv','w+');
fprintf(fileID,'time,');
for domain_id = unique_domain_ids
    %TODO add mapping domain name
    name_dom = strcat('dom',int2str(domain_id),',');
    fprintf(fileID,name_dom);
end
for domain_id = unique_domain_ids
    for pmc_index = pmc_to_consider_first
        name_hpc = strcat('dom',int2str(domain_id),'_hpc',int2str(pmc_index),',');
        fprintf(fileID,name_hpc);
    end
    for ctr_index = ctr_to_consider
        name_ctr = strcat('dom',int2str(domain_id),'_ctr',int2str(ctr_index),',');
        fprintf(fileID,name_ctr);
    end
end

for pmc_index = pmc_to_consider_first
    name_pmc_global = strcat('global_pmc',int2str(pmc_index),',');
    fprintf(fileID,name_pmc_global);
end
for ctr_index = ctr_to_consider
    name_ctr_global = strcat('global_ctr',int2str(ctr_index),',');
    fprintf(fileID,name_ctr_global);
end
fprintf(fileID,'wattsup_power\n');

for i=1:tests_length
    fprintf(fileID,'%g,',wattsup_ts_resample_no_empty_data.time(i));
    
    for domain_id = unique_domain_ids
        domain_index = map_domain(domain_id);
        domain_running = check_domain_running(counter_domain(domain_index).pmc,i);
        if(domain_running)
            fprintf(fileID,'1,');
        else
            fprintf(fileID,'0,');
        end
    end
    
    for domain_id = unique_domain_ids
        domain_index = map_domain(domain_id);
       for pmc_index = pmc_to_consider_first
           fprintf(fileID,'%g,',counter_domain(domain_index).pmc(pmc_index).pmc_ts.data(i));
       end 
       for ctr_index = ctr_to_consider
           fprintf(fileID,'%g,',counter_domain(domain_index).ctr(ctr_index).ctr_ts.data(i));
       end 
    end
    for pmc_index = pmc_to_consider_first
        fprintf(fileID,'%g,',global_pmc(pmc_index).pmc_cumulated_ts.data(i));
    end
    for ctr_index = ctr_to_consider
        fprintf(fileID,'%g,',global_ctr(ctr_index).ctr_cumulated_ts.data(i));
    end
  
  
    fprintf(fileID,'%g\n',wattsup_ts_resample_no_empty_data.data(i));
    
end


% Plot Package Energy and Power, measured with RAPL and with the Watts Up Power meter
disp('- Plot Package Energy and Power, measured with RAPL and with the Watts Up Power meter');
for i=1:length(rapl_per_socket)
    fig = figure;
    orient landscape
    hold on;
    [hAx,hLine1,hLine2] = plotyy(rapl_per_socket(i).rapl_pkg_ts_resample.time, ...
                                    rapl_per_socket(i).rapl_pkg_ts_resample.data,  ...
                                    [power_per_socket(i).power_pkg_ts_resample.time, wattsup_ts_resample_no_empty_data.time,power_per_socket(i).power_pp0_ts_resample.time], ...
                                    [power_per_socket(i).power_pkg_ts_resample.data, wattsup_ts_resample_no_empty_data.data,power_per_socket(i).power_pp0_ts_resample.data]);
    xlabel('Time (s)');
    ylabel(hAx(1),'Energy (J)');    % left y-axis
    ylabel(hAx(2),'Power (W)');     % right y-axis
    leg = legend('Package Energy (RAPL)','Package Power (RAPL)','Workstation Power (external)');
    set(leg,'location','northoutside','orientation','horizontal');
    grid on;
    grid minor;
    hold off;
    print(fig,strcat('rapl_socket_',int2str(i)),'-dpng')
end

fig = figure;
orient landscape
hold on;
X_Energy = [];
Y_Energy = [];
X_Power = [];
Y_Power = [];
for z=1:length(rapl_per_socket)
    X_Energy = [X_Energy rapl_per_socket(z).rapl_pkg_ts_resample.time'];
    Y_Energy = [Y_Energy rapl_per_socket(z).rapl_pkg_ts_resample.data'];
    X_Power = [X_Power  power_per_socket(z).power_pkg_ts_resample.time'];
    Y_Power = [Y_Power  power_per_socket(z).power_pkg_ts_resample.data'];
end
[hAx,hLine1,hLine2] = plotyy(X_Energy,Y_Energy,X_Power,Y_Power);
                                
ylabel(hAx(1),'Energy (J)');    % left y-axis
ylabel(hAx(2),'Power (W)');     % right y-axis
leg = legend('Package Energy (RAPL)','Package Power (RAPL)','Workstation Power (external)');
set(leg,'location','northoutside','orientation','horizontal');
grid on;
grid minor;
hold off;
print(fig,'rapl_compare.png','-dpng');


% Plot Package Energy and Power (RAPL), with PMCi for every domain
disp('- Plot Package Energy and Power (RAPL), with PMCi for every domain');
i = 0;
for domain_id = unique_domain_ids
    i = i+1;
    j=1;
    max_to_plot = 0;
    
    for current_pmc = pmc_to_plot
        fig = figure;
        orient landscape
        total_plots = 2;
        subplot(total_plots,1,1);
        X_Energy = [];
        Y_Energy = [];
        X_Power = [];
        Y_Power = [];
        for z=1:length(rapl_per_socket)
            X_Energy = [X_Energy rapl_per_socket(z).rapl_pkg_ts_resample.time'];
            Y_Energy = [Y_Energy rapl_per_socket(z).rapl_pkg_ts_resample.data'];
            X_Power = [X_Power  power_per_socket(z).power_pkg_ts_resample.time'];
            Y_Power = [Y_Power  power_per_socket(z).power_pkg_ts_resample.data'];
        end
        
        plot_energy_and_power(X_Energy,Y_Energy,X_Power, Y_Power);
        hold on;
        y_limits = ylim;
        %plot_overlay(y_limits(2));
        hold off;
        subplot(total_plots,1,2);
        hold on;
        plot(counter_domain(i).pmc(current_pmc).pmc_ts.time, counter_domain(i).pmc(current_pmc).pmc_ts.data, '-');
        if(max_to_plot < max(counter_domain(i).pmc(current_pmc).pmc_ts.data(126:end).'))
           
        end
        legend_index=j;
        legendInfo{1} = char(label_pmc(current_pmc));
        y_limits = ylim;
        %plot_range(y_limits(2));
        title(['PMCs of dom-' int2str(counter_domain(i).id)]);
        leg = legend(legendInfo);
        a = int2str(counter_domain(2).id);
        set(leg,'location','southoutside','Orientation','horizontal');
        xlabel('Time (s)');
        ylabel('PMC value');
        grid on;
        grid minor;
        hold off;
        print(fig,strcat('PMCs',int2str(counter_domain(i).id),'-',char(label_pmc(current_pmc))),'-dpng');
        j=j+1;
    end
end


i = 1;
for domain_id = unique_domain_ids
    
    j=1;
    for current_ctr = ctr_to_plot
        fig = figure;
        orient landscape
        total_plots = 2;

        subplot(total_plots,1,1);
        X_Energy = [];
        Y_Energy = [];
        X_Power = [];
        Y_Power = [];
        for z=1:length(rapl_per_socket)
            X_Energy = [X_Energy rapl_per_socket(z).rapl_pkg_ts_resample.time'];
            Y_Energy = [Y_Energy rapl_per_socket(z).rapl_pkg_ts_resample.data'];
            X_Power = [X_Power  power_per_socket(z).power_pkg_ts_resample.time'];
            Y_Power = [Y_Power  power_per_socket(z).power_pkg_ts_resample.data'];
        end
        
        plot_energy_and_power(X_Energy,Y_Energy,X_Power, Y_Power);
        y_limits = ylim;
        %plot_overlay(y_limits(2));
        subplot(total_plots,1,2);
        hold on;
        plot(counter_domain(i).ctr(current_ctr).ctr_ts.time, counter_domain(i).ctr(current_ctr).ctr_ts.data, '-');
        legend_index=j;
        legendInfo{1} = ['ctr' int2str(current_ctr)];
        title(['CTRs of dom-' int2str(counter_domain(i).id)]);
        leg = legend(legendInfo);
        set(leg,'location','southoutside','Orientation','horizontal');
        xlabel('Time (s)');
        ylabel('CTR value');
        grid on;
        grid minor;
        hold off;
        print(fig,strcat('CTRs',int2str(counter_domain(i).id),int2str(current_ctr)),'-dpng')
        j=j+1;
    end
    y_limits = ylim;
    %plot_range(y_limits(2));
    
    i = i+1;
    
end


% Distribution of Package Energy consumption per domain, for every PMCi
% disp('- Plot distribution of Package Energy consumption per domain, for every PMCi');
% 
% i = 1;
% for current_pmc = pmc_to_plot
%     
%     figure;
%     orient landscape
%     total_plots = 2;
% 
%     subplot(total_plots,1,1);
%     for i=1:length(rapl_per_socket)
%         plot_energy_and_power(rapl_per_socket(i).rapl_pkg_ts_resample.time, rapl_per_socket(i).rapl_pkg_ts_resample.data, power_per_socket(i).power_pkg_ts_resample.time, power_per_socket(i).power_pkg_ts_resample.data);
%     end
%     subplot(total_plots,1,2);
%     hold on;
% 
%     j=1;
%     contributions_matrix = [];
%     for domain_id = unique_domain_ids
%         contributions_matrix = [contributions_matrix, counter_domain(j).pmc(current_pmc).pmc_percent_ts.data];
%         legend_index=j;
%         legendInfo{legend_index} = ['dom-' int2str(counter_domain(j).id)];
%         j=j+1;
%     end
%     
%     contributions_matrix(sum(contributions_matrix,2)>contribution_matrix_filter,:) = [];
%     area(contributions_matrix);
%     title(['Reference: PMC ' label_pmc(current_pmc)]);
%     leg = legend(legendInfo);
%     set(leg,'location','southoutside','Orientation','horizontal');
%     xlabel('Time (s)');
%     ylabel('Contribution to the total consumption (%)');
%     grid on;
%     grid minor;
%     hold off;
%     
%     i = i+1;
% end
% 
% 
% 
% % Distribution of Package Energy consumption per domain, for every PMCi
% %disp('- Plot distribution of Package Energy consumption per domain, for every PMCi');
% % figure;
% % orient landscape
% % subplot(2,1,1);
% % for i=1:length(rapl_per_socket)
% %    plot_energy_and_power(rapl_per_socket(i).rapl_pkg_ts_resample.time, rapl_per_socket(i).rapl_pkg_ts_resample.data, power_per_socket(i).power_pkg_ts_resample.time, power_per_socket(i).power_pkg_ts_resample.data);
% % end
% % subplot(2,1,2);
% % hold on;
% % 
% % j=1;
% % contributions_matrix = [];
% % for domain_id = unique_domain_ids
% %     contributions_matrix = [contributions_matrix, counter_domain(j).power_pkg_ts.data];
% %     legend_index=j;
%     legendInfo{legend_index} = ['dom-' int2str(counter_domain(j).id)];
%     j=j+1;
% end
%     
% contributions_matrix(sum(contributions_matrix,2)>contribution_matrix_filter*max(power_pkg_ts_resample.data),:) = [];
% area(contributions_matrix);
% plot(power_pkg_ts_resample.time, power_pkg_ts_resample.data);
% title('Reference: PMC');
% leg = legend(legendInfo);
% set(leg,'location','southoutside','Orientation','horizontal');
% xlabel('Time (s)');
% ylabel('Contribution to the total consumption (%)');
% grid on;
% grid minor;
% hold off;