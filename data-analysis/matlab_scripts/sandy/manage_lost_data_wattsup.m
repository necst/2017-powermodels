function [wattsup_ts_fixed] = manage_lost_data_wattsup(source_ts_data,source_ts_time,base_power)
    
wattsup_empy_data_bit_mask = source_ts_data(:)<=0; 
source_ts_data(wattsup_empy_data_bit_mask)= base_power;
wattsup_ts_fixed=timeseries(source_ts_data,source_ts_time);
end