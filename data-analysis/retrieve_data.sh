#!/bin/bash

echo $(date +%s.%N) " - Retrieving tests from remote machine..."
scp -r marc@$TEST_MACHINE_IP:$REMOTE_TEST_FOLDER $LOCAL_TEST_FOLDER

cd $LOCAL_TEST_FOLDER

echo $(date +%s.%N) " - Decompressing data..."
for file in *.tar.gz; do tar -zxf $file; done