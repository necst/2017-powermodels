import os, logging, subprocess, sys, shutil, yaml

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler("./parser.log")
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.DEBUG)
sh.setFormatter(formatter)
logger.addHandler(sh)


CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))

RAW_TESTS_DIRECTORY = "{}/tests".format(CURRENT_PATH)

CONFIG_FILE = "./config.yml"

with open(CONFIG_FILE, 'r') as f:
    config = yaml.load(f)
MATLAB_EXEC = config["matlab_executor"]
CURRENT_MACHINE = config["current_machine"]
MATLAB_SOURCE_CODE = "{}/matlab_scripts/{}".format(CURRENT_PATH,CURRENT_MACHINE)

ONEDRIVE_FOLDER = config["ondedrive_folder"].replace(" ","\ ")


def manage_error(message):
	logger.error(message)
	sys.exit(-1)

def parse_shell_result(cmd,out,err):
	return "\nCOMMAND => {}\nOUTUP => {}\nERROR => {}".format(cmd,out,err)

def executor(cmd):
	logger.info("Trying executing command:\n{}".format(cmd))
	proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = proc.communicate()
	exit_status = proc.wait()
	report = parse_shell_result(cmd,out,err)

	if exit_status != 0:
		manage_error(report)
	else:
		logger.debug(parse_shell_result(cmd,out,err))

	return out

def launch_matlab_preprocessing():
	cmd = "{} -nodisplay -browser -nosplash < marc_multiple_socket.m".format(MATLAB_EXEC)
	executor(cmd)

def copy_matlab_code(destination_folder):
	src_files = os.listdir(MATLAB_SOURCE_CODE)
	for file_name in src_files:
		full_file_name = os.path.join(MATLAB_SOURCE_CODE, file_name)
		if (os.path.isfile(full_file_name)):
			shutil.copy(full_file_name, destination_folder)

def copy_folder_to_cloud(file_name,source_folder):
	cmd = "tar -zcvf {} -C {} {}".format(ONEDRIVE_FOLDER+"/"+file_name+".tar.gz",file_name,".")
	executor(cmd)



logger.info("Start preprocessing data tests....")
os.chdir(RAW_TESTS_DIRECTORY)

onlyfiles = [f for f in os.listdir(RAW_TESTS_DIRECTORY) if os.path.isfile(os.path.join(RAW_TESTS_DIRECTORY, f))]
for file in onlyfiles:
	if ".tar.gz" in file:
		cmd = "tar --strip-components 5 -xvf {}".format(file)
		executor(cmd)
		cmd = "rm {}".format(file)
		executor(cmd)
		folder = file.replace(".tar.gz","")
		full_path_folder = RAW_TESTS_DIRECTORY + "/" + folder
		copy_matlab_code(folder)
		os.chdir(folder)
		os.remove(full_path_folder+"/trace.data")
		launch_matlab_preprocessing()
		os.chdir(RAW_TESTS_DIRECTORY)
		copy_folder_to_cloud(folder,full_path_folder)
		cmd = "rm -r {}".format(full_path_folder)
		executor(cmd)



