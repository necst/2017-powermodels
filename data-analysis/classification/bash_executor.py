import os,logging, subprocess, sys
from email_sender import EmailSender


class BashExecutor:

	def __init__(self,log_file_path):
		self.__setup_logger(log_file_path)
		self.sender = EmailSender(["andrea.corna.ac.91@gmail.com"])

	def execute(self,command):
		self.logger.info("Trying executing command:\n{}".format(command))
		proc = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		exit_status = proc.wait()
		report = self.__parse_shell_result(command,out,err)

		if exit_status != 0:
			self.__manage_error(report)
		else:
			self.logger.debug(self.__parse_shell_result(command,out,err))

		return out

	def execute_background(self,command):
		self.logger.info("Trying executing command:\n{}".format(command))
		proc = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		self.logger.debug(self.__parse_shell_result(command,"",""))


	def __parse_shell_result(self,cmd,out,err):
		return "\nCOMMAND => {}\nOUTUP => {}\nERROR => {}".format(cmd,out,err)


	def __manage_error(self,message):
		self.logger.error(message)
		self.sender.send("Error bash_executor",message)
		sys.exit(-1)

	def __setup_logger(self,log_file_path):
		self.logger = logging.getLogger(__name__)
		self.logger.setLevel(logging.DEBUG)
		handler = logging.FileHandler(log_file_path)
		handler.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
		handler.setFormatter(formatter)
		self.logger.addHandler(handler)
		sh = logging.StreamHandler(sys.stdout)
		sh.setLevel(logging.DEBUG)
		sh.setFormatter(formatter)
		self.logger.addHandler(sh)


	def generous_executor(self,cmd):
		logger.info("Trying executing command:\n{}".format(cmd))
		proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		exit_status = proc.wait()
		report = parse_shell_result(cmd,out,err)

		if exit_status != 0:
			logger.error(parse_shell_result(cmd,out,err))
		else:
			logger.debug(parse_shell_result(cmd,out,err))

		return out







