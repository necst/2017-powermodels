import os
from bash_executor import BashExecutor


SOURCE_FOLDER = "./tests"

directories = [os.path.join(SOURCE_FOLDER, f) for f in os.listdir(SOURCE_FOLDER) if os.path.isdir(os.path.join(SOURCE_FOLDER, f))]

bash_executor = BashExecutor("./log.log")


for directory in directories:
	internal_directories = [os.path.join(directory, f) for f in os.listdir(directory) if os.path.isdir(os.path.join(directory, f))]
	cmd = "java -cp asplos.jar it.necst.marc.asplos.ComparisonMultiDomainRapl /Users/andreacorna/Desktop/MULTIDOMAIN_COMPOARISON/config.txt {} {}".format(internal_directories[0].split("/")[-1],directory)
	bash_executor.execute(cmd)