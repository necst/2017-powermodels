import os,time,logging,subprocess,sys,tarfile,shutil,yaml
from datetime import datetime, timedelta
from bash_executor import BashExecutor

OUTPUT_FOLDER = "./divide/output"
SOURCE_FOLDER = "./divide/source"

directories = [os.path.join(SOURCE_FOLDER,o) for o in os.listdir(SOURCE_FOLDER) if os.path.isdir(os.path.join(SOURCE_FOLDER,o))]

bash_executor = BashExecutor("./log.log")



for directory in directories:
	test_name = directory.split("/")[-1]

	list_tests = []

	list_first_socket = []

	if len(test_name.split("+")) > 1:
		#case of heterogeneous test
		test_name_no_date = ''.join([i for i in test_name if not i.isdigit()]).replace("_","")
		#get tests on different sockets
		sockets = test_name_no_date.split("-")
		to_append = 0
		first_socket = len(sockets[0].split("+"))
		for socket in sockets:
			tests = socket.split("+")
			for test in tests:
				list_tests.append(test.strip("\n").replace(" ","")+str(to_append))
				to_append = to_append + 1
		for i in range(0,first_socket):
			list_first_socket.append(list_tests[i])

	else:
		#case omogeneous test
		components = test_name.split("_")
		last_underscore_index = test_name.rfind("_")
		test_name_no_date = test_name[:last_underscore_index]
		to_append = 0
		first_socket = 0
		if "page" in components[0]:
			benchmark_name = components[0] + "_" + components[1]
			total_tests = int(components[2]) + int(components[3])
			first_socket = int(components[2])
		else:
			benchmark_name = components[0]
			total_tests = int(components[1]) + int(components[2])
			first_socket = int(components[1])
		for i in range(0,total_tests):
			list_tests.append("{}".format(benchmark_name+str(to_append)))
			to_append = to_append + 1

		for i in range(0,first_socket):
			list_first_socket.append(list_tests[i])

	list_second_socket = [t for t in list_tests if t not in list_first_socket]



	for item in list_first_socket:
		if not os.path.exists(OUTPUT_FOLDER+"/"+test_name+"-socket1"):
			os.makedirs(OUTPUT_FOLDER+"/"+test_name+"-socket1")
		mv_cmd = "mv  {} {}".format(directory+"/"+item,OUTPUT_FOLDER+"/"+test_name+"-socket1")
		bash_executor.execute(mv_cmd)

	for item in list_second_socket:
		if not os.path.exists(OUTPUT_FOLDER+"/"+test_name+"-socket2"):
			os.makedirs(OUTPUT_FOLDER+"/"+test_name+"-socket2")
		mv_cmd = "mv {} {}".format(directory+"/"+item,OUTPUT_FOLDER+"/"+test_name+"-socket2")
		bash_executor.execute(mv_cmd)


