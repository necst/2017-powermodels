import os,time,logging,subprocess,sys,tarfile,shutil,yaml
from datetime import datetime, timedelta
from bash_executor import BashExecutor

OUTPUT_FOLDER = "./divide/output"
SOURCE_FOLDER = "./divide/source"

directories = [os.path.join(SOURCE_FOLDER,o) for o in os.listdir(SOURCE_FOLDER) if os.path.isdir(os.path.join(SOURCE_FOLDER,o))]

bash_executor = BashExecutor("./log.log")

for directory in directories:

	folder_name = directory.split("/")[-1]

	if "-all" in folder_name:
		get_test = folder_name.split("-SEP-")[1]

		if not os.path.exists(OUTPUT_FOLDER+"/"+get_test):
			os.makedirs(OUTPUT_FOLDER+"/"+get_test)

		component = folder_name.split("-SEP-")[0]

		mv_cmd = "mv {} {}".format(directory,OUTPUT_FOLDER+"/"+get_test+"/"+component)
		bash_executor.execute(mv_cmd)

	else:
		rm = "rm -r {}".format(directory)
		bash_executor.execute(rm)