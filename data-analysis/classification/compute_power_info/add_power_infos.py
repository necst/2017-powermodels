import os,time,logging,subprocess,sys,tarfile,shutil,yaml
from datetime import datetime, timedelta
from bash_executor import BashExecutor


NOW = time.strftime("%Y-%m-%d")

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.DEBUG)
sh.setFormatter(formatter)
logger.addHandler(sh)


def main():


	configuration_file_path = os.path.dirname(os.path.abspath(__file__))+'/config.yml'

	if os.path.isfile(configuration_file_path) is False:
		logger.error("Configuration file {} not found".format(configuration_file_path))
		exit(-1)

	with open(configuration_file_path, 'r') as f:
		config = yaml.load(f)

	SOURCE_FOLDER = "{}".format(config['SOURCE_FOLDER'])

	logger.info(SOURCE_FOLDER)

	bash_executor = BashExecutor("./log.log")

	#list tar file
	#export
	#remove tar 
	#copy power_info.txt
	#remove folder

	onlyfiles = [os.path.join(SOURCE_FOLDER, f) for f in os.listdir(SOURCE_FOLDER) if os.path.isfile(os.path.join(SOURCE_FOLDER, f))]


	directories = [os.path.join(SOURCE_FOLDER,o) for o in os.listdir(SOURCE_FOLDER) if os.path.isdir(os.path.join(SOURCE_FOLDER,o))]

	logger.info(directories)
	for directory in directories:

		name_test = directory.split("/")[-1]

		multi_socket = 0
		if len(name_test.split("+")) > 1:
			multi_socket = 1
		else:
			components = name_test.split("_")
					
			last_underscore_index = name_test.rfind("_")
			test_name_no_date = name_test[:last_underscore_index]
			tests_on_second_Socket = 0
			if "page" in components[0]:
				tests_on_second_Socket =  int(components[3])
			else:
				tests_on_second_Socket = int(components[2])
			if tests_on_second_Socket > 0:
				multi_socket = 1

		logger.info(multi_socket)
		logger.info(name_test)

		JAR_PATH = config['JAR_PATH']
		MAIN_CLASS = config['MAIN_CLASS']
		create_power_info_file_cmd = "java -cp {} {} {} {}".format(JAR_PATH,MAIN_CLASS,directory,multi_socket)
		bash_executor.execute(create_power_info_file_cmd)


main()