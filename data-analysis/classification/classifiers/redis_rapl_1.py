#!/usr/bin/env python

'''
ASPLOS Classifier

Parses a HPC + RAPL trace, generating
1. Power classes 	[edit KDE_VALUES for boundaries]
2. HPC classes 		[edit CLASSIFIER for classification routine]

Computes missclassification stats in the end.
'''

from scipy import stats
from scipy.signal import argrelextrema
import pandas as pd
import numpy as np
import sys,os.path

if len(sys.argv) < 4:
	print "ERROR - Please specify source file, time column and rapl column"
	exit(-1)

if os.path.exists(sys.argv[1]) == False:
    print "ERROR - The file specified does not exist"
    exit(-1)

'''
EDIT TO CHANGE CLASSIFIER
'''
KDE_VALUES = [24300]

def CLASSIFIER(row):
	un_core_cycle   = row["UN_CORE_CYCLE"]

	main_min        = 278218.0
	main_max        = 3391910000.0
	main_step       = 100000
	main_cp			= compute_factory(main_min, main_max, main_step)

	if main_cp(0) <= un_core_cycle and un_core_cycle < main_cp(44434):
		return 0
	else:
		return 1

'''
BEGIN OF SCRIPT
'''
path = sys.argv[1]
time_col = sys.argv[2]
rapl_col = sys.argv[3]

data = pd.read_csv(path, sep=',', header=0).dropna()

data.replace('', np.nan, inplace=True)
data.dropna(inplace=True)

data['Dt'] = data[time_col].diff()
data['Dy'] = data[rapl_col].diff()

power_column = '__POWER__'

data[power_column] = data['Dy'] / data['Dt']

data = data.drop('Dt', axis=1).drop('Dy', axis=1)

data.replace('', np.nan, inplace=True)
data.dropna(inplace=True)

def oracle(row):
	value = row[power_column]
	power_class = 0
	for item in KDE_VALUES:
		if value > (item / 1000):
			power_class = power_class + 1
		else:
			break
	
	return power_class

data['power_class'] = data.apply(oracle, axis=1)

def compute_factory(min, max, steps):
	return (lambda value: min + (((max - min) / steps) * value))


data['class'] = data.apply(CLASSIFIER, axis=1)

data.to_csv('./CLASSIFIED_SPECIFIC.csv', sep=',', index=False, mode='w+')

classifications = {}
rows = data.iterrows()

for val in np.unique(data['power_class']):
	classifications[val] = {
		'miss': 0,
		'ok': 0
	}

for i, row in rows:
	clazz = row['power_class']
	if clazz == row['class']:
		classifications[row['power_class']]['ok'] = classifications[row['power_class']]['ok'] + 1
	else:
		classifications[row['power_class']]['miss'] = classifications[row['power_class']]['miss'] + 1

tot_ok = 0
tot_miss = 0

print "Classification report:"
print "----------------------"
for key, value in classifications.iteritems():
	print "CLASS " + str(key)
	tot_ok = tot_ok + value['ok']
	tot_miss = tot_miss + value['miss']
	missed = float(value['miss']) / (value['miss'] + value['ok'])
	print "misclassification: " + str(missed * 100) + '% (missed: ' + str(value['miss']) + ')'

missed = float(tot_miss) / (tot_miss + tot_ok)
print "OVERALL misclassification: " + str(missed * 100) + '% (missed: ' + str(tot_miss) + ')'