#!/bin/bash

# Check if script is launched as root
if [ $(whoami) != "root" ]; then
    echo "ERROR - Run me as root"
    exit 1
fi

# Stop if sudo not installed
which sudo 1>/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
	echo "WARNING - sudo command not found. Installing..."
	apt-get update && apt-get install sudo 
fi

if [[ -f "./environment.sh" ]]; then

	echo "------ save current working directory ------"
	CWD=$(pwd)

	echo "------ users setup ------"
	# Create user: marc
	echo "[*] Adding user: marc"
	adduser marc --disabled-password --gecos ""
	# Setup ssh
	echo "[*] Creating .ssh directory in marc user home"
	mkdir -p /home/marc/.ssh/
	echo "[*] Writing authorized_keys file in marc .ssh"
	echo "" >> /home/marc/.ssh/authorized_keys
	echo "    matt@macbook"
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPGJPGHjEo4z/0c0lcRFHir91DJLpKGrqVsGu2oyGLA1tL0i7w5/EbL/EvYN/0YjW0UiodxE+hX3maDBCQgImsPbCr+9Xz6bDPyNHaXCs5LAddZaUAQJLqC51EVsQAaXBH5TMfT1XpU0xJLRIkqqaTR0PSofnBblqxx2x3XCKqZmUiyqRTOkLukd/1tHuqp0QPBucaPc2WSpe5sH3dcLG/eYSzqfUX+QS735skDwnw9N3vpcnDZybPTEDv4JlWnrqhZBzrfdy+ruzj3H76JIG9XTlzjYH2NYxKhRyi4JBvt9/12MTcYtEOmZdk6VJNdha0tH4EdvLZPz9/HskCSmyR matt@macbook" >> /home/marc/.ssh/authorized_keys
	echo "" >> /home/marc/.ssh/authorized_keys
	echo "    andrea.corna.ac.91@gmail.com"
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDP6kCf+RB9TZm8VVPVFtE1tWIijzlf1qijAZOHn8tOujnASTo6cyHT/pgfHlCRyJjC5x8chSZNrMzkg6mMmksTRjz44GpVU5/j/vYyXP3abXI5tlSXz6Sa7pf37L466sF0Tf99yZoawQz+2YX+JfnaUpd7It4bjLD5ZXIK9NBq+7Qin6L9mEgbSP9iiRRmQL/Ivam1sDv8Z4hYVFjPRJty8lM7YHI7SiCjrGuGUBR1sb3dxy88i1JS8L0ZPyC3mnbj55kuwZ5I/zDv+wiJN5SElwZDhAxjQXa+k5E+YF423tLhO0jkBWDJ5XM4xmV5GSYEaZR7h7t8VwH8VnHEPQb/ andrea.corna.ac.91@gmail.com" >> /home/marc/.ssh/authorized_keys
	echo "" >> /home/marc/.ssh/authorized_keys
	echo "    andreadamiani@MacBook-Pro-di-Andrea.local"
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXBcGx/G5xshEVcE3aAU9jNZ0prQDgAvvO7Zq+G3k1GnIuTPRFo1y88pXG9Q7aw4o29JlLwQhJz0XMELyJ84t8Gs/vC+5l+UYFN95wPbbtwha6GnbpW38qry8VxeQZFDMyMhn3WS8OVW5YnsmkYh6OJPtMwOaOAem2wrsLIsHHG8rk8+Q9ublNsvbdi9vxe02oGvJV4X68x08VLtaFsfVYs0WzE1MKTV83s3H78p6D/WDfRIZWmWh4PrHRecRMff7xGmZI16GW3csh8f5lPmSG4U6+acsHTTv40YZmJyD7Y8+MmdpnhK5BPVvHBg0DaJuVbl/VFsxLlqOQs0WE77p9 andreadamiani@MacBook-Pro-di-Andrea.local" >> /home/marc/.ssh/authorized_keys
	echo "" >> /home/marc/.ssh/authorized_keys
	

	# Permissions&co.
	echo "[*] Setting permissions in .ssh directory of marc"
	chmod -R 700 /home/marc/.ssh/
	chown -R marc:marc /home/marc/.ssh/
	chmod 600 /home/marc/.ssh/authorized_keys
	echo "[*] Setting sudoers policy"
	mkdir -p /etc/sudoers.d/
	echo "marc	ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers.d/marc
	echo "" >> /etc/sudoers.d/marc
	echo "[*] Setting sudoers file permissions"
	chmod 440 /etc/sudoers.d/marc
	echo ""

	echo "------ environment setup ------"
	cd $CWD
    source environment.sh

    echo "------ folders setup ------"
	mkdir $LOCAL_TEST_FOLDER

else
	echo "No environment.sh file found."
fi